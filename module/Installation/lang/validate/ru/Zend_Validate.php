<?php
return [
    "Passwords do not match" => "Пароли не совпадают",
    "The password length is less than %min% characters long" => "Длина пароля меньше %min% символов",
    "The input is less than %min% characters long" => "Количество символов меньше %min%",
    "Value is required and can't be empty" => "Значение обязательно для заполнения и не может быть пустым",
    "'%value%' must contains only alphabetic characters and digits" => "'%value%' должно содержать только буквенные символы и цифры",
    "'%value%' must contains only alphabetic characters, digits and underscore symbol" => "'%value%' должно содержать только буквенные символы, цифры и символ подчеркивания",
];