<?php
namespace Installation\Service;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Exception\ConnectionException;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class DatabaseService implements ServiceLocatorAwareInterface
{
     private static $_driverMap = [
        'pdo_mysql'  => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
        'pdo_sqlite' => 'Doctrine\DBAL\Driver\PDOSqlite\Driver',
        'pdo_pgsql'  => 'Doctrine\DBAL\Driver\PDOPgSql\Driver',
        'pdo_oci' => 'Doctrine\DBAL\Driver\PDOOracle\Driver',
        'oci8' => 'Doctrine\DBAL\Driver\OCI8\Driver',
        'ibm_db2' => 'Doctrine\DBAL\Driver\IBMDB2\DB2Driver',
        'pdo_ibm' => 'Doctrine\DBAL\Driver\PDOIbm\Driver',
        'pdo_sqlsrv' => 'Doctrine\DBAL\Driver\PDOSqlsrv\Driver',
        'mysqli' => 'Doctrine\DBAL\Driver\Mysqli\Driver',
        'drizzle_pdo_mysql'  => 'Doctrine\DBAL\Driver\DrizzlePDOMySql\Driver',
        'sqlsrv' => 'Doctrine\DBAL\Driver\SQLSrv\Driver',
     ];

    public function getConnectionParams()
    {
        $installator = $this->getServiceLocator()->get('InstallationService');
        $settings = $installator->getConfig('database');

        $connectionParams = [
            'dbname'   => $settings['dbname'],
            'user'     => $settings['username'],
            'password' => $settings['password'],
            'host'     => $settings['server'],
            'port'     => 3306,
            'prefix'   => $settings['prefix'] . '_',
            'driver'   => 'pdo_mysql',
        ];
        
        return $connectionParams;
    }

    public function getConnection()
    {
        $config = new Configuration();

        return DriverManager::getConnection($this->getConnectionParams(), $config);
    }

    public function testConnection()
    {
        $conn = $this->getConnection();

        try {
            $conn->connect();
        } catch (ConnectionException $e) {
            return $e->getMessage();
        }
        
        return true;
    }

    public function getDriverClass($driver)
    {
        return self::$_driverMap[$driver];
    }

    public function getTablePrefix()
    {
        $installator = $this->getServiceLocator()->get('InstallationService');
        $settings = $installator->getConfig('database');

        return $settings['prefix'];
    }

    public function getDatabaseName()
    {
        $conn = $this->getConnection();
        return $conn->getDatabase();
    }

    public function createTables()
    {
        $prefix = $this->getTablePrefix();
        
        $filename = MODULE_PATH . '/Installation/resources/sql/mysql/installation.sql';

        // Get sql statements from file
        $statement = file_get_contents($filename);
        $statement = str_replace('#_', $prefix, $statement);

        $conn = $this->getConnection();
        
        $stmt = $conn->prepare($statement);
        $stmt->execute();
    }

    public function dropTables()
    {
        $conn = $this->getConnection();

        $schemaManager = $conn->getSchemaManager();
        
        // Get list of existing tables
        $tables = $schemaManager->listTables();

        $conn->beginTransaction();
        try {
            foreach ($tables as $table) {
                // Get list of foreign keys for each table
                $foreignKeys = $schemaManager->listTableForeignKeys($table->getName());
                
                foreach ($foreignKeys as $foreignKey) {
                    // Drop foreign keys
                    $schemaManager->dropForeignKey($foreignKey, $table);
                }
            }
            foreach ($tables as $table) {
                // Drop tables
                $schemaManager->dropTable($table->getName());
            }
            $conn->commit();
        } catch(\Exception $e) {
            $conn->rollback();
            throw $e;
        }
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
