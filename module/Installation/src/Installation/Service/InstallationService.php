<?php
namespace Installation\Service;

use Zend\Session\Container;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class InstallationService implements ServiceLocatorAwareInterface
{
    private $container;
    private $config;

    public function __construct()
    {
        $this->container = new Container('installation');
        $this->config = new Container('installation_config');
    }

    public function complete($action)
    {
        if (!$this->container->$action) {
            $this->container->$action = true;
        }
    }

    public function isCompleted($action)
    {
        $action .= 'Action';
        return (bool) $this->container->$action;
    }

    public function finish()
    {
        $configPath = dirname(MODULE_PATH);
        rename($configPath . '/config/application.config.php', $configPath . '/config/application.config.php.tmp');
        rename($configPath . '/config/application.config.php.dist', $configPath . '/config/application.config.php');

        $serviceManager = $this->getServiceLocator();
        $sessionManager = $serviceManager->get('Zend\Session\SessionManager');
        $sessionManager->destroy();
    }

    public function getConfig($name = null)
    {
        if (!empty($name)) {
            return $this->config->$name;
        }

        return $this->config;
    }

    public function setConfig($name, $value = null)
    {
        $this->config->$name = $value;
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}