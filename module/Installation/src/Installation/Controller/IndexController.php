<?php
namespace Installation\Controller;

use Installation\Model\Installation;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Zend\ProgressBar\Adapter\JsPull;
use Zend\ProgressBar\ProgressBar;

use Installation\Form;

class IndexController extends AbstractActionController
{
    public function __construct(Installation $model)
    {
        $this->model = $model;
    }

    public function startAction()
    {
        $installator = $this->getServiceLocator()->get('InstallationService');

        if ($installator->isCompleted('install')) {
            $viewModel = $this->forward()->dispatch('Installation\Controller\Index', ['action' => 'install']);
            return $viewModel;
        }

        $form = $this->model->getForm('Profile');

        if (null !== $installator->getConfig('profile')) {
            $form->setData($installator->getConfig('profile'));
        }

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = [
                    'email'    => $request->getPost('email'),
                    'login'    => $request->getPost('login'),
                    'password' => $request->getPost('password')
                ];

                $installator->setConfig('profile', $data);

                $installator->complete(__FUNCTION__);

                $this->redirect()->toRoute('installation', ['action' => 'database']);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function databaseAction()
    {
        $installator = $this->getServiceLocator()->get('InstallationService');

        if (!$installator->isCompleted('start')) {
            return $this->redirect()->toRoute('installation', ['action' => 'start']);
        }

        if ($installator->isCompleted('install')) {
            $viewModel = $this->forward()->dispatch('Installation\Controller\Index', ['action' => 'install']);
            return $viewModel;
        }

        $databaseService = $this->getServiceLocator()->get('DatabaseService');

        $form = $this->model->getForm('Database');

        if (null !== $installator->getConfig('database')) {
            $form->setData($installator->getConfig('database'));
        }

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $data = [
                    'server'          => $request->getPost('server'),
                    'username'        => $request->getPost('username'),
                    'password'        => $request->getPost('password'),
                    'dbname'          => $request->getPost('dbname'),
                    'prefix'          => $request->getPost('prefix'),
                    'drop_old_tables' => $request->getPost('drop_old_tables')
                ];

                $installator->setConfig('database', $data);

                if (($message = $databaseService->testConnection()) === true) {
                    $installator->complete(__FUNCTION__);

                    $this->redirect()->toRoute('installation', ['action' => 'install']);
                } else  {
                    return new ViewModel([
                        'form' => $form,
                        'message' => $message
                    ]);
                }
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function installAction()
    {
        $request = $this->getRequest();
        $installator = $this->getServiceLocator()->get('InstallationService');

        if (!$installator->isCompleted('database')) {
            return $this->redirect()->toRoute('installation', ['action' => 'database']);
        }

        if ($request->isXmlHttpRequest()) {
            $translator = $this->getServiceLocator()->get('translator');

            $max = 3;

            $adapter     = new JsPull();
            $progressBar = new ProgressBar($adapter, 0, $max, 'progress');

            $offset = $request->getPost('offset');

            switch ($offset) {
                case 0 :
                    $databaseService = $this->getServiceLocator()->get('DatabaseService');
                    $databaseSettings = $installator->getConfig('database');

                    if ($databaseSettings['drop_old_tables']) {
                        $databaseService->dropTables();
                    }

                    $databaseService->createTables();
                    $this->model->saveDatabaseSettings();

                    $text = $translator->translate('Setting up the database', 'Installation');
                    break;
                case 1 : $this->model->saveSiteSettings();
                    $text = $translator->translate('Saving site settings', 'Installation');
                    break;
                case 2 : $this->model->createAdminProfile();
                    $text = $translator->translate('Creating an administrator profile', 'Installation');
                    break;
            }

            usleep(100000);
            $offset++;

            if ($offset <= $max) {
                $progressBar->update($offset, $text);
            } else {
                $installator->complete(__FUNCTION__);
                $installator->finish();

                $progressBar->finish();
            }
        }

        return new ViewModel();
    }

    public function selectLanguageAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            $this->getEvent()->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $languageIdent = $request->getPost('ident');

        if (null !== $languageIdent) {
            $appModel = $this->getServiceLocator()->get('Application\Model\Application');
            $languages = $appModel->getLanguages();

            $language = $languages[$languageIdent];
            $language['ident'] = $languageIdent;

            $installator = $this->getServiceLocator()->get('InstallationService');
            $installator->setConfig('language', $language);

            return new JsonModel([
                'success' => 1
            ]);
        }

        return new JsonModel([
            'success' => 0
        ]);
    }
}