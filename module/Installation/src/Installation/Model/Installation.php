<?php
namespace Installation\Model;

use Application\Mvc\Model\AbstractModel;
use Zend\Stdlib\ArrayUtils;
use Users\Model\Entity\GenericUser as User;

class Installation extends AbstractModel
{
    public function createAdminProfile()
    {
        $userModel = $this->getServiceLocator()->get('Users\Model\Users');
        $accessManager = $this->getServiceLocator()->get('AccessManager\Model\AccessManager');

        $installator = $this->getServiceLocator()->get('InstallationService');

        $user = new User();
        $user->setActive(1);
        $user->populate($installator->getConfig('profile'));

        $adminRole = $accessManager->getRoleById(1);
        $user->addRole($adminRole);

        $userModel->saveUser($user);
    }

    public function saveSiteSettings()
    {
        $installator = $this->getServiceLocator()->get('InstallationService');
        $appModel = $this->getServiceLocator()->get('Application\Model\Application');

        $language = $installator->getConfig('language');

        $appModel->setConfig('language', $language['ident'] ?: 'en');
    }

    public function saveDatabaseSettings()
    {
        $databaseService = $this->getServiceLocator()->get('DatabaseService');
        $connectionParams = $databaseService->getConnectionParams();

        $driver = array_pop($connectionParams);
        $driverClass = $databaseService->getDriverClass($driver);

        $settings = [
            'doctrine' => [
                'connection' => [
                    'orm_default' => [
                        'driverClass' => $driverClass,
                        'params' => $connectionParams
                    ]
                ]
            ]];

        $filepath = 'config/autoload/database.local.php';

        $this->writeArrayToFile($filepath, $settings);
    }

    protected function writeArrayToFile($filePath, $array)
    {
        $content = "<?php\nreturn " . var_export($array, 1) . ';';
        file_put_contents($filePath, $content);
        return $this;
    }
}
