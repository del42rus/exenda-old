<?php
namespace Users\Form\Validator;

use Zend\Validator\AbstractValidator;
use Application\Mvc\Model\AbstractModel as Model;

class UniqueEmail extends AbstractValidator
{
    const EMAIL_EXISTS = 'emailExists';

    protected $messageTemplates = [
        self::EMAIL_EXISTS =>  "Email '%value%' already exists in system"
    ];

    /**
     * @var \Users\Model\Users
     */
    protected $model;

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $user = $this->model->getUserByEmail($value);

        if (null === $user || $user->getId() == $context['id']) {
            return true;
        }

        $this->error(self::EMAIL_EXISTS);
        return false;
    }

    /**
     * Set model
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }
}