<?php
/**
 * Application CMS
 * 
 * @category Zend
 * @package Installation_Form
 */
namespace Installation\Form;

use Application\Form\Form;
use Installation\Form\Element\LanguageSelect;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

/**
 * @category Zend
 * @package Installation_Form
 */
class Profile extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $languageSelect = new LanguageSelect('language', [
            'model' => $this->getModel(),
            'label' => gettext('Language')
        ]);

        $this->add($languageSelect);

        $this->add([
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => [
                'label' => gettext('Email'),
            ],
            'attributes' => [
                'required' => 'required',
                'id' => 'inputEmail',
                'placeholder' => 'Email',
            ]
        ]);
        
        $this->add([
            'name' => 'login',
            'options' => [
                'label' => gettext('Login'),
            ],
            'attributes' => [
                'type' => 'text',
                'required' => 'required',
                'id' => 'inputLogin',
                'placeholder' => gettext('Login'),
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => [
                'label' => gettext('Password'),
            ],
            'attributes' => [
                'required' => 'required',
                'id' => 'inputPassword',
                'placeholder' => gettext('Password'),
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'confirm',
            'options' => [
                'label' => gettext('Confirm password'),
            ],
            'attributes' => [
                'required' => 'required',
                'id' => 'confirmPassword',
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'submit',
            'options' => [
                'label' => gettext('Next'),
            ],
            'attributes' => [
                'class' => 'btn btn-primary btn-flat pull-right',
                'type' => 'submit'
            ]
        ]);
        
        $inputFilter = $this->getInputFilter();
        
        $inputFilter->add([
            'name'     => 'login',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 2,
                        'max'      => 100,
                    ],
                ],
            ],
        ]);

        $loginValidator = new \Zend\Validator\Regex('/^[a-zA-Z0-9_]+/') ;
        $loginValidator->setMessage("'%value%' must contains only alphabetic characters, digits and underscore symbol", \Zend\Validator\Regex::NOT_MATCH);

        $inputFilter->get('login')
                    ->getValidatorChain()
                    ->addValidator($loginValidator);
        
        $inputFilter->add([
            'name'     => 'password',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ]
            ],
        ]);

        $inputFilter->add([
            'name'     => 'confirm',
            'required' => true,
            'filters'  => [
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ],
                [
                    'name' => 'Installation\Form\Validator\PasswordVerification',
                ]
            ],
        ]);

        $validator = new \Zend\Validator\StringLength(['min' => 8,
                                            'encoding' => 'UTF-8']);

        $validator->setMessage('The password length is less than %min% characters long', 
                                \Zend\Validator\StringLength::TOO_SHORT);

        $inputFilter->get('confirm')
                    ->getValidatorChain()
                    ->addValidator($validator);

        $inputFilter->get('password')
                    ->getValidatorChain()
                    ->addValidator($validator);

    }
}
