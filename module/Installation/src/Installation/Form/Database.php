<?php
namespace Installation\Form;

use Application\Form\Form;

class Database extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        
        $this->add([
            'name' => 'server',
            'options' => [
                'label' => gettext('Database server'),
            ],
            'attributes' => [
                'type' => 'text',
                'required' => 'required',
                'id' => 'inputServer',
                'value' => 'localhost',
            ]
        ]);
        
        $this->add([
            'name' => 'username',
            'options' => [
                'label' => gettext('Username'),
            ],
            'attributes' => [
                'type' => 'text',
                'required' => 'required',
                'id' => 'inputUsername',
                'value' => 'root',
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'required' => false,
            'options' => [
                'label' => gettext('Password'),
            ],
            'attributes' => [
                'id' => 'inputPassword',
            ]
        ]);
        
        $this->add([
            'name' => 'dbname',
            'options' => [
                'label' => gettext('Database name'),
            ],
            'attributes' => [
                'type' => 'text',
                'required' => 'required',
                'id' => 'inputName',
            ]
        ]);
        
        $this->add([
            'name' => 'prefix',
            'options' => [
                'label' => gettext('Table prefix'),
            ],
            'attributes' => [
                'type' => 'text',
                'required' => 'required',
                'id' => 'inputPrefix',
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'drop_old_tables',
            'options' => [
                'label' => gettext('Do you want drop old tables?'),
                'value_options' => [
                    1 => 'Yes', 
                    0 => 'No'
                ]
            ],
             'attributes' => [
                 'value' => 0,
                 'id' => 'radioDropTables'
             ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Button',
            'name' => 'submit',
            'options' => [
                'label' => gettext('Next'),
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary btn-flat pull-right'
            ]
        ]);
        
        $inputFilter = $this->getInputFilter();
        
        $inputFilter->add([
            'name'     => 'server',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ]
            ],
        ]);
        
        $inputFilter->add([
            'name'     => 'username',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ]
            ],
        ]);
        
        $inputFilter->add([
            'name'     => 'dbname',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ]
            ],
        ]);
        
        $inputFilter->add([
            'name'     => 'prefix',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
                ['name' => 'StringToLower'],
            ],
            'validators' => [
                [
                    'name' => 'NotEmpty'
                ],
            ],
        ]);
        
        $prefixValidator = new \Zend\Validator\Regex('/^[a-zA-Z0-9]+/') ;
        $prefixValidator->setMessage("'%value%' must contains only alphabetic characters and digits", \Zend\Validator\Regex::NOT_MATCH);
        
        $inputFilter->get('prefix')
                    ->getValidatorChain()
                    ->addValidator($prefixValidator);
        
        $inputFilter->add([
            'name'     => 'password',
            'filters'  => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'required' => false,
        ]);
   }
}
