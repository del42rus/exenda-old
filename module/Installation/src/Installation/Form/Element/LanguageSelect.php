<?php
namespace Installation\Form\Element;

use Zend\Form\Element\Select;

class LanguageSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $this->setAttribute('id', 'inputLanguage');

        $sm = $this->getOption('model')->getServiceLocator();

        $appModel = $sm->get('Application\Model\Application');
        $installator = $sm->get('InstallationService');

        $languages = $appModel->getLanguages();

        $values = [];

        if (count($languages)) {
            foreach ($languages as $ident => $language) {
                $values[$ident] = $language['name'];
            }
        }

        $this->setValueOptions($values);
        $this->setValue('en');

        if ($language = $installator->getConfig('language')) {
            $this->setValue($language['ident']);
        }
    }
    
    public function getInputSpecification()
    {
        $spec = [
            'name'     => $this->getName(),
            'required' => false,
            'validators' => []
        ];

        return $spec;
    }
}

