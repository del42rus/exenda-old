<?php
namespace Installation\View\Helper;

use Zend\View\Helper\AbstractHelper;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Installation extends AbstractHelper implements ServiceLocatorAwareInterface
{
    private $installationService;

    public function __invoke()
    {
        if (null === $this->installationService) {
            $helperPluginManager = $this->getServiceLocator();
            $sm = $helperPluginManager->getServiceLocator();

            $this->installationService = $sm->get('InstallationService');
        }

        return $this;
    }

    public function isCompleted($action)
    {
        return $this->installationService->isCompleted($action);
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}