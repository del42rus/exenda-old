<?php
namespace Installation;

return [
    'routes' => [
        'installation' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/installation[/:action]',
                'constraints' => [
                    'action' => '^\s$|[a-zA-Z0-9_-]*',
                ],
                'defaults' => [
                    'module' => __NAMESPACE__,
                    'controller' => 'Index',
                    'action' => 'start',
                ],
            ],
        ],
    ],
];