<?php
namespace Installation;

use Installation\Controller\IndexController;

return [
    'router' => include "router.config.php",

    'controllers' => [
        'factories' => [
            'Installation\Controller\Index' => function($cpm) {
                return new IndexController($cpm->getServiceLocator()->get('Installation\Model\Installation'));
            },
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],
    
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],
    
    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/..',
            ],
        ],
        'caching' => [
            'default' => [
                'cache'     => 'Filesystem',  // Apc, FilePath, FileSystem etc.
                'options' => [
                    'dir' => __DIR__ . '/../../../data/cache/assets', // path/to/cache
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            'installation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ],
    ],

    'navigation' => [
        'default' => [
            [
                'label' => 'Site configuration',
                'route' => 'installation',
                'order' => 1,
                'params' => [
                    'action' => 'start'
                ]
            ],
            [
                'label' => 'Database configuration',
                'route' => 'installation',
                'order' => 2,
                'params' => [
                    'action' => 'database'
                ]
            ],
            [
                'label' => 'Installation',
                'route' => 'installation',
                'order' => 3,
                'params' => [
                    'action' => 'install'
                ]
            ],
        ],
    ]
];
