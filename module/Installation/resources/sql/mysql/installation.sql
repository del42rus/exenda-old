CREATE TABLE IF NOT EXISTS `#__config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(255),
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `login` varchar(40),
  `email` varchar(60),
  `birthday` date,
  `gender` varchar(6) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `token` varchar(32),
  `series` varchar(32),
  `recovery_hash` varchar(32),
  `discriminator` varchar(255),
  `active` int(1) NOT NULL DEFAULT 0,
  `blocked` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `avatar` varchar(255),
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL UNIQUE,
  `description` text,
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `#__users`(`id`) ON DELETE CASCADE,
  FOREIGN KEY (`role_id`) REFERENCES `#__roles`(`id`) ON DELETE CASCADE
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__roles_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`role_id`) REFERENCES `#__roles`(`id`) ON DELETE CASCADE,
  FOREIGN KEY (`permission_id`) REFERENCES `#__permissions`(`id`) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=utf8;

insert into `#__roles` (`name`, `description`) values('Administrator',NULL);
insert into `#__roles` (`name`, `description`) values('Content manager',NULL);

insert into `#__permissions` (`name`, `module`, `description`) values('cpanel','Application','Control panel access');
insert into `#__permissions` (`name`, `module`, `description`) values('view-settings','Application','View settings');
insert into `#__permissions` (`name`, `module`, `description`) values('view','AccessManager','View access manager');
insert into `#__permissions` (`name`, `module`, `description`) values('add-role','AccessManager','Add role');
insert into `#__permissions` (`name`, `module`, `description`) values('edit-role','AccessManager','Edit role');
insert into `#__permissions` (`name`, `module`, `description`) values('delete-role','AccessManager','Delete role');
insert into `#__permissions` (`name`, `module`, `description`) values('set-permissions','AccessManager','Set permissions');
insert into `#__permissions` (`name`, `module`, `description`) values('add-user','Users','Add user');
insert into `#__permissions` (`name`, `module`, `description`) values('edit-user','Users','Edit user');
insert into `#__permissions` (`name`, `module`, `description`) values('delete-user','Users','Delete user');
insert into `#__permissions` (`name`, `module`, `description`) values('view','Users','View users');
insert into `#__permissions` (`name`, `module`, `description`) values('add','Pages','Add page');
insert into `#__permissions` (`name`, `module`, `description`) values('edit','Pages','Edit page');
insert into `#__permissions` (`name`, `module`, `description`) values('delete','Pages','Delete page');
insert into `#__permissions` (`name`, `module`, `description`) values('view','Pages','View pages');

CREATE TABLE IF NOT EXISTS `#__pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `alias` varchar(255) NOT NULL,
  `module` varchar(255) DEFAULT 'content',
  `template` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `seo_title` varchar(255),
  `description` text,
  `keywords` text,
  `link` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `published` int(1) NOT NULL DEFAULT 1,
  `hidden` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_alias` (`alias`),
  FOREIGN KEY (`link`) REFERENCES `#__pages` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (`parent_id`) REFERENCES `#__pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;

CREATE TABLE `#__content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` text,
  `page_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_title`(`title`),
  FOREIGN KEY (`page_id`) REFERENCES `#__pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__mail_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(255),
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__mail_event_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` text,
  KEY (`ident`),
  PRIMARY KEY (`id`)
) Engine=InnoDb CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type_id` int(11),
  `type` varchar(4) DEFAULT 'text',
  `active` int(1) DEFAULT 0,
  `sender_email` varchar(255) NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `carbon_copy` varchar(255),
  `blind_carbon_copy` varchar(255),
  `subject` varchar(255) NOT NULL,
  `text` text,
  `language` varchar(2) NOT NULL DEFAULT 'en',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `keep_on_server` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`event_type_id`) REFERENCES `#__mail_event_types`(`id`) ON DELETE SET NULL ON UPDATE CASCADE
) Engine=InnoDb CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__mail_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(255) NOT NULL,
  `sender_name` varchar(255),
  `recipient` varchar(255) NOT NULL,
  `cc` varchar(255),
  `bcc` varchar(255),
  `subject` varchar(255) NOT NULL,
  `content` text,
  `type` varchar(255) NOT NULL, #inbox, sent, trash
  `is_read` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) Engine=InnoDb CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__mailouts` (
  `id` int(11) NOT NULL  AUTO_INCREMENT,
  `recipient` VARCHAR(255) NOT NULL,
  `event_type_ident` VARCHAR(255) NOT NULL,
  `data` TEXT,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `is_sent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDb CHARSET=utf8;