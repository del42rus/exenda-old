var Installation = (function() {
    var $languageSelect = $('select[name="language"]');

    var $btnInstall = $('.js-btn-install');
    var $btnBack = $('.js-btn-back');
    var $pills = $('.nav-pills li');

    var isFinished = false;

    var progress = function(offset) {
        return $.ajax({
            url: '/installation/install',
            dataType: 'json',
            data: {'offset' : offset},
            type: 'POST',
            success: function(res) {
                if (!res.finished) {
                    var width = res.percent + '%';
                    $('.progress-bar').css({width : width});

                    jQuery('<li>', {
                        text: res.text
                    }).appendTo($('.progress-list'));

                    progress(res.current);
                } else {
                    $('.progress').addClass('hide');
                    $btnInstall.button('complete');
                    $btnInstall.attr('href', '/admin');
                    $btnBack.remove();

                    $('.box-title:visible, .box-body .text:visible').remove();
                    $('.box-title:hidden, .box-body .text:hidden').removeClass('hide');

                    $pills.last().removeClass('active')
                        .find('a').append('<span class="fa fa-check-circle"></span>');

                    isFinished = true;
                }
            }
        });
    };

    var changeLanguage = function() {
        return $.ajax({
            url : '/installation/select-language',
            data: { ident: $languageSelect.val() },
            type : 'post',
            dataType : 'json',
            success : function(res){
                if (res.success) {
                    window.location = window.location.href
                }
            }
        })
    };

    var bindUIActions = function() {
        $btnInstall.on('click', function(e) {
            if (isFinished) return;

            $(this).button('loading');
            $btnBack.attr('disabled', 'disabled');
            $pills.addClass('disabled');
            $pills.find('a').removeAttr('href');

            $('.progress').removeClass('hide');
            progress(0);
        });

        $languageSelect.on('change', function() {
            changeLanguage();
        })
    };

    var init = function() {
        bindUIActions();
    };

    return {
        init: init
    }
})();

$(function() {
   Installation.init();
});