<?php
namespace Installation;

use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\Router;

use AssetManager;
use Installation\Mvc\TranslatorListener;

class Module implements ViewHelperProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $translatorListener = new TranslatorListener();
        $translatorListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch']);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'onDispatch']);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setLayout'], -90);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setLayout'], -90);
    }

    public function onDispatch(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();

        if (!$matches instanceof Router\RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        $response = $e->getResponse();
        $request  = $e->getRequest();
        $serviceManager = $e->getApplication()->getServiceManager();
        $assetManager = $serviceManager->get('AssetManager\Service\AssetManager');

        if ($assetManager->resolvesToAsset($request)) {
            return;
        }

        $installator = $serviceManager->get('InstallationService');

        if ((strpos($matches->getMatchedRouteName(), 'installation') !== 0 &&
            !$installator->isCompleted('install')) ||
            $response->getStatusCode() == 404
        ) {
            $router = $e->getRouter();

            $uri = $router->assemble(['module' => 'installation', 'action' => 'start'], ['name' => 'default']);

            $response->setStatusCode(302);
            $response->getHeaders()->addHeaderLine('Location', $uri);

            $e->stopPropagation();
            $e->setResponse($response);
            $e->setResult($response);
        }
    }

    public function setLayout(MvcEvent $e)
    {
        $viewModel = $e->getViewModel();
        $viewModel->setTemplate('layout/installation');
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'InstallationService' => function($sm) {
                    return new Service\InstallationService();
                },
                'DatabaseService' => function($sm) {
                    return new Service\DatabaseService();
                },
                'Installation\Model\Installation' => function($sm) {
                    return new Model\Installation();
                }
            ],
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'installationService' => function($sm) {
                    $helper = new View\Helper\Installation();

                    return $helper;
                }
            ]
        ];
    }
}
