<?php
namespace ContentManager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use ContentManager\Model\ContentManager;

class IndexController extends AbstractActionController
{
    public function  __construct(ContentManager $contentManager)
    {
        $this->contentManager = $contentManager;
    }
    
    public function indexAction()
    {
        $id = (int) $this->params()->fromRoute('pageId', 0);
        $pageModel = $this->getServiceLocator()->get('Pages\Model\Pages');

        $page = $pageModel->getPageById($id);
        $content = $this->contentManager->findContentByPage($id);

        $rootModel = $this->getEvent()->getViewModel();
        $rootModel->setVariable('pageTitle', $page->getTitle());

        return new ViewModel([
            'content' => $content
        ]);
    }
}