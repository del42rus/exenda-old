<?php
namespace ContentManager\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use ContentManager\Form;
use ContentManager\Model\ContentManager;
use ContentManager\Model\Entity\Content;

use ZfcRbac\Exception\UnauthorizedException;

class AdminController extends AbstractActionController
{
    public function  __construct(ContentManager $contentManager)
    {
        $this->contentManager = $contentManager;
    }

    public function indexAction()
    {
        if (!$this->isGranted('content-manager.view')) {
            throw new UnauthorizedException();
        }

        $page = $this->params()->fromRoute('page', 1);

        $list = $this->contentManager->findAll($page);

        return new ViewModel([
            'list' => $list
        ]);
    }

    public function addAction()
    {
        if (!$this->isGranted('content-manager.add')) {
            throw new UnauthorizedException();
        }

        $ckfinderContainer = new \Zend\Session\Container('CKFinder');
        $ckfinderContainer->baseUrl = '/uploads/';
        $ckfinderContainer->userRole = 'Administrator';

        $form = $this->contentManager->getForm('ContentAdd');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $content = new Content();

            $form->setData($request->getPost());

            if ($form->isValid()) {
                
                $content->populate($form->getData());
                $this->contentManager->save($content);

                return $this->redirect()->toRoute('admin_default',
                    ['module' => 'content-manager', 'action' => 'edit', 'id' => $content->getId()]
                );
            } else {
                $rootViewModel = $this->getEvent()->getViewModel();
                $translator = $this->getServiceLocator()->get('translator');

                $text = $translator->translate('The form was completed with errors.', 'Gazetteer');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function editAction()
    {
        if (!$this->isGranted('content-manager.edit')) {
            throw new UnauthorizedException();
        }

        $ckfinderContainer = new \Zend\Session\Container('CKFinder');
        $ckfinderContainer->baseUrl = '/uploads/';
        $ckfinderContainer->userRole = 'Administrator';

        $id = (int) $this->params()->fromRoute('id', null);

        if (null === $id) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $content = $this->contentManager->findContentById($id);
        $translator = $this->getServiceLocator()->get('translator');

        if (null !== $content) {

            $form = $this->contentManager->getForm('ContentAdd');
            
            $viewModel = new ViewModel();

            $form->bind($content);

            $request = $this->getRequest();

            if ($request->isPost()) {
                $form->setData($request->getPost());

                $rootViewModel = $this->getEvent()->getViewModel();
                $translator = $this->getServiceLocator()->get('translator');

                if ($form->isValid()) {
                    $this->contentManager->save($content);

                    $text = $translator->translate('Content has been updated successfully.', 'Users');
                    $rootViewModel->setVariable('alert', ['type' => 'success', 'text' => $text]);
                } else {
                    $text = $translator->translate('The form was completed with errors.', 'Gazetteer');
                    $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
                }
            }
        } else {
            $message = sprintf($translator->translate('Content with id &laquo;%s&raquo; not found', 'ContentManager'), $id);

            $viewModel = new ViewModel(['message' => $message]);
            $viewModel->setCaptureTo('error');
            $viewModel->setTemplate('error/error');

            return $viewModel;
        }

        $viewModel->setVariables([
            'form' => $form,
            'content' => $content]
        );

        return $viewModel;
    }

    public function deleteAction()
    {
        if (!$this->isGranted('content-manager.delete')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', null);

        if (!$id || !$content = $this->contentManager->findContentById($id)) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()) {
            $this->contentManager->remove($content);

            return $this->redirect()->toRoute('admin_default', ['module' => 'content-manager']);
        }

        return new ViewModel([
            'url' => $request->getPost('url'), // get from ajax data
            'content' => $content,
        ]);
    }

    public function deleteSelectedAction()
    {
        if (!$this->isGranted('content-manager.delete')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            $this->contentManager->removeContent($ids);
            return $this->redirect()->toRoute('admin_default', ['module' => 'content-manager']);
        }

        return new ViewModel([
            'ids' => $ids,
            'url' => $request->getPost('url'), // get from ajax
        ]);
    }
}