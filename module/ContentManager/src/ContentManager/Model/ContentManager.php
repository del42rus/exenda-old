<?php
namespace ContentManager\Model;

use Application\Mvc\Model\AbstractModel;
use ContentManager\Model\Entity\Content;

use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class ContentManager extends AbstractModel
{
    public function findAll($paged = null, $itemCountPerPage = 50)
    {
        if (null !== $paged) {
            $qb = $this->getEntityManager()->createQueryBuilder();

            $query = $qb->select('c')
                        ->from('ContentManager\Model\Entity\Content', 'c')
                        ->getQuery();

            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                      ->setItemCountPerPage($itemCountPerPage)
                      ->setPageRange(5);

            return $paginator;
        }

        return $this->getEntity('Content')->findAll();
    }

    public function findContentById($id)
    {
        return $this->getEntity('Content')->find($id);
    }

    public function findPage($alias, $parent = null)
    {
        $appModel = $this->getServiceLocator()->get('Pages\Model\Pages');
        return $appModel->findPage($alias, $parent);
    }

    public function findContentByPage($id)
    {
        return $this->getEntity('Content')->findOneBy(['page' => $id]);
    }

    public function findDetachedContent()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        
        $query = $qb->select('c')
                    ->from('ContentManager\Model\Entity\Content', 'c')
                    ->where($qb->expr()->isNull('c.page'))
                    ->getQuery();
        
        return $query->getResult();
    }

    public function save(Content $content)
    {
        $id = (int) $content->getId();
        
        if ($id == 0) {
            $this->getEntityManager()->persist($content);
        }
        
        $this->getEntityManager()->flush();
    }

    public function remove(Content $content)
    {
        $this->getEntityManager()->remove($content);
        $this->getEntityManager()->flush();
    }

    public function removeContent($ids)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->delete('ContentManager\Model\Entity\Content', 'c')
                    ->where($qb->expr()->in('c.id', '?1'))
                    ->getQuery();

        $count = $query->setParameter(1, $ids)
                       ->execute();

        return $count;
    }

    public function removePageContent($ids, $removeChildrenContent = false)
    {
        if ($removeChildrenContent) {
            $pageModel = $this->getServiceLocator()->get('Pages\Model\Pages');

            foreach ($ids as $id) {
                $childrenIds = $pageModel->getPageChildrenIds($id, true);
                $ids = array_merge($ids, $childrenIds);
            }
        }

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->delete('ContentManager\Model\Entity\Content', 'c')
                    ->where($qb->expr()->in('c.page', '?1'))
                    ->getQuery();

        $count = $query->setParameter(1, $ids)
                        ->execute();

        return $count;
    }
}
