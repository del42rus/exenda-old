<?php
namespace ContentManager\Model\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="content")
 * @property string $alias
 * @property string $title
 * @property int $id
 */
class Content
{
    protected $inputFilter;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer");
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\OneToOne (targetEntity="Pages\Model\Entity\Page", fetch="EAGER")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", nullable=true)
     */
    protected $page;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated;


    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page = null)
    {
        $this->page = $page;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreated($format = 'd.m.Y H:i')
    {
        return $this->created->format($format);
    }

    public function getUpdated($format = 'd.m.Y H:i')
    {
        return $this->updated->format($format);
    }

    public function isDetached()
    {
        return $this->page ? false : true;
    }
    
    public function detach()
    {
        $this->getPage()->setContent(null);
        $this->page = null;
    }

    public function populate($data = [])
    {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->text = $data['text'];
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function __clone()
    {
        $this->id = null;
    }
}