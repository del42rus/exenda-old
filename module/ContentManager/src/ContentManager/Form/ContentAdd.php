<?php
namespace ContentManager\Form;

use Application\Form\Form;

class ContentAdd extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');

        $this->add([
            'name' => 'title',
            'options' => [
                'label' => gettext('Title')
            ],
            'attributes' => [
                'type' => 'text',
                'id' => 'title',
                'placeholder' => gettext('Title'),
                'required' => 'required'
            ]
        ]);

        $this->add([
            'name' => 'text',
            'type' => 'Zend\Form\Element\Textarea',
            'options' => [
                'label' => gettext('Text')
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Save'),
            ]
        ]);

        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'     => 'title',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 255,
                    ],
                ],
            ],
        ]);
    }
}

