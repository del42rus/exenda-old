<?php
namespace ContentManager\View\Helper;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

use Zend\View\Helper\AbstractHelper;

class Content extends AbstractHelper implements ServiceLocatorAwareInterface
{
    public function __invoke($id)
    {
        $pm = $this->getServiceLocator();
        $sl = $pm->getServiceLocator();

        $contentManager = $sl->get('ContentManager\Model\ContentManager');

        $content = $contentManager->findContentById($id);

        return $content;
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}