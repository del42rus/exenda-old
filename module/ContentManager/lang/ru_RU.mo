��          �      L      �     �     �  ,   �  6     &   <     c  *   s  
   �     �     �     �     �     �     �  #   �                           (   0  H   Y  \   �  /   �  !   /  7   Q     �  )   �  '   �     �  !   �       
   -  2   8  $   k     �     �            	                              
                                                          Action Add new content Are you sure that you want to delete content Are you sure that you want to delete selected content? Content has been updated successfully. Content manager Content with id &laquo;%s&raquo; not found Created at Edit content Last update No Remove selected Save Text The form was completed with errors. Tied to the page Title Yes Project-Id-Version: Content manager
POT-Creation-Date: 2015-04-17 12:10+0700
PO-Revision-Date: 2015-04-17 12:10+0700
Last-Translator: Ilya <coder4@web-axioma.ru>
Language-Team: ilya <coder4@web-axioma.ru>
Language: Russian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;translate
X-Poedit-Basepath: /var/www/skidka.me/www/module
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ContentManager
 Действие Создать новый контент Вы дествительно хотите удалить контент Вы дествительно хотите удалить выбранный контент? Контент успешно обновлен. Менеджер контента Контент с id &laquo;%s&raquo; не найден Дата создания Редактировать контент Последнее обновление No Удалить выбранные Сохранить Текст Форма заполнена с ошибками. Привязна к странице Заголовок Да 