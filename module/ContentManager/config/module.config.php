<?php
namespace ContentManager;

use ContentManager\Controller\AdminController;
use ContentManager\Controller\IndexController;

return [
    'controllers' => [
        'factories' => [
            'ContentManager\Controller\Index' => function($cpm) {
                return new IndexController($cpm->getServiceLocator()->get('ContentManager\Model\ContentManager'));
            },
            'ContentManager\Controller\Admin' => function($cpm) {
                return new AdminController($cpm->getServiceLocator()->get('ContentManager\Model\ContentManager'));
            },
        ],
        'aliases' => [
            'Content\Controller\Index' => 'ContentManager\Controller\Index',
            'Content\Controller\Admin' => 'ContentManager\Controller\Admin'
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            'ContentManager' => __DIR__ . '/../view',
        ],
    ],
    
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/..',
            ],
        ],
    ],

    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ]
    ],

    'navigation' => [
        'admin' => [
            [
                'label' => 'Content manager',
                'route' => 'admin_default',
                'permission' => 'content-manager.view',
                'order' => 3,
                'params' => [
                    'module' => 'content-manager',
                    'action' => 'index'
                ],
                'class' => 'fa fa-file-text',
                'pages' => [
                    [
                        'route' => 'admin_default',
                        'params' => [
                            'module' => 'content-manager',
                            'action' => 'add'
                        ],
                        'visible' => false
                    ],
                    [
                        'route' => 'admin_default/wildcard',
                        'params' => [
                            'module' => 'content-manager',
                            'action' => 'edit'
                        ],
                        'visible' => false
                    ],
                ],
            ],
        ],
    ],
];
