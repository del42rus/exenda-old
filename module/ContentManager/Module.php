<?php
namespace ContentManager;

use ContentManager\Model\ContentManager;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap($e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'injectTitle']);
    }

    public function injectTitle(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        $viewModel = $e->getViewModel();

        if (!$viewModel->getVariable('title') &&
            str_replace(['-', '_'], '', strtolower($matches->getParam('module'))) == strtolower(__NAMESPACE__)) {
            $serviceLocator = $e->getApplication()->getServiceManager();
            $translator = $serviceLocator->get('translator');

            $title = $translator->translate('Content manager', 'ContentManager');
            $viewModel->setVariable('title', $title);
        }
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'ContentManager\Model\ContentManager' =>  function($sm) {
                    return new ContentManager();
                },
            ],
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'content' => function($sm) {
                    $helper = new View\Helper\Content();

                    return $helper;
                },
            ]
        ];
    }

    public function getModuleName()
    {
        return 'Content';
    }
}
