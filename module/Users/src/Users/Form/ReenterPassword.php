<?php
namespace Users\Form;

use Application\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator\Regex as RegexValidator;

class ReenterPassword extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => [
                'label' => gettext('Password'),
            ],
            'attributes' => [
                'required' => 'required',
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'confirm',
            'options' => [
                'label' => gettext('Confirm password'),
            ],
            'attributes' => [
                'required' => 'required',
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Save')
            ],
        ]);

        $inputFilter = $this->getInputFilter();

        $validator = new \Zend\Validator\StringLength(['min' => 8, 'encoding' => 'UTF-8']);

        $validator->setMessage('The password length is less than %min% characters long', \Zend\Validator\StringLength::TOO_SHORT);

        $inputFilter->get('password')
                    ->getValidatorChain()
                    ->addValidator($validator);
    }

    public function getInputFilterSpecification()
    {
        return [
            'password' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty'
                    ]
                ],
            ],
            'confirm' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty'
                    ],
                    [
                        'name' => 'Users\Form\Validator\PasswordVerification',
                    ]
                ],
            ],
        ];
    }
}