<?php
namespace Users\Form\Validator;

use Zend\Validator\AbstractValidator;
use Application\Mvc\Model\AbstractModel as Model;

class UniqueLogin extends AbstractValidator
{
    const LOGIN_EXISTS = 'loginExists';

    protected $messageTemplates = array(
        self::LOGIN_EXISTS => "Login '%value%' already exists in the system"
    );

    /**
     * @var \Users\Model\Users
     */
    protected $model;

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $user = $this->model->getUserByLogin($value);

        if (null === $user || $user->getId() == $context['id']) {
            return true;
        }

        $this->error(self::LOGIN_EXISTS);
        return false;
    }

    public function setModel(Model $model)
    {
        $this->model = $model;
    }
}