<?php
namespace Users\Form\Validator;

use Zend\Validator\AbstractValidator;
use Application\Mvc\Model\AbstractModel as Model;

class UniqueEmail extends AbstractValidator
{
    const EMAIL_EXISTS = 'emailExists';

    protected $messageTemplates = array(
        self::EMAIL_EXISTS =>  "Email '%value%' already exists in the system"
    );

    /**
     * @var \Users\Model\Users
     */
    protected $model;

    /**
     * @var
     */
    protected $userClass;

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $user = $this->model->getUserByEmail($value, $this->getUserClass());

        if (null === $user || $user->getId() == $context['id']) {
            return true;
        }

        $this->error(self::EMAIL_EXISTS);
        return false;
    }

    /**
     * Set model
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    public function getUserClass()
    {
        return $this->userClass;
    }

    public function setUserClass($userClass)
    {
        $this->userClass = $userClass;
    }
}