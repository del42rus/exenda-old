<?php
/**
 * Exenda CMS
 * 
 * @category Zend
 * @package Users_Form_Validator
 */
namespace Users\Form\Validator;

use Zend\Validator\AbstractValidator;

/**
 * @category Zend
 * @package Xend_Validator
 */
class PasswordVerification extends AbstractValidator
{ 
    const NOT_MATCH = 'notMatch';
    
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::NOT_MATCH => 'Passwords do not match'
    );
    
    /**
     * Constructor
     * 
     * @param array $options
     */
    public function __construct($options = array()) {
        parent::__construct($options);
    }
    
    /**
     * Return true if passwords are matched
     * 
     * @param string $value
     * @param array $context
     * @return boolean
     */
    public function isValid($value, $context = null) 
    {
        $this->setValue($value);

        if (is_array($context)) {
            if ($value == $context['password']) {
                return true;
            }
        } elseif (is_string($context) && ($value == $context)) {
            return true;            
        }
            
        $this->error(self::NOT_MATCH);
        return false;
    }
}
