<?php
namespace Users\Form\Validator;

use Zend\Validator\AbstractValidator;
use Application\Mvc\Model\AbstractModel as Model;

class EmailExists extends AbstractValidator
{
    const EMAIL_NOT_FOUND = 'emailNotFound';

    protected $messageTemplates = array(
        self::EMAIL_NOT_FOUND =>  "Email '%value%' doesn't exists in the system",
    );

    /**
     * @var \Users\Model\Users
     */
    protected $model;
    protected $userClass;

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $user = $this->model->getUserByEmail($value, $this->getUserClass());

        if (null !== $user) {
            return true;
        }

        $this->error(self::EMAIL_NOT_FOUND);
        return false;
    }

    /**
     * Set model
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    public function getUserClass()
    {
        return $this->userClass;
    }

    public function setUserClass($userClass)
    {
        $this->userClass = $userClass;
    }
}