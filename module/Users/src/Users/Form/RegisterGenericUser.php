<?php
namespace Users\Form;

use Application\Form\Form;
use Users\Form\Element\RoleSelect;
use Zend\InputFilter\InputFilterProviderInterface;

class RegisterGenericUser extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');

        $this->add([
            'type' => 'Zend\Form\Element\File',
            'name' => 'avatar',
            'options' => [
                'label' => gettext('Avatar')
            ]
        ]);

        $this->add([
            'name' => 'name',
            'options' => [
                'label' => gettext('Name'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => [
                'label' => gettext('Email'),
            ],
            'attributes' => [
                'required' => 'required',
            ]
        ]);
        
        $this->add([
            'name' => 'login',
            'options' => [
                'label' => gettext('Login'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);
        
        
        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => [
                'label' => gettext('Password'),
            ],
            'attributes' => [
                'required' => 'required',
                'autocomplete' => 'off'
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'confirm',
            'options' => [
                'label' => gettext('Confirm password'),
            ],
            'attributes' => [
                'required' => 'required',
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'birthday',
            'options' => [
                'label' => gettext('Birthday'),
            ],
        ]);

        $translator = $this->getServiceLocator()->get('translator');

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'gender',
            'options' => [
                'label' => gettext('Gender'),
                'empty_option' => $translator->translate('Please choose your gender', 'Users'),
                'value_options' => [
                    'male' => $translator->translate('Male', 'Users'),
                    'female' => $translator->translate('Female', 'Users'),
                ],
            ],
        ]);

        $user = $this->getOption('user');
        $identity = $this->getOption('identity');

        if (!$user || $user != $identity) {
            $roleSelect = new RoleSelect('roleId', [
                'user' => $this->getOption('user'),
                'service_manager' => $this->getServiceLocator(),
            ]);

            $roleSelect->setLabel(gettext('Select roles'));
            $roleSelect->setAttribute('multiple', true);
            $this->add($roleSelect);

            $this->add([
                'type' => 'Zend\Form\Element\Radio',
                'name' => 'active',
                'options' => [
                    'label' => gettext('Is active?'),
                    'value_options' => [
                        0 => $translator->translate('No', 'Users'),
                        1 => $translator->translate('Yes', 'Users'),
                    ],
                ],
                'attributes' => [
                    'value' => 0,
                ]
            ]);

            $this->add([
                'type' => 'Zend\Form\Element\Radio',
                'name' => 'blocked',
                'options' => [
                    'label' => gettext('Is blocked?'),
                    'value_options' => [
                        0 => $translator->translate('No', 'Users'),
                        1 => $translator->translate('Yes', 'Users'),
                    ],
                ],
                'attributes' => [
                    'value' => 0,
                ]
            ]);
        }

        $this->add([
            'name' => 'id',
            'attributes' => [
                'type'  => 'hidden',
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Add user'),
            ]
        ]);

        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'roleId',
            'required' => false,
            'validators' => []
        ]);

        $validator = new \Zend\Validator\StringLength(['min' => 8, 'encoding' => 'UTF-8']);

        $validator->setMessage('The password length is less than %min% characters long', \Zend\Validator\StringLength::TOO_SHORT);

        $inputFilter->get('password')
                    ->getValidatorChain()
                    ->addValidator($validator);
    }

    public function getInputFilterSpecification()
    {
        $model = $this->getModel();

        return [
            'avatar' => [
                'type' => 'Zend\InputFilter\FileInput',
                'required' => false,
                'validators' => [
                    [
                        'name' => 'fileextension',
                        'options' => [
                            'extension' => ['jpeg', 'jpg', 'png'],
                            'case' => false,
                        ],
                    ],
                    [
                        'name' => 'fileimagesize',
                        'options' => [
                            'minWidth' => 100,
                            'minHeight' => 100,
                            'maxWidth' => 640,
                            'maxHeight' => 480,
                        ]
                    ],
                    [
                        'name' => 'filesize',
                        'options' => [
                            'min' => '10kB',
                            'max' => '8MB'
                        ],
                    ],
                ],
            ],
            'name' => [
               'required' => false,
               'filters'  => [
                   ['name' => 'StripTags'],
                   ['name' => 'StringTrim'],
               ],
               'validators' => [
                   [
                       'name'    => 'StringLength',
                       'options' => [
                           'encoding' => 'UTF-8',
                           'min'      => 1,
                           'max'      => 100,
                       ],
                   ],
               ],
            ],
            'login' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ],
                    ],
                    [
                        'name' => 'Users\Form\Validator\UniqueLogin',
                        'options' => [
                            'model' => $model
                        ]
                    ],
                ],
            ],
            'email' => [
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Users\Form\Validator\UniqueEmail',
                        'options' => [
                            'model' => $model,
                            'userClass' => 'GenericUser'
                        ]
                    ],
                ],
            ],
            'password' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty'
                    ]
                ],
            ],
            'confirm' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty'
                    ],
                    [
                        'name' => 'Users\Form\Validator\PasswordVerification',
                    ]
                ],
            ],
            'birthday' => [
                'required' => false,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'Date',
                        'options' => [
                            'format' => 'd/m/Y',
                        ],
                    ],
                ],
            ],
            'gender' => [
                'required' => false,
            ],
            'active' => [
                'required' => false,
            ],
            'blocked' => [
                'required' => false,
            ]
        ];
    }
}
