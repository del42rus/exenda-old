<?php
namespace Users\Form;

class RegisterCustomUser extends RegisterGenericUser
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);

        $this->add(array(
            'name' => 'attribute',
            'type' => 'text',
            'options' => array(
                'label' => gettext('Some user attribute'),
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));

        $inputFilter = $this->getInputFilter();

        $inputFilter->add(array(
            'name'     => 'attribute',
            'required' => false,
            'filters'  => array(),
            'validators' => array(),
        ));
    }
}