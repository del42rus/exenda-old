<?php
namespace Users\Form\Element;

use Zend\Form\Element\Select;
use Zend\InputFilter\InputProviderInterface;

class RoleSelect extends Select implements InputProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $user = $this->getOption('user');
        $sm = $this->getOption('service_manager');

        $accessManager = $sm->get('AccessManager\Model\AccessManager');
        $roles = $accessManager->getRoles();

        $values = [];

        foreach ($roles as $role) {
            $values[$role->getId()] = $role->getName();
        }

        $this->setValueOptions($values);

        if (null !== $user) {
            $selected = [];

            foreach ($user->getRoles() as $role) {
                $selected[] = $role->getId();
            }

            $this->setValue($selected);
        }
    }

    public function getInputSpecification()
    {
        return [
            'required' => false,
        ];
    }
}