<?php
namespace Users\Form;

use Application\Form\Form;
use Zend\Validator\StringLength;
use Zend\InputFilter\InputFilterProviderInterface;

class Login extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = []) {
        parent::__construct($name, $options);
        
        $this->setAttribute('method', 'post');
        
        $this->add([
            'name' => 'login',
            'options' => [
                'label' => gettext('Login'),
            ],
            'attributes' => [
                'type' => 'text',
                'required' => 'required',
                'id' => 'inputLogin',
                'placeholder' => 'Login',
                'class' => 'form-control'
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password',
            'options' => [
                'label' => gettext('Password'),
            ],
            'attributes' => [
                'required' => 'required',
                'id' => 'inputPassword',
                'placeholder' => 'Password',
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'rememberMe',
            'options' => [
                'use_hidden_element' => false,
                'label' => gettext('Remember me'),
            ],
            'attributes' => [
                'id' => 'rememberMe',
            ]
        ]);
        
        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Log in'),
                'class' => 'btn btn-warning pull-right'
            ]
        ]);
        
        $inputFilter = $this->getInputFilter();
        $validator = new StringLength(['min' => 8,
                                            'encoding' => 'UTF-8']);

        $validator->setMessage('The password length is less than %min% characters long', 
                                StringLength::TOO_SHORT);

        $inputFilter->get('password')
                    ->getValidatorChain()
                    ->addValidator($validator);
    }

    public function getInputFilterSpecification()
    {
        return [
            [
                'name'     => 'login',
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name'    => 'StringLength',
                        'options' => [
                            'encoding' => 'UTF-8',
                            'min'      => 2,
                            'max'      => 100,
                        ],
                    ],
                ],
            ],
            [
                'name'     => 'password',
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'NotEmpty',
                    ]
                ],
            ],
            [
                'name' => 'rememberMe',
                'required' => false,
            ]
        ];
    }
}
