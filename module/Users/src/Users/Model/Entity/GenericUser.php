<?php
namespace Users\Model\Entity;

use Rbac\Role\RoleInterface;
use ZfcRbac\Identity\IdentityInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

use DateTime;
use Doctrine\Common\Collections\Criteria;
/**
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminator", type="string")
 * @ORM\DiscriminatorMap({"generic_user" = "GenericUser"})
 * @ORM\Table(name="users")
 *
 * If you want to extend this class you need add one more discriminator value
 * in DiscriminatorMap({"generic_user" = "GenericUser", "custom_user" = "CustomUser"})
 */
class GenericUser implements IdentityInterface
{
    const AVATAR_HEIGHT = 96;
    const AVATAR_WIDTH = 96;
    const AVATAR_PATH = '/images/avatars/';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $login;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $gender;

    /**
     * @ORM\Column(type="date")
     */
    private $birthday;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     */
    private $salt;

    /**
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @ORM\Column(type="string")
     */
    private $series;

    /**
     * @ORM\Column(name="recovery_hash", type="string")
     */
    private $recoveryHash;

    /**
     * @ORM\ManyToMany(targetEntity="AccessManager\Model\Entity\Role")
     * @ORM\JoinTable(name="users_roles",
     *      joinColumns={@ORM\JoinColumn(name="user_id",
     * referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id",
     * referencedColumnName="id")})
     */
    private $roles;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $active;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $blocked;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="string")
     */
    private $avatar;

    public function __construct()
    {
        $this->active = 0;
        $this->blocked = 0;
        $this->roles = new ArrayCollection();
    }

    public function getArrayCopy()
    {
        $data = get_object_vars($this);
        $data['birthday'] = $this->getBirthday('d/m/Y');
        unset($data['roles']);

        return $data;
    }

    public function populate($data = [])
    {
        $this->id        = $data['id'];
        $this->name      = $data['name'];
        $this->email     = $data['email'];
        $this->login     = $data['login'];
        $this->gender    = $data['gender'];
        $this->active    = $data['active'] !== null ? $data['active'] : $this->active;
        $this->blocked    = $data['blocked'] !== null ? $data['blocked'] : $this->blocked;

        if (!empty($data['password'])) {
            $this->setPassword($data['password']);
        }
    }

    public function __get($property)
    {
        return $this->$property;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($value)
    {
        $this->login = $value;
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($value)
    {
        $this->email = $value;
        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    public function getBirthday($format = 'Y-m-d')
    {
        if ($this->birthday instanceof DateTime) {
            return $this->birthday->format($format);
        }

        return null;
    }

    public function setBirthday($birthday, $format = 'Y-m-d')
    {
        if (!empty($birthday) && is_string($birthday)) {
            $birthday = DateTime::createFromFormat($format, $birthday);
            $birthday->format('Y-m-d');
        }

        $this->birthday = $birthday instanceof DateTime ? $birthday : null;
        return $this;
    }

    public function setPassword($value)
    {
        $this->salt = $this->generateSalt();
        $this->password = md5($this->salt . $value);
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    private function generateSalt($length = 8)
    {
        $salt = null;

        $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";

        $strlen = strlen($chars);

        for ($i = 0; $i < $length; $i++) {
            $salt .= $chars[rand(0, $strlen)];
        }

        return $salt;
    }

    public function generatePassword($length = 8, $onlyDigits = false)
    {
        $password = null;

        if ($onlyDigits) {
            $chars = '1234567890';
        } else {
            $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
        }

        $strlen = strlen($chars) - 1;

        for ($i = 0; $i < $length; $i++) {
            $password .= $chars[rand(0, $strlen)];
        }

        $this->setPassword($password);

        return $password;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function setSeries($series)
    {
        $this->series = $series;
        return $this;
    }

    public function setActive($active)
    {
        $this->active = (int) $active;
    }

    /**
     * @param mixed $recoveryHash
     */
    public function setRecoveryHash($recoveryHash)
    {
        $this->recoveryHash = $recoveryHash;
    }

    /**
     * @return mixed
     */
    public function getRecoveryHash()
    {
        return $this->recoveryHash;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function isActive()
    {
        return (bool) $this->active;
    }

    /**
     * @return mixed
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * @param mixed $blocked
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;
    }

    public function isBlocked()
    {
        return (bool) $this->blocked;
    }

    public function getDiscriminator()
    {
        return $this->discriminator;
    }

    /**
     * @return mixed
     */
    public function getCreated($format = 'd.m.Y H:i')
    {
        return $this->created->format($format);
    }

    public function getUpdated($format = 'd.m.Y H:i')
    {
        return $this->updated->format($format);
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return self::AVATAR_PATH . $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function hasAvatar()
    {
        return (bool) $this->avatar;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles)
    {
        $this->roles->clear();

        foreach ($roles as $role) {
            $this->roles->add($role);
        }
    }

    public function addRole(RoleInterface $role)
    {
        $this->roles->add($role);
    }

    public function removeRole(RoleInterface $role)
    {
        $this->roles->removeElement($role);
    }

    public function hasRole($name)
    {
        $criteria = Criteria::create()->where(
            Criteria::expr()->eq('name', $name)
        );

        $result = $this->roles->matching($criteria);
        return count($result) > 0;
    }
}
