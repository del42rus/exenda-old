<?php
namespace Users\Model;

use Application\Mvc\Model\AbstractModel;
use Zend\Filter\Word\UnderscoreToCamelCase;
use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

use Doctrine\ORM\Query;
use Users\Model\Entity\GenericUser as User;

class Users extends AbstractModel
{
    public function getUsers($paged = null, $itemCountPerPage = 10, $order = [], $discriminator = null)
    {
        if (null !== $discriminator) {
            $filter = new UnderscoreToCamelCase();
            $entityName = $filter->filter($discriminator);

            $inflector = new \Zend\Filter\Inflector(':namespace\Entity\:name');

            $inflector->setRules([
                'namespace' => $this->getNamespace(),
                ':name'  => [new UnderscoreToCamelCase()],
            ]);

            $className = $inflector->filter(['name' => $entityName]);

            $em = $this->getEntityManager();
            $qb = $em->createQueryBuilder();

           $qb->select('u')
                ->from('Users\Model\User', 'u')
                ->where('u INSTANCE OF ' . $className);

            if (!empty($order)) {
                $qb->orderBy("u.{$order['column']}", $order['dir']);
            }

            $query = $qb->getQuery();

            if (null === $paged) {
                return $query->getResult();
            }
        }

        if (null !== $paged) {
            if (null === $discriminator) {
                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->select('u')
                    ->from('Users\Model\Entity\GenericUser', 'u');

                if (!empty($order)) {
                    $qb->orderBy("u.{$order['column']}", $order['dir']);
                }

                $query = $qb->getQuery();
            }

            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);
            
            $paginator->setCurrentPageNumber((int) $paged)
                      ->setItemCountPerPage($itemCountPerPage)
                      ->setPageRange(5);
            
            return $paginator;
        }

        return $this->getEntity('GenericUser')->findAll();
    }

    public function getUserById($id)
    {
        return $this->getEntity('GenericUser')->find($id);
    }

    public function getUserByEmail($email)
    {
        return $this->getEntity('GenericUser')->findOneByEmail($email);
    }

    public function getUserByLogin($login)
    {
        return $this->getEntity('GenericUser')->findOneByLogin($login);
    }

    public function saveUser(User $user)
    {        
        $id =  $user->getId();

        if (0 ==  $id) {
            $this->getEntityManager()->persist($user);
        }

        $this->getEntityManager()->flush();
    }

    public function deleteUser(User $user)
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }
    
    public function deleteUsers($ids = [])
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->delete('Users\Model\Entity\User', 'u')
            ->where($qb->expr()->in('u.id', '?1'))
            ->getQuery();

        $count = $query->setParameter(1, $ids)
                    ->execute();

        return $count;
    }

    public function setUserRoles(User $user, $roles = [])
    {
        $accessManager = $this->getServiceLocator()->get('AccessManager\Model\AccessManager');
        $userRoles = $user->getRoles();

        if (count($userRoles) && count($roles)) {
            foreach ($userRoles as $role) {
                $ids[] = $role->getId();
            }

            $forDetach = array_diff($ids, $roles);
            $forAttach = array_diff($roles, $ids);

            if (count($forDetach)) {
                $roles = $accessManager->getRolesByIds($forDetach);

                foreach ($roles as $role) {
                    $user->removeRole($role);
                }
            }

            if (count($forAttach)) {
                $roles = $accessManager->getRolesByIds($forAttach);

                foreach ($roles as $role) {
                    $user->addRole($role);
                }
            }
        } else {
            if (count($roles)) {
                $roles = $accessManager->getRolesByIds($roles);
            }

            $user->setRoles($roles);
        }
    }
}

