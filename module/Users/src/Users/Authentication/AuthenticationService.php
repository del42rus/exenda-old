<?php
namespace Users\Authentication;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

use Zend\Authentication\AuthenticationServiceInterface;

class AuthenticationService implements ServiceLocatorAwareInterface, AuthenticationServiceInterface
{
    /**
     * Performs an authentication attempt
     * 
     * @param array $credentials
     * @return boolean
     */
    public function authenticate($credentials = [])
    {
        $authService = $this->getAuth();

        $adapter = $authService->getAdapter();

        $identityProperty = $this->getAuth()->getAdapter()->getIdentityProperty();
        $credentialProperty = $this->getAuth()->getAdapter()->getCredentialProperty();

        $adapter->setIdentityValue($credentials[$identityProperty]);
        $adapter->setCredentialValue($credentials[$credentialProperty]);
        
        $this->result = $result = $authService->authenticate();

        if ($result->isValid()) {
            return true;  
        }
        
        return false;
    }
    
    /**
     * Returns authentication service
     * 
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuth()
    {
        return $this->getServiceLocator()->get('doctrine.authenticationservice.default');
    }
    
    /**
     * Returns the identity from storage or null if no identity is available
     *
     * @return mixed|null
     */
    public function getIdentity()
    {        
        $authService = $this->getAuth();
        
        if ($authService->hasIdentity()) {
            return $authService->getIdentity();
        }
        
        return null;
    }

    /**
     * Clears the identity from persistent storage
     *
     * @return void
     */
    public function clearIdentity()
    {
        $authService = $this->getAuth();
        $authService->clearIdentity();
    }

    public function hasIdentity()
    {
        $authService = $this->getAuth();
        return $authService->hasIdentity();
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getStorage()
    {
        return $this->getAuth()->getStorage();
    }
    
    /**
     * Sets service locator
     * 
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator 
     * 
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setCredentialProperty($credentialProperty)
    {
        $this->getAuth()->getAdapter()->setCredentialProperty($credentialProperty);
    }

    public function setIdentityProperty($identityProperty)
    {
        $this->getAuth()->getAdapter()->setIdentityProperty($identityProperty);
    }

    public function setIdentityClass($identityClass)
    {
        $this->getAuth()->getAdapter()->setIdentityClass($identityClass);
    }
}
