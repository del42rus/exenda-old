<?php
namespace Users\Authentication\Adapter;

use DoctrineModule\Authentication\Adapter\ObjectRepository as DoctrineAuthenticationAdapter;

class ObjectRepository extends DoctrineAuthenticationAdapter
{
    public function setCredentialProperty($credentialProperty)
    {
        $this->options->setCredentialProperty($credentialProperty);
    }

    public function setIdentityProperty($identityProperty)
    {
        $this->options->setIdentityProperty($identityProperty);
    }

    public function getCredentialProperty()
    {
        return $this->options->getCredentialProperty();
    }

    public function getIdentityProperty()
    {
        return $this->options->getIdentityProperty();
    }

    public function setIdentityClass($identityClass)
    {
        $this->options->setIdentityClass($identityClass);
    }

    public function getIdentityClass()
    {
        return $this->options->getIdentityClass();
    }
}