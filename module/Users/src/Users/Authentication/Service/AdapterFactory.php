<?php
namespace Users\Authentication\Service;

use Users\Authentication\Adapter\ObjectRepository;
use DoctrineModule\Service\AbstractFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Factory to create authentication adapter object.
 */
class AdapterFactory extends AbstractFactory
{
    /**
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return \DoctrineModule\Authentication\Adapter\DoctrineObjectRepository
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator, 'authentication');
        if (is_string($options->getObjectManager())) {
            $options->setObjectManager($serviceLocator->get($options->getObjectManager()));
        }
        return new ObjectRepository($options);
    }

    public function getOptionsClass()
    {
        return 'DoctrineModule\Options\Authentication';
    }
}
