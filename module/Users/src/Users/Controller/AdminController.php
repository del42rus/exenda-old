<?php
namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Users\Model\Users as UserModel;

use Zend\View\Model\ViewModel;
use Users\Model\Entity\GenericUser as User;

use Zend\Http\Header\SetCookie;
use ZfcRbac\Exception\UnauthorizedException;

use Application\Filter\File\ResizeUpload;

class AdminController extends AbstractActionController
{
    public function __construct(UserModel $model)
    {
        $this->model = $model;
    }

    public function indexAction()
    {
        if (!$this->isGranted('users.view')) {
            throw new UnauthorizedException();
        }

        $page = $this->params()->fromRoute('page', 1);
        $users = $this->model->getUsers($page);

        return new ViewModel([
            'users' => $users
        ]);
    }

    public function loginAction()
    {
        $this->layout()->setTemplate('layout/admin/login');

        if ($this->identity() && $this->isGranted('application.cpanel')) {
            $this->redirect()->toRoute('admin_default');
        }

        $form = $this->model->getForm('Login');

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function authenticateAction()
    {
        $this->layout()->setTemplate('layout/admin/login');

        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->redirect()->toRoute('admin_auth', ['action' => 'login']);
        }

        $form = $this->model->getForm('Login');

        $form->setData($request->getPost());

        if (!$form->isValid()) {
            $viewModel =  new ViewModel(['form' => $form]);
            $viewModel->setTemplate('users/admin/login');

            return $viewModel;
        }

        $authService = $this->getServiceLocator()->get('AuthenticationService');
        $translator = $this->getServiceLocator()->get('translator');

        $rememberMe = (bool) $request->getPost('rememberMe');

        $credentials = $request->getPost();

        $emailValidator = new \Zend\Validator\EmailAddress();

        if ($emailValidator->isValid($credentials['login'])) {
            $authService->setIdentityProperty('email');
            $credentials['email'] = $credentials['login'];
        }

        $result = $authService->authenticate($credentials);

        if ($result) {
            $user = $authService->getIdentity();

            if ($user->isActive()) {
                if ($rememberMe) {
                    $domain = $this->getRequest()->getUri()->getHost();

                    $cookie = new SetCookie();

                    $token = md5(uniqid());
                    $series = md5(md5(uniqid()));

                    $value = $token . ':' . $series . ':' . $user->getEmail();

                    $cookie->setValue($value);
                    $cookie->setName('auth');
                    $cookie->setDomain($domain);
                    $cookie->setPath('/');
                    $cookie->setExpires(time() + 60*60*24*30);

                    $this->getResponse()->getHeaders()->addHeader($cookie);

                    $user->setToken($token);
                    $user->setSeries($series);
                }

                $password = $request->getPost('password');

                // Re-generate salt and password hash for safety
                $user->setPassword($password);

                $this->model->saveUser($user);
            } else {
                $authService->clearIdentity();
                $error = $translator->translate('Your account is not activated.', 'Users');

                $viewModel =  new ViewModel([
                    'form' => $form,
                    'error' => $error,
                ]);

                $viewModel->setTemplate('users/admin/login');
                return $viewModel;
            }
        } else {
            $error = $translator->translate('Invalid login or password.', 'Users');

            $viewModel =  new ViewModel([
                'form'     => $form,
                'error'  => $error,
            ]);

            $viewModel->setTemplate('users/admin/login');
            return $viewModel;
        }

        return $this->redirect()->toRoute('admin_default');
    }

    public function logoutAction()
    {
        $authService = $this->getServiceLocator()->get('AuthenticationService');
        $authService->clearIdentity();

        // If rememberMe cookie exists then remove it
        if (isset($this->getRequest()->getCookie()->auth)) {
            $cookie = new SetCookie();

            $domain = $this->getRequest()->getUri()->getHost();

            $cookie->setName('auth');
            $cookie->setDomain($domain);
            $cookie->setPath('/');
            $cookie->setExpires(time() - 3600);

            $this->getResponse()->getHeaders()->addHeader($cookie);
        }

        return $this->redirect()->toRoute('admin_default');
    }

    public function restorePasswordAction()
    {
        $this->layout()->setTemplate('layout/admin/restore-password');

        $request = $this->getRequest();

        $form = $this->model->getForm('RestorePassword');

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $formData = $form->getData(\Zend\Form\FormInterface::VALUES_AS_ARRAY);
                $email = $formData['email'];

                $translator = $this->getServiceLocator()->get('translator');

                $user = $this->model->getUserByEmail($email);

                if (!$user->isActive()) {
                    $error = $translator->translate('Sorry, but the account is not activated.', 'Users');
                } elseif ($user->isBlocked()) {
                    $error = $translator->translate('Sorry, but the account is blocked.', 'Users');
                } else {
                    $secret = md5(uniqid() . $email . $user->getId());

                    $user->setRecoveryHash($secret);
                    $this->model->saveUser($user);

                    $link = 'http://' . $request->getUri()->getHost() . "/admin/reenter-password?email={$email}&secret={$secret}";

                    $mail = $this->getServiceLocator()->get('Mail\Service\Mail');

                    $fields = [
                        'EMAIL' => $user->getEmail(),
                        'LINK' => $link,
                    ];

                    $mail->send('USER_PASS_RESTORE', $fields);

                    $message = $translator->translate('A mail has been sent to your email. Please, open it and follow the link to establish a new password.', 'Users');
                }
            }
        }

        return new ViewModel([
            'form' => $form,
            'message' => $message,
            'error' => $error
        ]);
    }

    public function reenterPasswordAction()
    {
        $this->layout()->setTemplate('layout/admin/reenter-password');

        $request = $this->getRequest();
        $form = $this->model->getForm('ReenterPassword', $request->getQuery()->toArray());

        $email = $request->getQuery('email');
        $secret = $request->getQuery('secret');

        if (!$email || !$secret) {
            $this->getResponse()->setStatusCode(404);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);

            return $viewModel;
        }

        $user = $this->model->getUserByEmail($email);

        if (null === $user || $user->getRecoveryHash() !== $secret) {
            $this->getResponse()->setStatusCode(404);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);

            return $viewModel;
        }

        $translator = $this->getServiceLocator()->get('translator');

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                if (!$user->isActive()) {
                    $error = $translator->translate('Sorry, but the account is not activated.', 'Users');
                } elseif ($user->isBlocked()) {
                    $error = $translator->translate('Sorry, but the account is blocked.', 'Users');
                } else {
                    $user->setPassword($request->getPost('password'));
                    $user->setRecoveryHash(null);

                    $this->model->saveUser($user);

                    $credentials['email'] = $email;
                    $credentials['password'] = $request->getPost('password');

                    $authService = $this->getServiceLocator()->get('AuthenticationService');
                    $authService->setIdentityProperty('email');
                    $result = $authService->authenticate($credentials);

                    if ($result) {
                        return $this->redirect()->toRoute('admin_default');
                    } else {
                        $error = $translator->translate('Invalid login or password.', 'Users');
                    }
                }
            }
        }

        return new ViewModel([
            'form' => $form,
            'error' => $error
        ]);
    }

    public function addUserAction()
    {
        if (!$this->isGranted('users.add-user')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();
        $form = $this->model->getForm('RegisterGenericUser');

        if ($request->isPost()) {
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $user = new User();

            $form->setData($post);

            $validator = new \Zend\Validator\File\UploadFile();

            if ($form->isValid()) {
                $user->populate($form->getData());

                if ($birthday = $request->getPost('birthday')) {
                    $user->setBirthday($birthday, 'd/m/Y');
                }

                $roleIds = $request->getPost('roleId') ?: [];
                $this->model->setUserRoles($user, $roleIds);

                if ($validator->isValid($post['avatar'])) {
                    $target = DOCUMENT_ROOT . User::AVATAR_PATH;
                    $filter = new ResizeUpload($target);

                    // Avatar size
                    $filter->setHeight(User::AVATAR_HEIGHT);
                    $filter->setWidth(User::AVATAR_WIDTH);

                    // Avatar filename
                    $filter->setRandomize(true);
                    $filter->setUseUploadName(false);
                    $filter->setUseUploadExtension(true);

                    $filtered = $filter->filter($post['avatar']);
                    $filename = basename($filtered['tmp_name']);

                    $user->setAvatar($filename);
                }

                $this->model->saveUser($user);

                return $this->redirect()->toRoute('admin/users/edit-user', ['id' => $user->getId()]);
            } else {
                $form->get('avatar')->setValue(null);

                $rootViewModel = $this->getEvent()->getViewModel();
                $translator = $this->getServiceLocator()->get('translator');

                $text = $translator->translate('The form was completed with errors.', 'Users');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
            }
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }

    public function editUserAction()
    {
        $request = $this->getRequest();
        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $user = $this->model->getUserById($id);

        if (!$this->isGranted('users.edit-user', $user)) {
            throw new UnauthorizedException();
        }

        $translator = $this->getServiceLocator()->get('translator');

        if (null != $user) {
            $form = $this->model->getForm('RegisterGenericUser', [
                'user' => $user,
                'identity' => $this->identity(),
            ]);

            $form->bind($user);

            $rootViewModel = $this->getEvent()->getViewModel();

            if ($request->isPost()) {
                $post = array_merge_recursive(
                    $request->getPost()->toArray(),
                    $request->getFiles()->toArray()
                );

                $form->setData($post);

                if (!$request->getPost('confirm') && !$request->getPost('password')) {
                    $form->getInputFilter()->remove('password');
                    $form->getInputFilter()->remove('confirm');
                }

                $validator = new \Zend\Validator\File\UploadFile();

                if ($form->isValid()) {
                    if ($birthday = $request->getPost('birthday')) {
                        $user->setBirthday($birthday, 'd/m/Y');
                    }

                    if ($user != $this->identity()) {
                        $roleIds = $request->getPost('roleId') ?: [];
                        $this->model->setUserRoles($user, $roleIds);
                    }

                    if ($validator->isValid($post['avatar'])) {
                        $target = DOCUMENT_ROOT . User::AVATAR_PATH;
                        $filter = new ResizeUpload($target);

                        // Avatar size
                        $filter->setHeight(User::AVATAR_HEIGHT);
                        $filter->setWidth(User::AVATAR_WIDTH);

                        // Avatar filename
                        $filter->setRandomize(true);
                        $filter->setUseUploadName(false);
                        $filter->setUseUploadExtension(true);

                        $filtered = $filter->filter($post['avatar']);
                        $filename = basename($filtered['tmp_name']);

                        if ($user->hasAvatar()) {
                            @unlink(DOCUMENT_ROOT . $user->getAvatar());
                        }

                        $user->setAvatar($filename);
                        $form->get('avatar')->setValue($filename);

                    } elseif ($post['isDeleted']) {
                        if ($user->hasAvatar()) {
                            @unlink(DOCUMENT_ROOT . $user->getAvatar());
                            $user->setAvatar(null);

                            $form->get('avatar')->setValue(null);
                        }
                    }

                    $this->model->saveUser($user);

                    $text = $translator->translate('User has been updated successfully.', 'Users');
                    $rootViewModel->setVariable('alert', ['type' => 'success', 'text' => $text]);
                } else {
                    $text = $translator->translate('The form was completed with errors.', 'Users');
                    $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
                }

                if ($user->hasAvatar()) {
                    $form->get('avatar')->setValue($user->avatar);
                } else {
                    $form->get('avatar')->setValue(null);
                }
            }
        } else {
            $message = sprintf($translator->translate('User with id &laquo;%s&raquo; not found', 'Users'), $id);

            $viewModel = new ViewModel([
                'message' => $message,
            ]);
            $viewModel->setCaptureTo('error');
            $viewModel->setTemplate('error/error');

            return $viewModel;
        }

        return new ViewModel([
            'form' => $form
        ]);
    }


    public function editProfileAction()
    {
        return new ViewModel();
    }

    public function deleteAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id || !$user = $this->model->getUserById($id)) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $translator = $this->getServiceLocator()->get('translator');

        if (!$request->isXmlHttpRequest()) {
            if (!$this->isGranted('users.delete-user')) {
                throw new UnauthorizedException();
            }

            $identity = $this->identity();

            // User can't remove yourself
            if ($identity->getId() == $user->getId()) {
                $message = $translator->translate('You can\'t remove yourself', 'Users');

                $viewModel = new ViewModel([
                    'message' => $message
                ]);
                $viewModel->setCaptureTo('error');
                $viewModel->setTemplate('error/error');

                return $viewModel;
            }
        }

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()) {

            $this->model->deleteUser($user);

            return $this->redirect()->toRoute('admin_default', ['module' => 'users']);
        }

        $message = $translator->translate('Are you sure that you want to delete the user', 'Users');

        if ($user->getLogin()) {
            $message .= '&nbsp;&laquo;' . $user->getLogin() . '&raquo; ?';
        } elseif ($user->getEmail()) {
            $message .= '&nbsp;&laquo;' . $user->getEmail() . '&raquo; ?';
        } else {
            $message .= '?';
        }

        return new ViewModel([
            'url' => $request->getPost('url'), // get from ajax data
            'user' => $user,
            'message' => $message
        ]);
    }

    public function deleteSelectedAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        $translator = $this->getServiceLocator()->get('translator');

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            if (!$this->isGranted('users.delete-user')) {
                throw new UnauthorizedException();
            }

            $this->model->deleteUsers($ids);
            $this->redirect()->toRoute('admin_default', ['module' => 'users']);
        }

        $message = $translator->translate('Are you sure that you want to delete selected users?', 'Users');

        $viewModel = new ViewModel([
            'ids'     => $ids,
            'message' => $message,
            'url'     => $request->getPost('url') // get from ajax
        ]);

        return $viewModel;
    }
}