<?php
namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Users\Model\Users as UserModel;

class IndexController extends AbstractActionController
{
    public function __construct(UserModel $model)
    {
        $this->model = $model;
    }
}