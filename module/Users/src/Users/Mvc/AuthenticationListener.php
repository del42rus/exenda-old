<?php
namespace Users\Mvc;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router;

use Zend\Http\Header\SetCookie;

class AuthenticationListener implements ListenerAggregateInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = [];

    /**
     * Attach listeners to an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 10000);
    }

    /**
     * Detach listeners from an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }
    
    /**
     * Redirect to login action
     * 
     * @param \Zend\Mvc\MvcEvent $e
     * @return void
     */
    public function onDispatch(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        $request = $e->getRequest();

        $response = $e->getResponse();

        if (!$matches instanceof Router\RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        if (strpos($matches->getMatchedRouteName(), 'admin') === false ||
            in_array($matches->getParam('action'), ['authenticate', 'login', 'restore-password', 'reenter-password'])
        ) {
            return;
        }


        $serviceLocator = $e->getApplication()->getServiceManager();
        $authService = $serviceLocator->get('AuthenticationService');

        if (isset($request->getCookie()->auth)
            && null == $authService->getIdentity())
        {
            $value = $request->getCookie()->auth;

            list($token, $series, $email) = explode(':', $value);

            $userModel = $serviceLocator->get('Users\Model\Users');

            $user = $userModel->getUserByEmail($email);

            if (null !== $user && $user->getSeries() == $series) {
                if ($user->getToken() == $token) {
                    $token = md5(uniqid());

                    $domain = $request->getUri()->getHost();

                    $cookie = new SetCookie();

                    // Update token value in the cookie
                    $value = $token . ':' . $series . ':' . $user->getEmail();

                    $cookie->setValue($value);
                    $cookie->setName('auth');
                    $cookie->setDomain($domain);
                    $cookie->setPath('/');
                    $cookie->setExpires(time() + 60*60*24*30);

                    $response->getHeaders()->addHeader($cookie);

                    $user->setToken($token);
                    $userModel->saveUser($user);

                    $authService->getStorage()->write($user);
                } else {
                    // send message about hacking to user
                }
            }
        }

        $authorizationService = $serviceLocator->get('ZfcRbac\Service\AuthorizationService');

        if (!$authService->getIdentity() || !$authorizationService->isGranted('application.cpanel')) {
            $router = $e->getRouter();
            $uri = $router->assemble(['action' => 'login'], ['name' => 'admin_auth']);
            $response = $e->getResponse();

            $response->getHeaders()->addHeaderLine('Location', $uri);
            $response->setStatusCode(302);

            $e->stopPropagation();
            $e->setResponse($response);
            $e->setResult($response);
        }
    }
}
