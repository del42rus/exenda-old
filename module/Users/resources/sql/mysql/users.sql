CREATE TABLE IF NOT EXISTS `ex_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `login` varchar(40),
  `email` varchar(60),
  `birthday` date,
  `gender` varchar(6) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `token` varchar(32),
  `series` varchar(32),
  `recovery_hash` varchar(32),
  `discriminator` varchar(255),
  `active` int(1) NOT NULL DEFAULT 0,
  `blocked` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;
