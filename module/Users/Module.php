<?php
namespace Users;

use Users\Model\Users;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Users\Authentication;

use DoctrineModule;
use Zend\Mvc\Router;
use Zend\Mvc\MvcEvent;

use Users\Mvc\AuthenticationListener;

class Module implements ViewHelperProviderInterface, ServiceProviderInterface
{
    public function onBootstrap($e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'injectTitle']);
        $serviceLocator = $app->getServiceManager();

        $moduleManager = $serviceLocator->get('ModuleManager');
        $modules = $moduleManager->getModules();

        if (!in_array('Installation', $modules)) {
            $authListener = new AuthenticationListener();
            $authListener->attach($eventManager);
        }
    }

    public function injectTitle(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        $viewModel = $e->getViewModel();

        if (!$viewModel->getVariable('title') &&
            str_replace(['-', '_'], '', strtolower($matches->getParam('module'))) == strtolower(__NAMESPACE__)) {
            $serviceLocator = $e->getApplication()->getServiceManager();
            $translator = $serviceLocator->get('translator');

            $title = $translator->translate('Users', 'Users');
            $viewModel->setVariable('title', $title);
        }
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'Users\Model\Users' =>  function($sm) {
                    return new Users();
                },
                'doctrine.authenticationadapter.default'  => new Authentication\Service\AdapterFactory('default'),
                'doctrine.authenticationstorage.default'  => new DoctrineModule\Service\Authentication\StorageFactory('default'),
                'doctrine.authenticationservice.default'  => new DoctrineModule\Service\Authentication\AuthenticationServiceFactory('default'),
            ],
            'aliases' => [
                'Zend\Authentication\AuthenticationService' => 'AuthenticationService',
            ],
            'invokables' => [
                'AuthenticationService' => 'Users\Authentication\AuthenticationService'
            ],
        ];
    }
    
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'identity' => 'Users\View\Helper\Service\IdentityFactory'
            ]
        ];
    }
}
