<?php
namespace Users;

return [
    'routes' => [
        'admin' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin',
                'defaults' => [
                    'module' => 'Application',
                    'controller' => 'Admin',
                    'action' => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes' => [
                'users' => [
                    'type' => 'literal',
                    'options' => [
                        'route' => '/users',
                        'defaults' => [
                            'module' => __NAMESPACE__,
                            'action' => 'index',
                        ]
                    ],
                    'may_terminate' => true,
                    'child_routes' => [
                        'add' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/add',
                                'defaults' => [
                                    'action' => 'add-user',
                                ],
                            ],
                        ],
                        'edit' => [
                            'type' => 'segment',
                            'options' => [
                                'route' => '/edit/:id',
                                'constraints' => [
                                    'id' => '\d+',
                                ],
                                'defaults' => [
                                    'action' => 'edit-user'
                                ]
                            ],
                        ],
                        'edit-profile' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/edit-profile',
                                'defaults' => [
                                    'action' => 'edit-profile'
                                ]
                            ],
                        ],
                        'delete' => [
                            'type' => 'segment',
                            'options' => [
                                'route' => '/delete/:id',
                                'constraints' => [
                                    'id' => '\d+',
                                ],
                                'defaults' => [
                                    'action' => 'delete'
                                ]
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];