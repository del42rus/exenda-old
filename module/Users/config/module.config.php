<?php
namespace Users;

use Users\Controller\AdminController;
use Users\Controller\IndexController;

return [
    'router' => include "router.config.php",

    'controllers' => [
         'factories' => [
            'Users\Controller\Index' => function($cpm) {
                return new IndexController($cpm->getServiceLocator()->get('Users\Model\Users'));
            },
            'Users\Controller\Admin' => function($cpm) {
                return new AdminController($cpm->getServiceLocator()->get('Users\Model\Users'));
            },
        ]
    ],

    'controller_plugins' => [
        'factories' => [
            'Users\Controller\Plugin\Identity' => 'Users\Factory\IdentityPluginFactory'
        ],
        'aliases' => [
            'identity' => 'Users\Controller\Plugin\Identity'
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],

    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
        
        'authentication' => [
            'default' => [
                'objectManager' => 'doctrine.entitymanager.orm_default',
                'identityClass' => 'Users\Model\Entity\GenericUser',
                'identityProperty' => 'login',
                'credentialProperty' => 'password',
                'credentialCallable' => function($identity, $credentialValue) {
                    return md5($identity->getSalt() . $credentialValue);
                }
            ],
        ],
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/..',
            ],
        ],
        'caching' => [
            'default' => [
                'cache'     => 'Filesystem',  // Apc, FilePath, FileSystem etc.
                'options' => [
                    'dir' => __DIR__ . '/../../../data/cache/assets', // path/to/cache
                ],
            ],
        ],
    ],
    
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],

    'navigation' => [
        'admin' => [
            [
                'label' => 'Users',
                'route' => 'admin_default',
                'class' => 'fa fa-users',
                'permission' => 'users.view',
                'order' => 4,
                'params' => [
                    'module' => 'users',
                ],
                'pages' => [
                    [
                        'route' => 'admin/users/add',
                        'visible' => false,
                    ],
                    [
                        'route' => 'admin/users/edit',
                        'visible' => false,
                    ],
                ]
            ],
        ],
    ],
];
