<?php
namespace Application;

use Application\Mvc\TranslatorListener;
use Application\Mvc\ModuleRouteListener;

use Application\Mvc\View\Http\XmlHttpRequestListener;

use Application\Mvc\View;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;

use Zend\Mvc\Router;

use Application\Model\Application;

use Zend\Console\Request as ConsoleRequest;

use Zend\Session\SessionManager;
use Zend\Session\Container;

use Application\View\Helper;

class Module implements ServiceProviderInterface,
    ViewHelperProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $request = $e->getRequest();

        $app = $e->getApplication();
        $serviceLocator = $app->getServiceManager();
        $eventManager  = $app->getEventManager();

        $moduleManager = $serviceLocator->get('ModuleManager');
        $modules = $moduleManager->getModules();

        if (!in_array('Installation', $modules)) {
            $translatorListener = new TranslatorListener();
            $translatorListener->attach($eventManager);
        }

        if ($request instanceof ConsoleRequest) {
            return;
        }

        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager, -150);

        $xmlHttpRequestListener = new XmlHttpRequestListener();
        $sharedEvents = $eventManager->getSharedManager();
        $sharedEvents->attach('Zend\Stdlib\DispatchableInterface',
            MvcEvent::EVENT_DISPATCH, [$xmlHttpRequestListener, 'setTerminal'], -90);

        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'registerJsonStrategy'], 100);
        $this->bootstrapSession($e);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setLayout'], 100);
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setLayout'], 100);
    }

    public function setLayout(MvcEvent $e)
    {
        $viewModel = $e->getViewModel();
        $viewModel->setTemplate('layout/admin/layout');
    }

    /**
     * Register json strategy
     *
     * @param \Zend\Mvc\MvcEvent $e
     * @return void
     */
    public function registerJsonStrategy(MvcEvent $e)
    {
        $matches  = $e->getRouteMatch();

        if (!$matches instanceof RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        // Set the JSON strategy when controllers from this module are selected
        $app          = $e->getTarget();
        $locator      = $app->getServiceManager();
        $view         = $locator->get('Zend\View\View');
        $jsonStrategy = $locator->get('ViewJsonStrategy');

        // Attach strategy, which is a listener aggregate, at high priority
        $view->getEventManager()->attach($jsonStrategy, 100);
    }

    public function bootstrapSession($e)
    {
        $session = $e->getApplication()
            ->getServiceManager()
            ->get('Zend\Session\SessionManager');

        $session->start();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'Application\Model\Application' =>  function($sm) {
                    return new Application();
                },
                'Zend\Session\SessionManager' => function ($sm) {
                    $config = $sm->get('config');
                    if (isset($config['session'])) {
                        $session = $config['session'];

                        $sessionConfig = null;
                        if (isset($session['config'])) {
                            $class = isset($session['config']['class'])  ? $session['config']['class'] : 'Zend\Session\Config\SessionConfig';
                            $options = isset($session['config']['options']) ? $session['config']['options'] : [];
                            $sessionConfig = new $class();
                            $sessionConfig->setOptions($options);
                        }

                        $sessionManager = new SessionManager($sessionConfig);
                    } else {
                        $sessionManager = new SessionManager();
                    }

                    Container::setDefaultManager($sessionManager);
                    return $sessionManager;
                },
            ],
            'shared' => [
                'ControllerPluginManager' => true
            ],
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'config' => function($pm) {
                    $helper = new Helper\Config();

                    return $helper;
                },
                'wysiwyg' => '\Application\Form\View\Helper\Service\WysiwygFactory',
            ],
            'invokables' => [
                'translateNavigation' => 'Zend\I18n\View\Helper\Translate'
            ],
        ];
    }
}
