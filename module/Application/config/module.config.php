<?php
namespace Application;

return [
    'controllers' => [
        'invokables' => [
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Admin' => 'Application\Controller\AdminController',
        ],
    ],
    
    'router' => include "router.config.php",

    'service_manager' => [
        'factories' => [
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'Zend\I18n\Translator\TranslatorInterface' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation_admin' => 'Application\Navigation\Service\AdminNavigationFactory',
            'navigation_settings' => 'Application\Navigation\Service\SettingsNavigationFactory',
            'TablePrefix' => 'Application\DoctrineExtensions\Service\TablePrefixFactory',
        ],
    ],
    
    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'error/error' => __DIR__ . '/../view/error/error.phtml',
            'layout/layout' =>  __DIR__ . '/../view/layout/admin/layout.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
        'configuration' => [
            'orm_default' => [
                'string_functions' => [
                    // Register Mysql FIELD function
                    'FIELD' => 'DoctrineExtensions\Query\Mysql\Field'
                ],
                'custom_hydration_modes' => [
                    'FetchColumnHydrator' => 'Application\Doctrine\Internal\Hydration\FetchColumnHydrator'
                ],
                'date_time_functions' => [
                    'DATE_FORMAT' => 'Application\Doctrine\ORM\Query\AST\Functions\DateFormatFunction'
                ]
            ]
        ],
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    'TablePrefix',
                    'Gedmo\Tree\TreeListener',
                    'Gedmo\Sortable\SortableListener',
                    'Gedmo\Timestampable\TimestampableListener'
                ],
            ],
        ],
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/../assets',
            ],
        ],
        'caching' => [
            'default' => [
                'cache'     => 'Filesystem',  // Apc, FilePath, FileSystem etc.
                'options' => [
                    'dir' => __DIR__ . '/../../../data/cache/assets', // path/to/cache
                ],
            ],
        ],
    ],

    'navigation' => [
        'admin' => [
            [
                'label' => 'Dashboard',
                'route' => 'admin',
                'class' => 'fa fa-dashboard',
                'order' => 0,
            ],
        ],
        'settings' => [
            [
                'label' => 'Global configuration',
                'route' => 'admin/settings',
                'class' => 'fa fa-cogs',
                'order' => 1,
                'pages' => [
                    [
                        'label' => 'Site settings',
                        'route' => 'admin/settings/site',
                    ],
                    [
                        'label' => 'System settings',
                        'route' => 'admin/settings/system',
                    ],
                    [
                        'label' => 'Server settings',
                        'route' => 'admin/settings/server',
                    ],
                ],
            ],
        ],
    ],
];
