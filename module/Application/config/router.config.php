<?php
namespace Application;

return [
    'routes' => [
        'default' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '[/:module][/:action]',
                'constraints' => [
                    'module' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'action' => '^\s$|[a-zA-Z0-9_-]*',
                ],
                'defaults' => [
                    'module' => 'ContentManager',
                    'controller' => 'Index',
                    'action'     => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'wildcard' => [
                    'type' => 'wildcard',
                    'options' => [
                        'key_value_delimiter' => '/',
                        'param_delimiter' => '/',
                    ],
                    'may_terminate' => true,
                ],
            ],
        ],

        'admin_default' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin[/:module][/:action]',
                'constraints' => [
                    'lang' => '[a-zA-Z]{2}',
                    'module' => '[a-zA-Z0-9_-]*',
                    'action' => '[a-zA-Z0-9_-]*',
                ],
                'defaults' => [
                    'module' => 'Application',
                    'controller' => 'Admin',
                    'action'     => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'wildcard' => [
                    'type' => 'wildcard',
                    'may_terminate' => true,
                    'options' => [
                        'key_value_delimiter' => '/',
                        'param_delimiter' => '/'
                    ],
                ],
            ],
        ],

        'admin_auth' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin/:action',
                'constraints' => [
                    'action' => 'login|authenticate|logout',
                ],
                'defaults' => [
                    'module' => 'Users',
                    'controller' => 'Admin',
                ],
            ],
        ],

        'admin_lang' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin/:lang',
                'constraints' => [
                    'lang' => '[a-zA-Z]{2}',
                ],
                'defaults' => [
                    'module' => 'Application',
                    'controller' => 'Admin',
                    'action'     => 'set-language',
                ],
            ],
        ],

        'admin' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin',
                'defaults' => [
                    'module' => 'Application',
                    'controller' => 'Admin',
                    'action' => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes' => [
                'settings' => [
                    'type' => 'literal',
                    'options' => [
                        'route' => '/settings',
                        'defaults' => [
                            'module' => __NAMESPACE__,
                            'action' => 'index',
                        ]
                    ],
                    'may_terminate' => true,
                    'child_routes' => [
                        'site' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/site',
                                'defaults' => [
                                    'action' => 'site-settings',
                                ],
                            ],
                        ],
                        'system' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/system',
                                'defaults' => [
                                    'action' => 'system-settings',
                                ],
                            ],
                        ],
                        'server' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/server',
                                'defaults' => [
                                    'action' => 'server-settings',
                                ],
                            ],
                        ],
                        'permissions' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/permissions',
                                'defaults' => [
                                    'action' => 'permission-settings',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'priority' => 10
        ],
    ],
];