<?php
namespace Application\Navigation\Service;

use Zend\Navigation\Service\AbstractNavigationFactory;

class SettingsNavigationFactory extends AbstractNavigationFactory
{
    protected function getName()
    {
        return 'settings';
    }
}
