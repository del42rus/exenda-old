<?php
namespace Application\Model;

use Application\Model\Entity\Config;
use Application\Mvc\Model\AbstractModel;
use Zend\Stdlib\Glob;

use Zend\Filter\FilterChain;

class Application extends AbstractModel
{
    public function getModules()
    {
        $serviceModules = ['Application', 'DoctrineORMModule', 'DoctrineModule',
                                'Installation', 'Pages', 'AssetManager',
                                'AccessManager'];

        $moduleManager = $this->getServiceLocator()->get('ModuleManager');

        $modules = $moduleManager->getModules();
        $modules = array_diff($modules, $serviceModules);

        $filterChain = new FilterChain();
        $filterChain->attachByName('wordcamelcasetodash')
                    ->attachByName('stringtolower');

        foreach ($modules as $module) {
            if ($module == 'ContentManager') {
                $module = 'Content';
            }
            $_modules[$filterChain->filter($module)] = $module;
        }

        return $_modules;
    }

    /**
     * Get layout templates depending on current theme
     * 
     * @return array
     */
    public function getTemplates()
    {
        $templateDir = MODULE_PATH . '/Application/view/layout/index/';

        $templates = Glob::glob($templateDir . '*.phtml', 0);

        $_templates = [];
        
        foreach ($templates as $template) {
            $templateName = basename($template);
            $_templates[$templateName] = $templateName;
        }

        return $_templates;
    }

    public function setConfig($name, $value)
    {
        $configRepository = $this->getEntity('Config');

        $config = $configRepository->findOneByName($name);

        if (!$config) {
            $config = new Config();
            $config->setName($name);
            $this->getEntityManager()->persist($config);
        }

        $config->setValue($value);

        $this->getEntityManager()->flush();
    }

    public function getLanguage()
    {
        $language = null;

        $config = $this->getEntity('Config');
        $lang = $config->findOneByName('language');

        if ($lang) {
            $ident = $lang->getValue();

            $languages = $this->getLanguages();

            $language = $languages[$ident];
            $language['ident'] = $ident;
        }

        return $language;
    }

    public function getLanguages()
    {
        $serviceLocator = $this->getServiceLocator();
        $moduleManager = $serviceLocator->get('ModuleManager');

        $configListener = $moduleManager->getEvent()->getConfigListener();
        $config         = $configListener->getMergedConfig(false);

        return $config['languages'];
    }

    protected function writeArrayToFile($filePath, $array)
    {
        $content = "<?php\nreturn " . var_export($array, 1) . ';';
        file_put_contents($filePath, $content);
        return $this;
    }
}
