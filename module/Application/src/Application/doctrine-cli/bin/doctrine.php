<?php

chdir(__DIR__);

$previousDir = '.';

while (!file_exists('config/application.config.php')) {
    $dir = dirname(getcwd());

    $previousDir = $dir;
    chdir($dir);
}

require_once 'init_autoloader.php';
require_once 'vendor/doctrine/doctrine-module/bin/doctrine-module.php';
