<?php
namespace Application\Doctrine\Internal\Hydration;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;
use PDO;

class FetchColumnHydrator extends AbstractHydrator
{
    protected function hydrateAllData()
    {
        return $this->_stmt->fetchAll(PDO::FETCH_COLUMN);
    }
}