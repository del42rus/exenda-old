<?php
/**
 * Exenda CMS
 *
 * @category Zend
 * @package Appliccation_View
 */
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHtmlElement;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
/**
 * @category Zend
 * @package Application_View
 * @subpackage Helper
 */
class Config extends AbstractHtmlElement implements ServiceLocatorAwareInterface
{

    /**
     * @var \Application\Model\Application
     */
    private $appModel;

    /**
     * @return self
     */
    public function __invoke()
    {
        $helperPluginManager = $this->getServiceLocator();
        $serviceLocator = $helperPluginManager->getServiceLocator();

        $this->appModel = $serviceLocator->get('Application\Model\Application');

        return $this;
    }

    /**
     * Returns language list
     *
     * @param bool $attribs
     * @param bool $escape
     * @return string
     */
    public function getLanguages($attribs = false, $escape = true)
    {
        $items = $this->appModel->getLanguagePacks();

        $list = '';

        foreach ($items as $item) {
            $ident = $item['ident'];

            if ($escape) {
                $escaper = $this->view->plugin('escapeHtml');
                $item    = $escaper($item['name']);
            }
            $list .= "<li><a href='/administrator/{$ident}'>" . $item . '</a></li>' . self::EOL;
        }

        if ($attribs) {
            $attribs = $this->htmlAttribs($attribs);
        } else {
            $attribs = '';
        }

        $tag = 'ul';
        return '<' . $tag . $attribs . '>' . self::EOL . $list . '</' . $tag . '>' . self::EOL;
    }

    /**
     * Returns current language
     * @return \Application\Model\Entity\Language
     */
    public function getCurrentLanguage()
    {
        $lang = $this->appModel->getLanguage();
        return $lang['name'];
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
