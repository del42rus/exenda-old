<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class AdminController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function installAction()
    {
        $configPath = dirname(MODULE_PATH);
        rename($configPath . '/config/application.config.php', $configPath . '/config/application.config.php.dist');
        rename($configPath . '/config/application.config.php.tmp', $configPath . '/config/application.config.php');

        return $this->redirect()->toUrl('/installation/start');
    }
}
