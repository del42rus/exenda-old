<?php
namespace Application\Controller;

use Application\Mvc\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


class IndexController extends ActionController
{    public function indexAction()
    {
        $this->getResponse()->setStatusCode(404);
        return new ViewModel();
    }
}
