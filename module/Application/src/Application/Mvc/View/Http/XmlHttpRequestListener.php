<?php
/**
 * Exenda CMS
 * 
 * @category Zend
 * @package Xend_Mvc_View
 */
namespace Application\Mvc\View\Http;

use Zend\EventManager\EventManagerInterface as Events;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;

/**
 * @category Zend
 * @package Xend_Mvc_View
 * @subpackage Http
 */
class XmlHttpRequestListener implements ListenerAggregateInterface
{
    /**
     * Listeners we've registered
     *
     * @var array
     */
    protected $listeners = array();

    /**
     * Attach listeners
     *
     * @param  Events $events
     * @return void
     */
    public function attach(Events $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, array($this, 'setTerminal'), -90);
    }

    /**
     * Detach listeners
     *
     * @param  Events $events
     * @return void
     */
    public function detach(Events $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * Set view model terminal if request is XmlHttpRequest
     * 
     * @param \Zend\Mvc\MvcEvent $e
     */
    public function setTerminal(MvcEvent $e)
    {
        $request = $e->getRequest();

        if ($request->isXmlHttpRequest()) {
            $model = $e->getResult();
            $model->setTerminal(true);
        }
    }
    
}
