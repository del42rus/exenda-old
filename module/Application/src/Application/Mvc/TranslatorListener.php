<?php
namespace Application\Mvc;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router;

use Zend\Validator\AbstractValidator;
use Application\Form\Form;

class TranslatorListener implements ListenerAggregateInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = [];

    /**
     * Attach listeners to an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 10000);
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'onDispatch'], 10000);
    }

    /**
     * Detach listeners from an event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * Set locale to major translator
     * 
     * Create, configure and set own translator for AbstractValidator
     * 
     * @param \Zend\Mvc\MvcEvent $e
     * @return void
     */
    public function onDispatch(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();

        if (!$matches instanceof Router\RouteMatch) {
            return;
        }

        $serviceLocator = $e->getApplication()->getServiceManager();

        $appModel = $serviceLocator->get('Application\Model\Application');

        // Retrieve current language
        $lang = $appModel->getLanguage();
        $locale = $lang['locale'] ?: 'en_US';

        $em = $e->getApplication()->getServiceManager()->get('Doctrine\ORM\EntityManager');
        $conn = $em->getConnection();
        $conn->exec("SET lc_time_names = {$locale}");

        $mvcTranslator = $e->getApplication()->getServiceManager()->get('MvcTranslator');
        $translator = $e->getApplication()->getServiceManager()->get('translator');

        $mvcTranslator->setLocale($locale)
                      ->setFallbackLocale('en_US');

        $translator->setLocale($locale)
                   ->setFallbackLocale('en_US');

        $validatorTranslator = new \Zend\Mvc\I18n\Translator(new \Zend\I18n\Translator\Translator());

        $validatorTranslator->setLocale($locale)
                            ->setFallbackLocale('en_US');

        $navigationTranslator = new \Zend\Mvc\I18n\Translator(new \Zend\I18n\Translator\Translator());

        $navigationTranslator->setLocale($locale)
                             ->setFallbackLocale('en_US');

        $moduleManager = $serviceLocator->get('ModuleManager');
        $modules = $moduleManager->getModules();

        $viewHelperManager = $serviceLocator->get('ViewHelperManager');

        foreach ($modules as $module) {
            $filename = MODULE_PATH . '/' . $module . '/lang/validate/' . $lang['ident'] . '/Zend_Validate.php';

            if (file_exists($filename)) {
                $validatorTranslator->addTranslationFile('phparray', $filename , 'default', $locale);
            }

            $filename = MODULE_PATH . '/' . $module. '/lang/navigation/' . $lang['ident'] . '/Zend_Navigation.php';

            // Configure navigation translator
            if (file_exists($filename)) {
                $navigationTranslator->addTranslationFile('phparray', $filename, 'default', $locale);
            }
        }

        AbstractValidator::setDefaultTranslator($validatorTranslator);

        $translateHelper    = $viewHelperManager->get('translateNavigation');
        $translateHelper->setTranslator($navigationTranslator);

        $navigationHelper = $viewHelperManager->get('navigation');
        $navigationHelper->setTranslator($navigationTranslator);
    }
}
