<?php
namespace Application\Mvc\Model;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Doctrine\ORM\EntityManager;

abstract class AbstractModel implements
    AbstractModelInterface,
    ServiceLocatorAwareInterface

{
    protected $_forms;

    /**
     * Get model namespace
     * @return string
     */
    protected function getNamespace()
    {
       return substr(get_class($this), 0, strrpos(get_class($this), '\\'));
    }

    protected function getModuleName()
    {
        return substr(get_class($this), 0, strpos(get_class($this), '\\'));
    }

    /**
     * Get entity repository
     * 
     * @param type $name
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getEntity($name)
    {
        $inflector = new \Zend\Filter\Inflector(':namespace\Entity\:name');
        
        $inflector->setRules([
            'namespace' => $this->getNamespace(),
            ':name'  => [new \Zend\Filter\Word\SeparatorToCamelCase('-')],
        ]);

        $name = $inflector->filter(['name' => $name]);

        $entity = $this->getEntityManager()->getRepository($name);
        return $entity;
    }

    /**
     * Get entity manager
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    /**
     * Sets entity manager
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @return void
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Sets service locator
     * 
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return void
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get root service manager
     * 
     * @return \Zend\ServiceManager\ServiceManager
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function getForm($name, $options = [])
    {
        if (!isset($this->_forms[$name])) {
            $inflector = new \Zend\Filter\Inflector(':module\Form\:name');

            $inflector->setRules([
                'module' => $this->getModuleName(),
                ':name'  => [new \Zend\Filter\Word\SeparatorToCamelCase('-')],
            ]);

            $class = $inflector->filter(['name' => $name]);

            $options['model'] = $this;
            $options['service_manager'] = $this->getServiceLocator();

            $this->_forms[$name] = new $class($name, $options);
        }

        return $this->_forms[$name];
    }
}
