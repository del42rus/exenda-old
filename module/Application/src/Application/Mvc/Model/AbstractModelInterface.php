<?php
/**
 * Exenda CMS
 * 
 * @category Zend
 * @package Xend_Mvc
 */
namespace Application\Mvc\Model;

use  Doctrine\ORM\EntityManager;

/**
 * Abstract model interface
 * 
 * @category Zend
 * @package Xend_Mvc
 * @subpackage Model
 */
interface AbstractModelInterface 
{
    /**
     * Get entity repository
     * 
     * @param type $name
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getEntity($name);
    
    /**
     * Get entity manager
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    
    public function getEntityManager();
    /**
     * Set entity manager
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @return void
     */
    public function setEntityManager(EntityManager $em);

    public function getForm($name);
}
