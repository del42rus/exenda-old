<?php
namespace Application\DoctrineExtensions\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Application\DoctrineExtensions\TablePrefix;

class TablePrefixFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Configuration');
        $params = $options['doctrine']['connection']['orm_default']['params'];
        $prefix = $params['prefix'];
        
        return new TablePrefix($prefix);
        
    }
}
?>
