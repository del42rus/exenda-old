<?php
namespace Application\Form;

use Zend\I18n\Translator\Translator;
use Application\Mvc\Model\AbstractModelInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Form extends \Zend\Form\Form
{
    public function setModel(AbstractModelInterface $model)
    {
        $this->options['model'] = $model;
    }

    public function getModel()
    {
        return $this->options['model'];
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->option['service_manager'] = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->options['service_manager'];
    }
}