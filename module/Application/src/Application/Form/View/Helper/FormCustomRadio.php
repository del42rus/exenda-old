<?php
namespace Application\Form\View\Helper;

use Zend\Form\ElementInterface;

class FormCustomRadio extends FormCustomMulticheckbox
{
    /**
     * Return input type
     *
     * @return string
     */
    protected function getInputType()
    {
        return 'radio';
    }

    /**
     * Get element name
     *
     * @param  ElementInterface $element
     * @return string
     */
    protected static function getName(ElementInterface $element)
    {
        return $element->getName();
    }
}