<?php
namespace Application\Form\View\Helper;

use Zend\Form\Element\Textarea;
use Zend\Form\View\Helper\AbstractHelper;

use Zend\View\Exception;
use CKEditor;
use CKFinder;

use Zend\Form\ElementInterface;
use Zend\Stdlib\ArrayUtils;

class FormWysiwyg extends AbstractHelper
{
    /**
     * Wysiwyg configuration
     * @var array
     */
    protected  $options = array();

    /**
     * Invoke helper as function
     *
     * Proxies to {@link render()}.
     *
     * @param  ElementInterface|null $element
     * @return string|FormElement
     */
    public function __invoke(ElementInterface $element = null)
    {
        if (!$element) {
            return $this;
        }

        return $this->render($element);
    }

    public function render(ElementInterface $element)
    {
        if (!$element instanceof Textarea) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s requires that the element is of type Zend\Form\Element\Textarea',
                __METHOD__
            ));
        }

        $name = $element->getName();
        if (empty($name) && $name !== 0) {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        $name = $element->getName();
        $value = $element->getValue();

        $ckeditor = new CKEditor();
        $ckeditor->basePath = '/admin/js/vendor/ckeditor/';

        CKFinder::SetupCKEditor($ckeditor, '/admin/js/vendor/ckfinder/');

        ob_start();

        echo $ckeditor->editor($name, $value, $this->options);


        $return =  ob_get_clean();

        return $return;
    }

    public function setOptions($options)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        } elseif (!is_array($options)) {
            throw new Exception\InvalidArgumentException(
                'The options parameter must be an array or a Traversable'
            );
        }

        $this->options = $options;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getOption($option)
    {
        if (!isset($this->options[$option])) {
            return null;
        }

        return $this->options[$option];
    }

    public function setOption($option, $value)
    {
        $this->options[$option] = $value;
        return $this;
    }

    public function toolbar($name)
    {
        $this->options['customConfig'] = "config/ckeditor_{$name}_config.js";
        return $this;
    }
}