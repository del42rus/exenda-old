<?php
namespace Application\Form\View\Helper\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Application\Form\View\Helper\FormWysiwyg;

class WysiwygFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     *
     * @return \Application\View\Helper\Navigation
     */
    public function createService(ServiceLocatorInterface $pm)
    {
        $services = $pm->getServiceLocator();

        $appModel = $services->get('Application\Model\Application');
        $language = $appModel->getLanguage();

        $wysiwyg = new FormWysiwyg();

        $wysiwyg->setOption('language', $language['ident']);
        $wysiwyg->setOption('uiColor', '#eeeeee');
        $wysiwyg->setOption('customConfig', 'config/ckeditor_full_config.js');

        return $wysiwyg;
    }
}