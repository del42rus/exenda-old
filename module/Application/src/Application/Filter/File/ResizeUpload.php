<?php
namespace Application\Filter\File;

use Zend\Filter\File\RenameUpload;
use Zend\Filter\Exception;

use WideImage;

class ResizeUpload extends RenameUpload
{
    /**
     * @var array
     */
    protected $options = [
        'use_upload_name'      => false,
        'use_upload_extension' => false,
        'overwrite'            => false,
        'randomize'            => false,
        'target'               => null,
        'width'                => null,
        'height'               => null,
        'thumbnail_target'     => null,
        'thumbnail_width'      => null,
        'thumbnail_height'     => null,
    ];

    protected $imageExtensions = ['jpeg', 'jpg', 'png', 'bmp'];

    protected $filtered;

    public function filter($value)
    {
        @mkdir($this->getTarget(), 0777, true);

        $filtered = parent::filter($value);
        $sourceinfo = pathinfo($filtered['name']);

        if (!in_array($sourceinfo['extension'], $this->imageExtensions)) {
            throw new Exception\InvalidArgumentException(
                sprintf("File '%s' is not supported image file.", $filtered['name'])
            );
        }

        $image = WideImage::load($filtered['tmp_name']);

        if ($this->getWidth() && $this->getHeight()) {
            $image->resize($this->getWidth(), $this->getHeight(), 'outside')
                  ->crop('center', 'center', $this->getWidth(), $this->getHeight())
                  ->saveToFile($filtered['tmp_name']);
        }

        if ($this->getThumbnailTarget() &&
            $this->getThumbnailWidth() && $this->getThumbnailHeight()) {
            @mkdir($this->getThumbnailTarget(), 0777, true);

            $image->resize($this->getThumbnailWidth(), $this->getThumbnailHeight(), 'outside')
                ->crop('center', 'center', $this->getThumbnailWidth(), $this->getThumbnailHeight())
                ->saveToFile($this->getThumbnailTarget() . '/' . basename($filtered['tmp_name']));
        }

        return $filtered;
    }

    public function setWidth($width)
    {
        $this->options['width'] = $width;
        return $this;
    }

    public function getWidth()
    {
        return $this->options['width'];
    }

    public function setHeight($height)
    {
        $this->options['height'] = $height;
        return $this;
    }

    public function getHeight()
    {
        return $this->options['height'];
    }

    public function setThumbnailTarget($thumbnailTarget)
    {
        if (null !== $thumbnailTarget && !is_string($thumbnailTarget)) {
            throw new Exception\InvalidArgumentException(
                'Invalid thumbnail target, must be a string'
            );
        }

        $this->options['thumbnail_target'] = $thumbnailTarget;
        return $this;
    }

    public function getThumbnailTarget()
    {
        return $this->options['thumbnail_target'];
    }

    public function setThumbnailWidth($width)
    {
        $this->options['thumbnail_width'] = $width;
        return $this;
    }

    public function getThumbnailWidth()
    {
        return $this->options['thumbnail_width'];
    }

    public function setThumbnailHeight($height)
    {
        $this->options['thumbnail_height'] = $height;
        return $this;
    }

    public function getThumbnailHeight()
    {
        return $this->options['thumbnail_height'];
    }
}