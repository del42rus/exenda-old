<?php
namespace AccessManager;

use AccessManager\Controller\AdminController;

return [
    'router' => include "router.config.php",

    'controllers' => [
        'factories' => [
            'AccessManager\Controller\Admin' => function($cpm) {
                return new AdminController($cpm->getServiceLocator()->get('AccessManager\Model\AccessManager'));
            }
        ]
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ =>  __DIR__ . '/../view',
        ],
    ],

    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/..',
            ],
        ],
        'caching' => [
            'default' => [
                'cache'     => 'Filesystem',  // Apc, FilePath, FileSystem etc.
                'options' => [
                    'dir' => __DIR__ . '/../../../data/cache/assets', // path/to/cache
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            // override ZfcRbac\Service\AuthorizationService
            // This custom AuthorizationService will be used in ZfcRbac isGranted controller plugin and view helper
            'ZfcRbac\Service\AuthorizationService'  => 'AccessManager\Factory\AuthorizationServiceFactory',
        ],
        'aliases' => [
            'AuthorizationService' => 'ZfcRbac\Service\AuthorizationService'
        ]
    ],

    'navigation' => [
        'admin' => [
            [
                'label' => 'Access manager',
                'route' => 'admin/access-manager',
                'class' => 'fa fa-lock',
                'permission' => 'access-manager.view',
                'order' => 1,
                'visible' => true,
                'pages' => [
                    [
                        'label' => 'Roles',
                        'route' => 'admin/access-manager',
                        'pages' => [
                            [
                                'route' => 'admin/access-manager/roles',
                                'visible' => false
                            ],
                            [
                                'route' => 'admin/access-manager/roles/add',
                                'visible' => false
                            ],
                            [
                                'route' => 'admin/access-manager/roles/edit',
                                'visible' => false
                            ],
                        ],
                    ],

                    [
                        'label' => 'Permissions',
                        'route' => 'admin/access-manager/permissions',
                    ],
                ],
            ],
        ],
    ],
];