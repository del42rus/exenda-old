<?php
namespace AccessManager;

return [
    'routes' => [
        'admin' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin',
                'defaults' => [
                    'module' => 'Application',
                    'controller' => 'Admin',
                    'action' => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes' => [
                'access-manager' => [
                    'type' => 'literal',
                    'options' => [
                        'route' => '/access-manager',
                        'defaults' => [
                            'module' => __NAMESPACE__,
                            'action' => 'index',
                        ]
                    ],
                    'may_terminate' => true,
                    'child_routes' => [
                        'permissions' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/permissions',
                                'defaults' => [
                                    'action' => 'permissions',
                                ],
                            ],
                        ],
                        'roles' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/roles',
                                'defaults' => [
                                    'action' => 'index',
                                ],
                            ],
                            'may_terminate' => true,
                            'child_routes' => [
                                'add' => [
                                    'type' => 'literal',
                                    'options' => [
                                        'route' => '/add',
                                        'defaults' => [
                                            'action' => 'add-role',
                                        ]
                                    ],
                                ],
                                'edit' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/edit/:id',
                                        'constraints' => [
                                            'id' => '\d+',
                                        ],
                                        'defaults' => [
                                            'action' => 'edit-role'
                                        ]
                                    ],
                                ],
                                'delete' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/delete/:id',
                                        'constraints' => [
                                            'id' => '\d+',
                                        ],
                                        'defaults' => [
                                            'action' => 'delete-role'
                                        ]
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ]
];