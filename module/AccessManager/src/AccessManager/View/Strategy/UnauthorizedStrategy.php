<?php
namespace AccessManager\View\Strategy;

use Zend\Http\Response as HttpResponse;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use ZfcRbac\Exception\UnauthorizedExceptionInterface;
use ZfcRbac\Guard\GuardInterface;
use ZfcRbac\Options\UnauthorizedStrategyOptions;

use ZfcRbac\View\Strategy\AbstractStrategy;

/**
 * This strategy renders a specific template when a user is unauthorized
 *
 * @author  Michaël Gallego <mic.gallego@gmail.com>
 * @licence MIT
 */
class UnauthorizedStrategy extends AbstractStrategy
{
    /**
     * @var UnauthorizedStrategyOptions
     */
    protected $options;

    /**
     * Constructor
     *
     * @param UnauthorizedStrategyOptions $options
     */
    public function __construct(UnauthorizedStrategyOptions $options)
    {
        $this->options = $options;
    }

    /**
     * @private
     * @param  MvcEvent $event
     * @return void
     */
    public function onError(MvcEvent $event)
    {
        // Do nothing if no error or if response is not HTTP response
        if (!($event->getParam('exception') instanceof UnauthorizedExceptionInterface)
            || ($event->getResult() instanceof HttpResponse)
            || !($event->getResponse() instanceof HttpResponse)
        ) {
            return;
        }

        $model = new ViewModel();
        $model->setCaptureTo('error');
        $model->setTemplate($this->options->getTemplate());

        switch ($event->getError()) {
            case GuardInterface::GUARD_UNAUTHORIZED:
                $model->setVariable('error', GuardInterface::GUARD_UNAUTHORIZED);
                break;

            default:
        }

        $response = $event->getResponse() ?: new HttpResponse();
        $response->setStatusCode(403);

        $event->setResponse($response);
        $event->setResult($model);
    }
}
