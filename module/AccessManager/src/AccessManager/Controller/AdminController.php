<?php
namespace AccessManager\Controller;

use ZfcRbac\Exception\UnauthorizedException;
use Zend\Mvc\Controller\AbstractActionController;;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

use AccessManager\Model\Entity\Role;
use AccessManager\Model\AccessManager as Model;

class AdminController extends AbstractActionController
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function indexAction()
    {
        if (!$this->isGranted('access-manager.view')) {
            throw new UnauthorizedException();
        }

        $page = $this->params()->fromRoute('page', 1);
        $roles = $this->model->getRoles($page, 20);

        return [
            'roles' => $roles,
        ];
    }

    public function addRoleAction()
    {
        if (!$this->isGranted('access-manager.add-role')) {
            throw new UnauthorizedException();
        }

        $form = $this->model->getForm('RoleAdd');

        $request = $this->getRequest();

        if ($request->isPost()) {
            $role = new Role();

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $role->populate($form->getData());

                $this->model->saveRole($role);

                return $this->redirect()->toRoute('admin/access-manager/roles/edit', ['id' => $role->getId()]);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    public function editRoleAction()
    {
        if (!$this->isGranted('access-manager.edit-role')) {
            throw new UnauthorizedException();
        }

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $role = $this->model->getRoleById($id);

        if ($role) {
            $form = $this->model->getForm('RoleAdd', ['role' => $role]);

            $form->bind($role);

            $request = $this->getRequest();
            if ($request->isPost()) {
                $form->setData($request->getPost());

                $rootViewModel = $this->getEvent()->getViewModel();
                $translator = $this->getServiceLocator()->get('translator');

                if ($form->isValid()) {
                    $this->model->saveRole($role);

                    $text = $translator->translate('Role has been updated successfully.', 'AccessManager');
                    $rootViewModel->setVariable('alert', ['type' => 'success', 'text' => $text]);
                } else {
                    $text = $translator->translate('The form was completed with errors.', 'AccessManager');
                    $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
                }
            }
        } else {
            $translator = $this->getServiceLocator()->get('translator');

            $message = sprintf($translator->translate('Role with id &laquo;%s&raquo; not found', 'AccessManager'), $id);

            $viewModel = new ViewModel(['message' => $message]);
            $viewModel->setCaptureTo('error');
            $viewModel->setTemplate('error/error');

            return $viewModel;
        }

        return new ViewModel(['form' => $form]);
    }

    public function deleteRoleAction()
    {
        if (!$this->isGranted('access-manager.delete-role')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id || !$role = $this->model->getRoleById($id)) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $translator = $this->getServiceLocator()->get('translator');

        if (!$request->isXmlHttpRequest()) {
            // Administrator role can't be removed
            if ($id == 1) {
                $message = sprintf($translator->translate('Role  &laquo;%s&raquo; can\'t be removed', 'AccessManager'), $role->getName());

                $viewModel = new ViewModel(['message' => $message]);
                $viewModel->setCaptureTo('error');
                $viewModel->setTemplate('error/error');

                return $viewModel;
            }
        }

        if ($request->isPost() && !$request->isXmlHttpRequest()) {
            $this->model->removeRole($role);

            return $this->redirect()->toRoute('admin_default', ['module' => 'access-manager', 'action' => 'roles']);
        }

        $message = $translator->translate('Are you sure that you want to delete role', 'AccessManager') .
            '&nbsp;&laquo;' . $role->getName() . '&raquo; ?';

        $viewModel = new ViewModel([
            'url'  => $request->getPost('url'), // get from ajax data
            'role'   => $role,
            'message' => $message,
        ]);

        $viewModel->setTemplate('access-manager/admin/delete');

        return $viewModel;
    }

    public function deleteRolesAction()
    {
        if (!$this->isGranted('access-manager.delete-role')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            if (count($ids)) {
                $this->model->removeRoles($ids);
            }

            $this->redirect()->toRoute('admin_default', ['module' => 'access-manager', 'action' => 'roles']);
        }

        $translator = $this->getServiceLocator()->get('translator');
        $message = $translator->translate('Are you sure that you want to delete selected roles?', 'AccessManager');

        $viewModel = new ViewModel([
            'ids'     => $ids,
            'message' => $message,
            'url'     => $request->getPost('url') // get from ajax
        ]);

        $viewModel->setTemplate('access-manager/admin/delete-selected');

        return $viewModel;
    }

    public function permissionsAction()
    {
        if (!$this->isGranted('access-manager.set-permissions')) {
            throw new UnauthorizedException();
        }

        $modules = $this->model->getModules();

        $permissions = [];
        $permissionIds = [];

        foreach ($modules as $module) {
            $permissions[$module] = $this->model->getModulePermissions($module);
            $permissionIds[$module] = $this->model->getModulePermissionIds($module);
        }

        // Exclude Administrator role, because Administrator can do everything
        $roles = $this->model->getRoles(null, null, [1]);

        return [
            'modules' => $modules,
            'permissions' => $permissions,
            'permissionIds' => $permissionIds,
            'roles' => $roles
        ];
    }

    public function setPermissionsAction()
    {
        if (!$this->isGranted('access-manager.set-permissions')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() || !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $newPermissions = $request->getPost('permissions');
        $module = $request->getPost('module');

        $this->model->setPermissions($newPermissions, $module);

        $translator = $this->getServiceLocator()->get('translator');
        $message = $translator->translate('Permission settings have been updated successfully.', 'AccessManager');

        return new JsonModel([
            'success' => 1,
            'message' => $message
        ]);
    }
}

