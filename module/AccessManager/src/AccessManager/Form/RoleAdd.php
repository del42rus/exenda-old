<?php
namespace AccessManager\Form;

use Application\Form\Form;

class RoleAdd extends Form
{
    public function __construct($name, $options = [])
    {
        parent::__construct($name, $options);

        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name',
                'required' => true
            ],
            'options' => [
                'label' => gettext('Role name')
            ]
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'Zend\Form\Element\Textarea',
            'options' => [
                'label' => gettext('Description')
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
        ]);

        $this->add([
            'type' => 'Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Add role'),
            ]
        ]);

        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name'     => 'name',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 100,
                    ],
                ],
                [
                    'name' => 'AccessManager\Form\Validator\UniqueRole',
                    'options' => [
                        'model' => $this->getModel()
                    ]
                ],
                [
                    'name' => 'NotEmpty'
                ],
            ],
        ]);
    }
}
