<?php
namespace AccessManager\Form\Validator;

use Zend\Validator\AbstractValidator;

class UniqueRole extends AbstractValidator
{
    const ROLE_EXISTS = 'roleExists';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::ROLE_EXISTS => "Role '%value%' already exists"
    );

    /**
     *
     * @var \AccessManager\Model\AccessManager
     */
    protected $model;

    public function __construct($options = null) {
        parent::__construct($options);
    }

    /**
     * Return true if there isn't the same page alias on the same level
     *
     * @param string $value
     * @return boolean
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $role = $this->model->getRoleByName($value);

        if (null === $role || $role->getId() == $context['id']) {
            return true;
        }

        $this->error(self::ROLE_EXISTS);
        return false;
    }

    /**
     * Set model
     * @param \Application\Mvc\Model\AbstractModel $model
     */
    public function setModel(\Application\Mvc\Model\AbstractModel $model)
    {
        $this->model = $model;
    }
}