<?php
namespace AccessManager\Service;

use ZfcRbac\Service\AuthorizationService as ZfcRbacAuthorizationService;

class AuthorizationService extends ZfcRbacAuthorizationService
{
    public function isGranted($permission, $context = null)
    {
        if ($this->getIdentity()->hasRole('Administrator')) {
            return true;
        }

        return parent::isGranted($permission, $context);
    }
}
