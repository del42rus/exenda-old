<?php
namespace AccessManager\Model;

use Application\Mvc\Model\AbstractModel;
use AccessManager\Model\Entity\Role;

use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class AccessManager extends AbstractModel
{
    public function getRoleById($id)
    {
        return $this->getEntity('Role')->findOneById($id);
    }

    public function getRoleByName($name)
    {
        return $this->getEntity('Role')->findOneByName($name);
    }

    public function getPermissionById($id)
    {
        return $this->getEntity('Permission')->findOneById($id);
    }

    public function getRoles($paged = null, $itemCountPerPage = 10, $exclude = [])
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $stmt = $qb->select('r')
            ->from('AccessManager\Model\Entity\Role', 'r');

        if (!empty($exclude)) {
            $stmt->where($qb->expr()->notIn('r.id', $exclude));
        }

        $query = $stmt->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                ->setItemCountPerPage($itemCountPerPage)
                ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    public function getRolesByIds($ids = [])
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        return $qb->select('r')
                ->from('AccessManager\Model\Entity\Role', 'r')
                ->where($qb->expr()->in('r.id', $ids))
                ->getQuery()
                ->getResult();
    }

    public function saveRole(Role $role)
    {
        $em = $this->getEntityManager();

        $id = $role->getId();

        if (0 == $id) {
            $em->persist($role);
        }

        $em->flush();
    }

    public function removeRole(Role $role)
    {
        $this->getEntityManager()->remove($role);
        $this->getEntityManager()->flush();
    }

    public function removeRoles($ids)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->delete('AccessManager\Model\Entity\Role', 'r')
                    ->where($qb->expr()->in('r.id', '?1'))
                    ->getQuery();

        $count = $query->setParameter(1, $ids)
                        ->execute();

        return $count;
    }

    public function getModules()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('DISTINCT p.module')
                    ->from('AccessManager\Model\Entity\Permission', 'p')
                    ->orderBy('p.module', 'ASC')
                    ->getQuery();

        return $query->getResult('FetchColumnHydrator');
    }

    public function getModulePermissions($module)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
                    ->from('AccessManager\Model\Entity\Permission', 'p')
                    ->where($qb->expr()->like('p.module', $qb->expr()->literal(ucfirst($module))))
                    ->getQuery();

        return $query->getResult();
    }

    public function getModulePermissionIds($module)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p.id')
                    ->from('AccessManager\Model\Entity\Permission', 'p')
                    ->where($qb->expr()->like('p.module', $qb->expr()->literal(ucfirst($module))))
                    ->getQuery();

        return $query->getResult('FetchColumnHydrator');
    }

    public function setPermissions($newPermissions, $module)
    {
        $roles = $this->getRoles();

        $permissions = [];

        foreach ($roles as $role) {
            $rolePermissions = $role->getPermissions($module);
            $newRolePermissions = $newPermissions[$role->getId()] ? array_keys($newPermissions[$role->getId()]) : [];

            foreach ($rolePermissions as $permission) {
                if (!in_array($permission->getId(), $newRolePermissions)) {

                    $role->removePermission($permission);
                }
            }

            foreach ($newRolePermissions as $permissionId) {
                $permissions[$permissionId] = $permissions[$permissionId] ?: $this->getPermissionById($permissionId);

                $permission = $permissions[$permissionId];

                if (!$role->hasPermission($permission)) {
                    $role->addPermission($permission);
                }
            }
        }

        $this->getEntityManager()->flush();
    }
}