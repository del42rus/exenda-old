<?php
namespace AccessManager\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use ZfcRbac\Permission\PermissionInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="permissions")
 */
class Permission implements PermissionInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $module;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $description;

    /**
     * Constructor
     */
    public function __construct($name, $module)
    {
        $this->name  = (string) $name;
        $this->module = $module;
    }

    /**
     * Get the permission identifier
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param null|string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        $inflector = new \Zend\Filter\Inflector(':module');
        $inflector->setRules([
            ':module'  => ['Word\CamelCaseToDash', 'StringToLower']
        ]);

        $module = $inflector->filter(['module' => $this->module]);

        return $module . '.' . $this->name;
    }
}
