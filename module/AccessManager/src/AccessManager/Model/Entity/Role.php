<?php
namespace AccessManager\Model\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Rbac\Role\RoleInterface;

use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="roles")
 */
class Role implements RoleInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=48, unique=true)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string")
     */
    protected $description;

    /**
     * @ORM\ManyToMany(targetEntity="Permission", fetch="LAZY")
     * @ORM\JoinTable(name="roles_permissions",
     *      joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="permission_id", referencedColumnName="id")}
     *      )
     **/
    protected $permissions;

    public function __get($property)
    {
        return $this->$property;
    }

    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * Init the Doctrine collection
     */
    public function __construct()
    {
        $this->permissions = new ArrayCollection();
    }

    /**
     * Get the role identifier
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the role name
     *
     * @param  string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = (string) $name;
    }

    /**
     * Get the role name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * {@inheritDoc}
     */
    public function addPermission(Permission $permission)
    {
        $this->permissions->add($permission);
    }

    public function removePermission(Permission $permission)
    {
        $this->permissions->removeElement($permission);
    }

    public function getPermissions($module = null)
    {
        if (null !== $module) {
            $criteria = Criteria::create()->where(
                Criteria::expr()->eq('module', $module)
            );

            return $this->permissions->matching($criteria);
        }

        return $this->permissions;
    }

    public function getPermissionIds($module = null)
    {
        $permissions = $this->permissions;

        if (null !== $module) {
            $criteria = Criteria::create()->where(
                Criteria::expr()->eq('module', $module)
            );

            $permissions = $this->permissions->matching($criteria);
        }

        $ids = [];

        foreach ($permissions as $permission) {
            $ids[] = $permission->getId();
        }

        return $ids;
    }

    /**
     * {@inheritDoc}
     */
    public function hasPermission($permission)
    {
        $permission = (string) $permission;
        list($module, $name) = explode('.', $permission);

        $filter = new \Zend\Filter\Word\DashToCamelCase();
        $moduleName = $filter->filter($module);

        $criteria = Criteria::create()->where(
            Criteria::expr()->andX(
                Criteria::expr()->eq('name', $name),
                Criteria::expr()->eq('module', $moduleName)
            )
        );

        $result   = $this->permissions->matching($criteria);

        return count($result) > 0;
    }

    public function populate($data = [])
    {
        $this->name = $data['name'];
        $this->description = $data['description'];
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
