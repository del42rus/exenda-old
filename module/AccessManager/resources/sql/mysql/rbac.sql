CREATE TABLE IF NOT EXISTS `ex_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL UNIQUE,
  `description` text,
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_users_roles` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `user_id` int(11) NOT NULL,
    `role_id` int(11) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_id`) REFERENCES `ex_users`(`id`) ON DELETE CASCADE,
    FOREIGN KEY (`role_id`) REFERENCES `ex_roles`(`id`) ON DELETE CASCADE
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_roles_permissions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_id` INT(11) NOT NULL,
  `permission_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`role_id`) REFERENCES `ex_roles`(`id`) ON DELETE CASCADE,
  FOREIGN KEY (`permission_id`) REFERENCES `ex_permissions`(`id`) ON DELETE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=utf8;




