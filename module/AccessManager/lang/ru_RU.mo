��          �   %   �      @     A     H     U  )   ^  4   �     �  	   �     �     �  
   �  3   �          &  '   6  #   ^  	   �  '   �     �     �  	   �  
   �  #   �  	   �       �       �     �       D   (  X   m     �  #   �     �            ;   (  "   d  !   �  ;   �  5   �       9   5     o     x     �     �  2   �     �     	                                                                                            
           	                       Action Add new role Add role Are you sure that you want to delete role Are you sure that you want to delete selected roles? Description Edit role Name No Permission Permission settings have been updated successfully. Permissions Remove selected Role  &laquo;%s&raquo; can't be removed Role has been updated successfully. Role name Role with id &laquo;%s&raquo; not found Roles Save Saving... Select all The form was completed with errors. User type Yes Project-Id-Version: AccessManager
POT-Creation-Date: 2015-04-10 14:46+0700
PO-Revision-Date: 2015-04-10 14:50+0700
Last-Translator: Ilya <coder4@web-axioma.ru>
Language-Team:  <coder4@web-axioma.ru>
Language: Russian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;translate
X-Poedit-Basepath: /var/www/skidka.me/www/module/
X-Poedit-SearchPath-0: AccessManager
 Действие Добавить роль Добавить роль Вы действительно хотите удалить роль Вы действительно хотите удалить выбранные роли? Описание Редактировать роль Название Нет Привилегия Настройки доступа были изменены Настройки доступа  Удалить выбранные Роль с id &laquo;%s&raquo; нельзя удалить Изменения успешно сохранены. Название роли Роль с id &laquo;%s&raquo; не существует Роли Сохранить Сохранение... Выбрать все Форма заполнена с ошибками. Тип пользователя Да 