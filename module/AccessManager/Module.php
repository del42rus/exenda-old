<?php
namespace AccessManager;

use AccessManager\Model\AccessManager;
use Zend\Mvc\MvcEvent;
use AccessManager\View\Strategy\UnauthorizedStrategy;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $moduleOptions = $app->getServiceManager()->get('ZfcRbac\Options\ModuleOptions');

        $eventManager->attach(
            new UnauthorizedStrategy($moduleOptions->getUnauthorizedStrategy())
        );

        $eventManager->getSharedManager()->attach(
            'Zend\View\Helper\Navigation\AbstractHelper',
            'isAllowed',
            ['AccessManager\View\Helper\Navigation\Listener\RbacListener', 'accept']
        );

        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'injectTitle']);
    }

    public function injectTitle(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        $viewModel = $e->getViewModel();

        if (!$viewModel->getVariable('title') &&
            str_replace(['-', '_'], '', strtolower($matches->getParam('module'))) == strtolower(__NAMESPACE__)) {
            $serviceLocator = $e->getApplication()->getServiceManager();
            $translator = $serviceLocator->get('translator');

            $title = $translator->translate('Access manager', 'AccessManager');
            $viewModel->setVariable('title', $title);
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'AccessManager\Model\AccessManager' =>  function($sm) {
                    return new AccessManager();
                },
            ]
        ];
    }
}
