<?php
namespace Mail;

use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap($e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'injectTitle']);
    }

    public function injectTitle(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        $viewModel = $e->getViewModel();

        if (!$viewModel->getVariable('title') &&
            str_replace(['-', '_'], '', strtolower($matches->getParam('module'))) == strtolower(__NAMESPACE__)) {
            $serviceLocator = $e->getApplication()->getServiceManager();
            $translator = $serviceLocator->get('translator');

            $title = $translator->translate('Templates', 'Mail');
            $viewModel->setVariable('title', $title);
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'Mail\Service\Mail' =>  function($sm) {
                    $mailService = new Service\Mail();
                    $mailService->setEncoding('utf-8');
                    return $mailService;
                },
                'Mail\Model\Mail' =>  function($sm) {
                    return new Model\Mail();
                },
            ],
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'mail' => function($sm) {
                    $helper = new View\Helper\Mail();

                    return $helper;
                },
            ]
        ];
    }
}
