<?php
namespace Mail;

return [
    'routes' => [
        'admin' => [
            'type'    => 'segment',
            'options' => [
                'route'    => '/admin',
                'constraints' => [
                    'lang' => '[a-zA-Z]{2}',
                ],
                'defaults' => [
                    'module' => 'Application',
                    'controller' => 'Admin',
                    'action' => 'index',
                ],
            ],
            'may_terminate' => true,
            'child_routes' => [
                'mail' => [
                    'type' => 'literal',
                    'options' => [
                        'route' => '/mail',
                        'defaults' => [
                            'module' => __NAMESPACE__,
                            'action' => 'index',
                        ],
                    ],
                    'may_terminate' => true,
                    'child_routes' => [
                        'settings' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/settings',
                                'defaults' => [
                                    'module' => __NAMESPACE__,
                                    'action' => 'settings',
                                ],
                            ],
                        ],
                        'templates' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/templates',
                                'defaults' => [
                                    'module' => __NAMESPACE__,
                                    'action' => 'index',
                                ],
                            ],
                            'may_terminate' => true,
                            'child_routes' => [
                                'add' => [
                                    'type' => 'literal',
                                    'options' => [
                                        'route' => '/add',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'add-template',
                                        ],
                                    ],
                                ],
                                'edit' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/edit/:id',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'edit-template',
                                        ],
                                        'constraints' => [
                                            'id' => '\d+'
                                        ],
                                    ],
                                ],
                                'delete' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/delete/:id',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'delete-template',
                                        ],
                                    ],
                                ],
                                'pagination' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/page/:page',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'index',
                                        ],
                                        'constraints' => [
                                            'page' => '\d+'
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'event-types' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/event-types',
                                'defaults' => [
                                    'module' => __NAMESPACE__,
                                    'action' => 'event-types',
                                ],
                            ],
                            'may_terminate' => true,
                            'child_routes' => [
                                'add' => [
                                    'type' => 'literal',
                                    'options' => [
                                        'route' => '/add',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'add-event-type',
                                        ],
                                    ],
                                ],
                                'edit' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/edit/:id',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'edit-event-type',
                                        ],
                                        'constraints' => [
                                            'id' => '\d+'
                                        ],
                                    ],
                                ],
                                'delete' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/delete/:id',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'delete-event-type',
                                        ],
                                    ],
                                ],
                                'pagination' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/page/:page',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'event-types',
                                        ],
                                        'constraints' => [
                                            'page' => '\d+'
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'inbox' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/inbox',
                                'defaults' => [
                                    'module' => __NAMESPACE__,
                                    'action' => 'inbox',
                                ],
                            ],
                            'may_terminate' => true,
                            'child_routes' => [
                                'message' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/:id',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'message',
                                        ],
                                        'constraints' => [
                                            'id' => '\d+'
                                        ],
                                    ],
                                ],
                                'pagination' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/page/:page',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'inbox',
                                        ],
                                        'constraints' => [
                                            'page' => '\d+'
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        'trash' => [
                            'type' => 'literal',
                            'options' => [
                                'route' => '/trash',
                                'defaults' => [
                                    'module' => __NAMESPACE__,
                                    'action' => 'trash',
                                ],
                            ],
                            'may_terminate' => true,
                            'child_routes' => [
                                'message' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/:id',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'message',
                                        ],
                                        'constraints' => [
                                            'id' => '\d+'
                                        ],
                                    ],
                                ],
                                'pagination' => [
                                    'type' => 'segment',
                                    'options' => [
                                        'route' => '/page/:page',
                                        'defaults' => [
                                            'module' => __NAMESPACE__,
                                            'action' => 'trash',
                                        ],
                                        'constraints' => [
                                            'page' => '\d+'
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];
