<?php
namespace Mail;

use Mail\Controller\AdminController;
use Mail\Controller\IndexController;

return [
    'controllers' => [
        'factories' => [
            'Mail\Controller\Admin' => function($cpm) {
                return new AdminController($cpm->getServiceLocator()->get('Mail\Model\Mail'));
            },
            'Mail\Controller\Index' =>  function($cpm) {
                return new IndexController($cpm->getServiceLocator()->get('Mail\Model\Mail'));
            },
        ],
    ],

    'router' => include 'router.config.php',

    'console' => [
        'router' => [
            'routes' => [
                'delivery' => [
                    'options' => [
                        'route'    => 'mail delivery [--limit=] [--delay=]',
                        'defaults' => [
                            'controller' => 'Mail\Controller\Cron',
                            'action'     => 'delivery'
                        ]
                    ]
                ],
            ]
        ]
    ],

    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/..',
            ],
        ],
    ],

    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],

    'zfc_rbac' => [
        'assertion_map' => [
            'mail.delete-message' => 'Mail\Assertion\DeleteMessageAssertion',
        ]
    ],

    'navigation' => [
        'admin' => [
            [
                'label' => 'Mail',
                'route' => 'admin/mail',
                'class' => 'fa fa-envelope',
                'permission' => 'mail.view',
                'order' => 5,
                'visible' => true,
                'pages' => [
                    [
                        'label' => 'Templates',
                        'route' => 'admin/mail/templates',
                    ],
                    [
                        'label' => 'Mail events',
                        'route' => 'admin/mail/event-types',
                    ],
                ],
            ],
        ],
        'settings' => [
            [
                'label' => 'Mail settings',
                'route' => 'admin/mail/settings',
                'class' => 'fa fa-envelope',
                'order' => 2,
            ],
        ],
    ],
];
