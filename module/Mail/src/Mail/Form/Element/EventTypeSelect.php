<?php
namespace Mail\Form\Element;

use Zend\Form\Element\Select;

class EventTypeSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $model = $this->getOption('model');
        $eventTypes = $model->getEventTypes();

        $values = [];

        foreach ($eventTypes as $eventType) {
            $values[$eventType->getId()] = $eventType->getName() . " [{$eventType->getIdent()}]";
        }

        $this->setValueOptions($values);

        $eventType = $this->getOption('eventType');

        if ($eventType) {
            $this->setValue($eventType->getId());
        }
    }
}