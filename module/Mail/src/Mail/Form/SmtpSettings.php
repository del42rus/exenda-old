<?php
namespace Mail\Form;

use Application\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator;

class SmtpSettings extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $translator = $this->getServiceLocator()->get('translator');

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'use_smtp',
            'options' => [
                'label' => gettext('Use SMTP'),
                'value_options' => [
                    0 => $translator->translate('No', 'Mail'),
                    1 => $translator->translate('Yes', 'Mail'),
                ],
            ],
            'attributes' => [
                'value' => 0,
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'smtp_server',
            'options' => [
                'label' => gettext('SMTP-server')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'smtp_encryption',
            'options' => [
                'label' => gettext('Encryption'),
                'value_options' => [
                    0 => $translator->translate('None'),
                    'ssl' => 'SSL',
                    'tls' => 'TLS',
                ],
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'smtp_port',
            'options' => [
                'label' => gettext('Server port')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'smtp_login',
            'options' => [
                'label' => gettext('Login')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'smtp_password',
            'options' => [
                'label' => gettext('Password')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);


        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Save',
                'data-loading-text' => $translator->translate('Saving...', 'Mail')
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'smtp_server' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    [
                        'name' => 'hostname',
                        'options' => [
                            'allow' => Validator\Hostname::ALLOW_DNS
                        ],
                    ],
                ],
            ],
            'smtp_port' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
                'validators' => [
                    ['name' => 'digits'],
                ],
            ],
            'smtp_login' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'smtp_password' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'smtp_encryption' => [
                'required' => false,
            ]
        ];
    }
}