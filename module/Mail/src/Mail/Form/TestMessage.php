<?php
namespace Mail\Form;

use Application\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Captcha;

class TestMessage extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $sl = $this->getModel()->getServiceLocator();
        $translator = $sl->get('translator');

        $this->add([
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'options' => [
                'label' => gettext('E-mail')
            ],
            'attributes' => [
                'placeholder' => 'E-mail'
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Send a test message'),
                'data-loading-text' => $translator->translate('Sending...', 'Mail')
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'email' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ],
        ];
    }
}