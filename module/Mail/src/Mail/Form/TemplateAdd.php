<?php
namespace Mail\Form;

use Application\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

use Mail\Form\Element\EventTypeSelect;

class TemplateAdd extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $eventTypeSelect = new EventTypeSelect('eventTypeId', [
            'model' => $this->getModel(),
            'eventType' => $this->getOption('eventType'),
        ]);

        $translator = $this->getServiceLocator()->get('translator');

        $eventTypeSelect->setLabel(gettext('Type of mail event'));

        $this->add($eventTypeSelect);

        $this->add([
            'type' => 'text',
            'name' => 'senderEmail',
            'options' => [
                'label' => gettext('From')
            ],
            'attributes' => [
                'required' => true,
                'value' => '#DEFAULT_EMAIL_FROM#'
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'senderName',
            'options' => [
                'label' => gettext('Sender name')
            ],
            'attributes' => [
                'required' => true,
                'value' => '#DEFAULT_NAME_FROM#'
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'recipient',
            'options' => [
                'label' => gettext('To')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'cc',
            'options' => [
                'label' => gettext('Copy')
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'bcc',
            'options' => [
                'label' => gettext('Blind copy')
            ],
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'subject',
            'options' => [
                'label' => gettext('Subject')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'text',
            'options' => [
                'label' => gettext('Text')
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'type',
            'options' => [
                'label' => gettext('Message type'),
                'value_options' => [
                    'text' => $translator->translate('Text', 'Mail'),
                    'html' => 'HTML',
                ],
            ],
            'attributes' => [
                'value' => 0,
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Select',
            'name' => 'language',
            'options' => [
                'label' => gettext('Language'),
                'value_options' => [
                    'en' => 'English',
                    'ru' => 'Русский',
                ],
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'active',
            'options' => [
                'label' => gettext('Is active?'),
                'value_options' => [
                    0 => $translator->translate('No', 'Mail'),
                    1 => $translator->translate('Yes', 'Mail'),
                ],
            ],
            'attributes' => [
                'value' => 0,
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'keepOnServer',
            'options' => [
                'label' => gettext('Keep on server?'),
                'value_options' => [
                    0 => $translator->translate('No', 'Mail'),
                    1 => $translator->translate('Yes', 'Mail'),
                ],
            ],
            'attributes' => [
                'value' => 0,
            ]
        ]);

        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Add template',
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'senderEmail' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'senderName' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'recipient' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'subject' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
        ];
    }
}