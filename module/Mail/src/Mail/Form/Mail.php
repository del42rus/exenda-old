<?php
namespace Mail\Form;

use Application\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Captcha;

class Mail extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $serviceLocator = $this->getOption('serviceLocator');
        $translator = $serviceLocator->get('translator');

        $this->add([
                'type' => 'text',
                'name' => 'name',
                'required' => true,
                'attributes' => [
                    'id' => 'name',
                    'placeholder' => $translator->translate('Your name', 'Mail')
                ]
            ]
        );

        $this->add([
            'type' => 'Zend\Form\Element\Email',
            'name' => 'email',
            'attributes' => [
                'placeholder' => 'E-mail'
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'text',
            'attributes' => [
                'id' => 'text',
            ]
        ]);

/*        // Pass captcha image options
        $captchaImage = new Captcha\Image([
            'font' => DOCUMENT_ROOT . '/captcha/fonts/arial.ttf',
            'width' => 150,
            'height' => 80,
            'dotNoiseLevel' => 40,
            'lineNoiseLevel' => 3,
            'wordLen' => 6
        ]);

        $captchaImage->setImgDir(DOCUMENT_ROOT . '/captcha/images');
        $captchaImage->setImgUrl('/captcha/images');

        $this->add([
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => [
                'captcha' => $captchaImage,
            ],
        ]);*/

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Send'),
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'name' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
            'email' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                ],
            ],
            'text' => [
                'required' => true,
            ],
        ];
    }
}