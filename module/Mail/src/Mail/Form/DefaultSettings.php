<?php
namespace Mail\Form;

use Application\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Validator;

class DefaultSettings extends Form implements InputFilterProviderInterface
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);

        $translator = $this->getServiceLocator()->get('translator');

        $this->add([
            'type' => 'email',
            'name' => 'default_email',
            'options' => [
                'label' => gettext('Default E-mail')
            ],
            'attributes' => [
                'required' => true,
            ]
        ]);

        $this->add([
            'type' => 'text',
            'name' => 'default_name',
            'options' => [
                'label' => gettext('Default name')
            ],
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Save',
                'data-loading-text' => $translator->translate('Saving...', 'Mail')
            ]
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'default_email' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StripTags'],
                    ['name' => 'StringTrim'],
                ],
            ],
        ];
    }
}