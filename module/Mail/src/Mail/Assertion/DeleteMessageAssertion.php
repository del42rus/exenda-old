<?php
namespace Mail\Assertion;

use Mail\Model\Entity\Message;

use ZfcRbac\Assertion\AssertionInterface;
use ZfcRbac\Service\AuthorizationService;

class DeleteMessageAssertion implements AssertionInterface
{
    public function assert(AuthorizationService $authorization, Message $message = null)
    {
       return $authorization->getIdentity()->getEmail() === $message->getRecipient();
    }
}