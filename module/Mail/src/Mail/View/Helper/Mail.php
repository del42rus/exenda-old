<?php
namespace Mail\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Mail extends AbstractHelper implements ServiceLocatorAwareInterface
{
    public function __invoke()
    {
        return $this;
    }

    public function getUnreadMessages()
    {
        $helperPluginManager = $this->getServiceLocator();
        $serviceLocator = $helperPluginManager->getServiceLocator();

        $mail = $serviceLocator->get('Mail\Model\Mail');
        $messages = $mail->getInboxMessages(null, 10, false);

        return $this->getView()->partial('mail/admin/partial/unread-messages', ['messages' => $messages]);
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}