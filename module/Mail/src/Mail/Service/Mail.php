<?php
namespace Mail\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Sms\Twig\Extension\Plural;

use Zend\Mail as ZendMail;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class Mail implements ServiceLocatorAwareInterface
{
    public function getMessage()
    {
        if (null === $this->message) {
            $this->message = new ZendMail\Message();
        }

        return $this->message;
    }

    public function getTransport()
    {
        if (null === $this->transport) {
            $model = $this->getServiceLocator()->get('Mail\Model\Mail');
            $useSmtp = $model->getConfigValue('use_smtp');

            if ($useSmtp) {
                $this->transport = new ZendMail\Transport\Smtp();

                $options = new ZendMail\Transport\SmtpOptions();
                $options->setHost($model->getConfigValue('smtp_server'));
                $options->setConnectionClass('login');
                $options->setPort($model->getConfigValue('smtp_port'));

                $connectionConfig = [
                    'username' => $model->getConfigValue('smtp_login'),
                    'password' => $model->getConfigValue('smtp_password'),
                ];

                if ($encryption = $model->getConfigValue('smtp_encryption')) {
                    $connectionConfig['ssl'] = $encryption;
                }

                $options->setConnectionConfig($connectionConfig);

                $this->transport->setOptions($options);
            } else {
                $this->transport = new ZendMail\Transport\Sendmail();
            }
        }

        return $this->transport;
    }

    public function setEncoding($encoding)
    {
        $message = $this->getMessage();

        $headers = $message->getHeaders();
        $headers->removeHeader('Content-Type');
        $headers->addHeaderLine('Content-Type', 'text/html; charset=' . $encoding);

        return $this;
    }

    public function send($event, $fields = [], $language = null, $templateId = null)
    {
        $model = $this->getServiceLocator()->get('Mail\Model\Mail');

        $transport = $this->getTransport();
        $message = $this->getMessage();

        if (null === $language && null === $templateId) {
            $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $appConfig = $em->getRepository('Application\Model\Entity\Config');
            $language = $appConfig->findOneByName('language')->getValue();
        }

        if (null === $templateId) {
            $template = $model->getTemplate($event, $language);
        } else {
            $template = $model->getTemplateById($templateId);
        }

        if (!$template) {
            throw new \Exception('Mail template not found');
        }

        $vars = array_change_key_case($fields ,CASE_LOWER);
        $fields = array_change_key_case($fields ,CASE_UPPER);

        $values = array_values($fields);
        $keys = array_keys($fields);

        if (!in_array('DEFAULT_EMAIL_FROM', $keys)) {
            $keys[] = 'DEFAULT_EMAIL_FROM';
            $values[] = $model->getConfigValue('default_email');
        }

        if (!in_array('DEFAULT_NAME_FROM', $keys)) {
            $keys[] = 'DEFAULT_NAME_FROM';
            $values[] = $model->getConfigValue('default_name');
        }

        if (!in_array('HOST', $keys)) {
            $request = $this->getServiceLocator()->get('Request');

            if ($request instanceof ConsoleRequest) {
                $config = $this->getServiceLocator()->get('config');
                $host = $config['domain'];
            } else {
                $host = $request->getUri()->getHost();
            }

            $keys[] = 'HOST';
            $values[] = preg_replace('{^www\.(.+\.)}i', '$1', $host);
        }

        $keys = array_map(function($key) {
            return '#'.$key.'#';
        }, $keys);

        if (null !== $template) {
            $body = new MimeMessage();

            $text = str_replace($keys, $values, $template->getText());

            if (class_exists('Twig_Environment')) {
                $twig = new \Twig_Environment(new \Twig_Loader_String());
                $twig->addExtension(new Plural());

                $text = $twig->render($text, $vars);
            }

            if ($template->isHtml()) {
                $html = new MimePart($text);
                $html->type = "text/html";

                $body->addPart($html);
            } else {
                $text = new MimePart($text);
                $text->type = "text/plain";

                $body->addPart($text);
            }
        }

        $from = str_replace($keys, $values, $template->getSenderEmail());
        $name = str_replace($keys, $values, $template->getSenderName());

        $recipient = str_replace($keys, $values, $template->getRecipient());
        $subject = str_replace($keys, $values, $template->getSubject());

        if ($template->getCc()) {
            $cc = str_replace($keys, $values, $template->getCc());
            $cc = preg_split('/\s*,\s*/', trim($cc));

            $message->addCc($cc);
        }

        if ($template->getBcc()) {
            $bcc = str_replace($keys, $values, $template->getBcc());
            $bcc = preg_split('/\s*,\s*/', trim($bcc));;

            $message->addBcc($bcc);
        }

        if ($template->getKeepOnServer()) {
            $msg = new \Mail\Model\Entity\Message();

            $msg->setSenderEmail($from)
                ->setSenderName($name)
                ->setRecipient($recipient)
                ->setSubject($subject)
                ->setContent($text)
                ->setType('inbox');

            if ($bcc) {
                $msg->setBcc(join(', ', $bcc));
            }

            if ($cc) {
                $msg->setCc(join(', ', $cc));
            }

            $model->save($msg);
        }

        $message->setFrom($from, $name)
                ->setTo($recipient)
                ->setSubject($subject)
                ->setBody($body);

        $transport->send($message);
    }

    /**
     * Sets service locator
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @return void
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get root service manager
     *
     * @return \Zend\ServiceManager\ServiceManager
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}