<?php
namespace Mail\Model;

use Application\Mvc\Model\AbstractModel;
use Mail\Model\Entity\Config;
use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Doctrine\ORM\Query;

use Mail\Model\Entity\Template;

class Mail extends AbstractModel
{
    public function getTemplateById($id)
    {
        return $this->getEntity('Template')->findOneById($id);
    }

    public function getTemplate($event, $language)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('t')
                    ->from('Mail\Model\Entity\Template', 't')
                    ->leftJoin('Mail\Model\Entity\EventType', 'et',
                        Query\Expr\Join::WITH, 't.eventType = et.id'
                    )
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('et.ident', $qb->expr()->literal($event)),
                        $qb->expr()->eq('t.language', $qb->expr()->literal($language)),
                        $qb->expr()->eq('t.active', 1)
                    ))
                    ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function getTemplates($paged = null, $itemCountPerPage = 10)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('t')
                    ->from('Mail\Model\Entity\Template', 't')
                    ->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                    ->setItemCountPerPage($itemCountPerPage)
                    ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    public function getEventTypeById($id)
    {
        return $this->getEntity('EventType')->findOneById($id);
    }

    public function getEventTypeByIdent($ident)
    {
        return $this->getEntity('EventType')->findOneByIdent($ident);
    }

    public function getEventTypes($paged = null, $itemCountPerPage = 10)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('et')
                    ->from('Mail\Model\Entity\EventType', 'et')
                    ->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                    ->setItemCountPerPage($itemCountPerPage)
                    ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    public function saveTemplate(Template $template)
    {
        $em = $this->getEntityManager();
        $id = (int) $template->getId();

        if ($id == 0) {
            $em->persist($template);
        }

        $em->flush();

        if ($template->isActive()) {
            $qb = $em->createQueryBuilder();

            $qb->update('Mail\Model\Entity\Template', 't')
                ->set('t.active', 0)
                ->where($qb->expr()->andX(
                    $qb->expr()->eq('t.eventType', $template->getEventType()->getId()),
                    $qb->expr()->eq('t.language', $qb->expr()->literal($template->getLanguage())),
                    $qb->expr()->neq('t.id', $template->getId())
                ))
                ->getQuery()
                ->execute();
        }
    }

    public function save($entity)
    {
        $id = (int) $entity->getId();

        if ($id == 0) {
            $this->getEntityManager()->persist($entity);
        }

        $this->getEntityManager()->flush();
    }

    public function remove($entity)
    {
        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush();
    }

    public function removeTemplates($ids)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->delete('Mail\Model\Entity\Template', 't')
                    ->where($qb->expr()->in('t.id', '?1'))
                    ->getQuery();

        $count = $query->setParameter(1, $ids)
                       ->execute();

        return $count;
    }

    public function removeEventTypes($ids)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->delete('Mail\Model\Entity\EventType', 'et')
                    ->where($qb->expr()->in('et.id', '?1'))
                    ->getQuery();

        $count = $query->setParameter(1, $ids)
                        ->execute();

        return $count;
    }

    public function getConfigValue($name)
    {
        $config = $this->getEntity('Config')->findOneByName($name);

        if ($config instanceof Config) {
            $value = $config->getValue();
        }

        return $value;
    }

    public function saveConfig($data = [])
    {
        $em = $this->getEntityManager();

        foreach ($data as $name => $value) {
            $config = $this->getEntity('Config')->findOneByName($name);

            if ($config instanceof Config) {
                $config->setValue($value);
            } else {
                $config = new Config();
                $config->setName($name);
                $config->setValue($value);

                $em->persist($config);
            }
        }

        $em->flush();
    }

    public function getInboxMessages($paged, $itemCountPerPage = 20, $isRead = null)
    {
        $authService = $this->getServiceLocator()->get('AuthenticationService');
        $identity = $authService->getIdentity();

        $qb = $this->getEntityManager()->createQueryBuilder();

        $stmt = $qb->select('m')
                    ->from('Mail\Model\Entity\Message', 'm')
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('m.recipient', $qb->expr()->literal($identity->getEmail())),
                        $qb->expr()->eq('m.type', $qb->expr()->literal('inbox'))
                    ))
                    ->orderBy('m.created', 'DESC')
                    ->setMaxResults($itemCountPerPage);

        if (null !== $isRead) {
           $stmt->andWhere($qb->expr()->eq('m.read', (int) $isRead));
        }

        $query = $stmt->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                    ->setItemCountPerPage($itemCountPerPage)
                    ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    public function getUnreadMessageCount()
    {
        $authService = $this->getServiceLocator()->get('AuthenticationService');
        $identity = $authService->getIdentity();

        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('COUNT(m.id)')
                    ->from('Mail\Model\Entity\Message', 'm')
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('m.recipient', $qb->expr()->literal($identity->getEmail())),
                        $qb->expr()->eq('m.type', $qb->expr()->literal('inbox')),
                        $qb->expr()->eq('m.read', 0)
                    ))
                    ->getQuery();

        return $query->getSingleScalarResult();
    }

    public function getSentMessages($paged, $itemCountPerPage = 20)
    {
        $authService = $this->getServiceLocator()->get('AuthenticationService');
        $identity = $authService->getIdentity();

        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('m')
                    ->from('Mail\Model\Entity\Message', 'm')
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('m.recipient', $qb->expr()->literal($identity->getEmail())),
                        $qb->expr()->eq('m.type', $qb->expr()->literal('sent'))
                    ))
                    ->orderBy('m.created', 'DESC')
                    ->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                ->setItemCountPerPage($itemCountPerPage)
                ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    public function getTrashMessages($paged, $itemCountPerPage = 20)
    {
        $authService = $this->getServiceLocator()->get('AuthenticationService');
        $identity = $authService->getIdentity();

        $qb = $this->getEntityManager()->createQueryBuilder();

        $query = $qb->select('m')
                    ->from('Mail\Model\Entity\Message', 'm')
                    ->where($qb->expr()->andX(
                        $qb->expr()->eq('m.recipient', $qb->expr()->literal($identity->getEmail())),
                        $qb->expr()->eq('m.type', $qb->expr()->literal('trash'))
                    ))
                    ->orderBy('m.created', 'DESC')
                    ->getQuery();

        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                ->setItemCountPerPage($itemCountPerPage)
                ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult();
    }

    public function getMessageById($id)
    {
        return $this->getEntity('Message')->findOneById($id);
    }

    public function getMessagesByIds($ids = [])
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('m')
                    ->from('Mail\Model\Entity\Message', 'm')
                    ->where($qb->expr()->in('m.id', $ids))
                    ->getQuery();

        return $query->getResult();
    }

    public function removeMessages($ids, $type)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        if ($type == 'trash') {
            $query = $qb->delete('Mail\Model\Entity\Message', 'm')
                        ->where($qb->expr()->andX(
                            $qb->expr()->in('m.id', $ids)
                        ))
                        ->getQuery();

            $count = $query->execute();
        } else {
            $query = $qb->update('Mail\Model\Entity\Message', 'm')
                        ->set('m.type', $qb->expr()->literal('trash'))
                        ->where($qb->expr()->andX(
                            $qb->expr()->in('m.id', $ids)
                        ))
                        ->getQuery();

            $count = $query->execute();
        }

        return $count;
    }

    public function changeMessagesStatus($ids, $status)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->update('Mail\Model\Entity\Message', 'm')
                    ->set('m.read', $status)
                    ->where($qb->expr()->andX(
                        $qb->expr()->in('m.id', $ids)
                    ))
                    ->getQuery();

        $count = $query->execute();

        return $count;
    }

    public function getMailouts($limit)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('m')
                    ->from('Mail\Model\Entity\Mailout', 'm')
                    ->where($qb->expr()->eq('m.isSent', 0))
                    ->setMaxResults($limit)
                    ->getQuery();

        return $query->getResult();
    }
}