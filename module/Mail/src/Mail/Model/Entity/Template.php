<?php
namespace Mail\Model\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="mail_templates")
 */
class Template
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private $type;

    /**
     * @ORM\Column(type="string", name="sender_email")
     */
    private $senderEmail;

    /**
     * @ORM\Column(type="string", name="sender_name")
     */
    private $senderName;

    /**
     * @ORM\Column(type="string")
     */
    private $recipient;

    /**
     * @ORM\Column(type="string", name="carbon_copy")
     */
    private $cc;

    /**
     * @ORM\Column(type="string", name="blind_carbon_copy")
     */
    private $bcc;

    /**
     * @ORM\Column(type="string")
     */
    private $subject;

    /**
     * @ORM\Column(type="string")
     */
    private $text;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated;

    /**
     * @ORM\ManyToOne(targetEntity="EventType")
     * @ORM\JoinColumn(name="event_type_id", referencedColumnName="id")
     */
    private $eventType;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private $language;

    /**
     * @ORM\Column(type="integer", length=1, name="keep_on_server")
     */
    private $keepOnServer;

    public function __construct()
    {
        $this->language = 'en';
        $this->keepOnServer = 0;
    }

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $bcc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
    }

    /**
     * @return mixed
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param mixed $cc
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    }

    /**
     * @return mixed
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param mixed $recipient
     */
    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    /**
     * @return mixed
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @param mixed $senderEmail
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * @return mixed
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @param mixed $senderName
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;
    }

    /**
     * @return mixed
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function isHtml()
    {
        return $this->type == 'html';
    }

    public function getCreated($format = 'd.m.Y H:i')
    {
        return $this->created->format($format);
    }

    public function getUpdated($format = 'd.m.Y H:i')
    {
        return $this->updated->format($format);
    }

    public function setEventType(EventType $eventType)
    {
        $this->eventType = $eventType;
        $eventType->addTemplate($this);
    }

    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    public function isActive()
    {
        return (bool) $this->active;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function setKeepOnServer($value)
    {
        $this->keepOnServer = $value;
    }

    public function getKeepOnServer()
    {
        return $this->keepOnServer;
    }

    public function populate($data = [])
    {
        $this->type = $data['type'];
        $this->language = $data['language'];
        $this->senderEmail = $data['senderEmail'];
        $this->senderName = $data['senderName'];
        $this->recipient = $data['recipient'];
        $this->cc = $data['cc'];
        $this->bcc = $data['bcc'];
        $this->subject = $data['subject'];
        $this->text = $data['text'];
        $this->active = $data['active'];
        $this->keepOnServer = $data['keepOnServer'];

        return $this;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}