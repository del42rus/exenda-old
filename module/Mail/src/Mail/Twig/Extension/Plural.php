<?php
namespace Mail\Twig\Extension;

class Plural extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions() {
        return array(
            'plural_ru' => new \Twig_Function_Method($this, 'pluralRussian')
        );
    }

    public function pluralRussian($number) {
        return (($number % 10 == 1) && ($number % 100 != 11))
                ? 0
                : ((($number % 10 >= 2)
                    && ($number % 10 <= 4)
                    && (($number % 100 < 10)
                        || ($number % 100 >= 20)))
                    ? 1
                    : 2
                );
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'my_bundle';
    }
}