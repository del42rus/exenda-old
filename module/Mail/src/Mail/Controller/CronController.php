<?php
namespace Mail\Controller;

use Zend\Console\Request as ConsoleRequest;
use Zend\Mvc\Controller\AbstractActionController;
use Mail\Model\Mail as Model;

class CronController extends AbstractActionController
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function deliveryAction()
    {
        $request = $this->getRequest();

        if (!$request instanceof ConsoleRequest) {
            throw new \RuntimeException('You can only use this action from a console!');
        }

        $limit = $request->getParam('limit') ?: 100;
        $delay = $request->getParam('delay') ?: 5;

        $messages = $this->model->getMailouts($limit);

        $mail = $this->getServiceLocator()->get('Mail\Service\Mail');

        foreach ($messages as $message) {
            $fields = $message->getData();
            $fields['EMAIL'] = $message->getRecipient();

            try {
                $mail->send($message->getEventType(), $fields);
                $message->setSent();

                $this->model->save($message);
            } catch (\RuntimeException $e) {
                continue;
            }

            sleep($delay);
        }
    }
}