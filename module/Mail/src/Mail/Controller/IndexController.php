<?php
namespace Mail\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\FormInterface;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

use Mail\Model\Mail as MailModel;

class IndexController extends AbstractActionController
{
    public function __construct(MailModel $model)
    {
        $this->model = $model;
    }

    public function indexAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $form = new \Mail\Form\Mail('Mail', [
            'serviceLocator' => $this->getServiceLocator()
        ]);

        return new ViewModel([
            'form' => $form,
        ]);
    }

    public function sendAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $form = new \Mail\Form\Mail('Mail', [
            'serviceLocator' => $this->getServiceLocator()
        ]);

        $form->setData($request->getPost());

        if ($form->isValid()) {
            $translator = $this->getServiceLocator()->get('translator');
            $mail = $this->getServiceLocator()->get('Mail\Service\Mail');

            $formData = $form->getData(FormInterface::VALUES_AS_ARRAY);

            $mail->send('MAIL_ME', $formData);

            return new JsonModel([
               'success' => 1,
               'message' => $translator->translate('Your message has been sent successfully.', 'Mail'),
            ]);
        } else {
            // Create a new captcha
/*            $element = $form->get('captcha');
            $captcha = $element->getCaptcha();

            $captcha->generate();

            $imgAttributes = [
                'captchaId' =>  $captcha->getId(),
                'src'    => $captcha->getImgUrl() . $captcha->getId() . $captcha->getSuffix(),
            ];*/

            return new JsonModel([
                'success' => 0,
                'messages' => $form->getMessages(),
//                'imgAttributes' => $imgAttributes,
            ]);
        }
    }
}

