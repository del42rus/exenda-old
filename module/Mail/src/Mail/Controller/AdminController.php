<?php
namespace Mail\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

use Mail\Model\Entity\EventType;
use Mail\Model\Entity\Template;

use Mail\Model\Mail as MailModel;

use ZfcRbac\Exception\UnauthorizedException;

use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

class AdminController extends AbstractActionController
{
    public function __construct(MailModel $model)
    {
        $this->model = $model;
    }

    public function indexAction()
    {
        if (!$this->isGranted('mail.view-templates')) {
            throw new UnauthorizedException();
        }

        $page = $this->params()->fromRoute('page', 1);
        $itemCountPerPage = $this->params()->fromQuery('onpage', 50);

        $templates = $this->model->getTemplates($page, $itemCountPerPage);

        return new ViewModel([
            'templates' => $templates,
        ]);
    }

    public function addTemplateAction()
    {
        if (!$this->isGranted('mail.add-template')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        $form = $this->model->getForm('TemplateAdd');

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $template = new Template();
                $template->populate($form->getData());

                $eventTypeId = $request->getPost('eventTypeId');
                $eventType = $this->model->getEventTypeById($eventTypeId);

                $template->setEventType($eventType);

                $this->model->saveTemplate($template);

                return $this->redirect()->toRoute('admin/mail/templates/edit', ['id' => $template->getId()]);
            } else {
                $rootViewModel = $this->getEvent()->getViewModel();
                $translator = $this->getServiceLocator()->get('translator');

                $text = $translator->translate('The form was completed with errors.', 'Mail');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
            }
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }

    public function editTemplateAction()
    {
        if (!$this->isGranted('mail.edit-template')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id', null);

        $template = $this->model->getTemplateById($id);

        $translator = $this->getServiceLocator()->get('translator');

        if (null !== $template) {
            $form = $this->model->getForm('TemplateAdd', [
                'eventType' => $template->getEventType()
            ]);

            $form->bind($template);

            if ($request->isPost()) {
                $form->setData($request->getPost());

                $rootViewModel = $this->getEvent()->getViewModel();
                $translator = $this->getServiceLocator()->get('translator');

                if ($form->isValid()) {
                    $eventTypeId = $request->getPost('eventTypeId');
                    $eventType = $this->model->getEventTypeById($eventTypeId);

                    $template->setEventType($eventType);

                    $this->model->saveTemplate($template);

                    $text = $translator->translate('Template has been updated successfully.', 'Mail');
                    $rootViewModel->setVariable('alert', ['type' => 'success', 'text' => $text]);
                } else {
                    $text = $translator->translate('The form was completed with errors.', 'Mail');
                    $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
                }
            }
        } else {
            $message = sprintf($translator->translate('Template with id &laquo;%s&raquo; not found', 'Mail'), $id);

            $viewModel = new ViewModel(['message' => $message]);
            $viewModel->setCaptureTo('error');
            $viewModel->setTemplate('error/error');

            return $viewModel;
        }

        return new ViewModel([
            'form' => $form,
            'eventType' => $template->getEventType(),
        ]);
    }

    public function deleteTemplateAction()
    {
        if (!$this->isGranted('mail.delete-template')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', null);

        if (!$id || !$template = $this->model->getTemplateById($id)) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()) {
            $this->model->remove($template);

            return $this->redirect()->toRoute('admin/mail/templates');
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('mail/admin/delete');

        $translator = $this->getServiceLocator()->get('translator');

        $message = $translator->translate('Are you sure that you want to delete the template', 'Mail')
            . '&nbsp;&laquo;' . $template->getSubject() . '&raquo; ?';

        $viewModel->setVariables([
            'url' => $request->getPost('url'), // get from ajax data
            'message' => $message,
        ]);

        return $viewModel;
    }

    public function deleteTemplatesAction()
    {
        if (!$this->isGranted('mail.delete-template')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        $translator = $this->getServiceLocator()->get('translator');

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            if (count($ids)) {
                $this->model->removeTemplates($ids);

                return $this->redirect()->toRoute('admin/mail/templates');
            } else {
                $message = $translator->translate('Nothing selected', 'Mail');

                $viewModel = new ViewModel(['message' => $message]);
                $viewModel->setCaptureTo('error');
                $viewModel->setTemplate('error/error');

                return $viewModel;
            }
        }

        $message = $translator->translate('Are you sure that you want to delete selected templates?', 'Mail');

        $viewModel = new ViewModel([
            'ids'     => $ids,
            'message' => $message,
            'url'     => $request->getPost('url') // get from ajax
        ]);

        $viewModel->setTemplate('mail/admin/delete-selected');

        return $viewModel;
    }

    public function eventTypesAction()
    {
        if (!$this->isGranted('mail.view-events')) {
            throw new UnauthorizedException();
        }

        $page = $this->params()->fromRoute('page', 1);
        $itemCountPerPage = $this->params()->fromQuery('onpage', 50);

        $translator = $this->getServiceLocator()->get('translator');
        $title = $translator->translate('Event types', 'Mail');

        $rootViewModel = $this->getEvent()->getViewModel();
        $rootViewModel->setVariable('title', $title);

        $eventTypes = $this->model->getEventTypes($page, $itemCountPerPage);

        return new ViewModel([
            'eventTypes' => $eventTypes,
        ]);
    }

    public function addEventTypeAction()
    {
        if (!$this->isGranted('mail.add-event-type')) {
            throw new UnauthorizedException();
        }

        $translator = $this->getServiceLocator()->get('translator');
        $title = $translator->translate('Event types', 'Mail');

        $rootViewModel = $this->getEvent()->getViewModel();
        $rootViewModel->setVariable('title', $title);

        $request = $this->getRequest();

        $form = $this->model->getForm('EventTypeAdd');

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $eventType = new EventType();
                $eventType->populate($form->getData());

                $this->model->save($eventType);

                return $this->redirect()->toRoute('admin/mail/event-types');
            } else {
                $text = $translator->translate('The form was completed with errors.', 'Mail');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
            }
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }

    public function editEventTypeAction()
    {
        if (!$this->isGranted('mail.edit-event-type')) {
            throw new UnauthorizedException();
        }

        $translator = $this->getServiceLocator()->get('translator');
        $title = $translator->translate('Event types', 'Mail');

        $rootViewModel = $this->getEvent()->getViewModel();
        $rootViewModel->setVariable('title', $title);

        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id', null);

        $eventType = $this->model->getEventTypeById($id);

        if (null !== $eventType) {
            $form = $this->model->getForm('EventTypeAdd');
            $form->bind($eventType);

            if ($request->isPost()) {
                $form->setData($request->getPost());

                if ($form->isValid()) {
                    $this->model->save($eventType);

                    $text = $translator->translate('Event type has been updated successfully.', 'Mail');
                    $rootViewModel->setVariable('alert', ['type' => 'success', 'text' => $text]);
                } else {
                    $text = $translator->translate('The form was completed with errors.', 'Mail');
                    $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
                }
            }
        } else {
            $message = sprintf($translator->translate('Type of mail event with id &laquo;%s&raquo; not found', 'Mail'), $id);

            $viewModel = new ViewModel(['message' => $message]);
            $viewModel->setCaptureTo('error');
            $viewModel->setTemplate('error/error');

            return $viewModel;
        }

        return new ViewModel([
            'form' => $form,
        ]);
    }

    public function deleteEventTypeAction()
    {
        if (!$this->isGranted('mail.delete-event-type')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', null);

        if (!$id || !$eventType = $this->model->getEventTypeById($id)) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()) {
            $this->model->remove($eventType);

            return $this->redirect()->toRoute('admin/mail/event-types');
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('mail/admin/delete');

        $translator = $this->getServiceLocator()->get('translator');

        $message = $translator->translate('Are you sure that you want to delete the event type', 'Mail')
            . '&nbsp;&laquo;' . $eventType->getIdent() . '&raquo; ?';

        $viewModel->setVariables([
            'url' => $request->getPost('url'), // get from ajax data
            'message' => $message,
        ]);

        return $viewModel;
    }

    public function deleteEventTypesAction()
    {
        if (!$this->isGranted('mail.delete-event-type')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        $translator = $this->getServiceLocator()->get('translator');

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            if (count($ids)) {
                $this->model->removeEventTypes($ids);

                return $this->redirect()->toRoute('admin/mail/event-types');
            } else {
                $message = $translator->translate('Nothing selected', 'Mail');

                $viewModel = new ViewModel(['message' => $message]);
                $viewModel->setCaptureTo('error');
                $viewModel->setTemplate('error/error');

                return $viewModel;
            }
        }

        $message = $translator->translate('Are you sure that you want to delete selected event types?', 'Mail');

        $viewModel = new ViewModel([
            'ids'     => $ids,
            'message' => $message,
            'url'     => $request->getPost('url') // get from ajax
        ]);

        $viewModel->setTemplate('mail/admin/delete-selected');

        return $viewModel;
    }

    public function getEventDescriptionAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $eventTypeId = $request->getPost('eventTypeId');

        if ($eventTypeId) {
            $eventType = $this->model->getEventTypeById($eventTypeId);

            if ($eventType instanceof EventType) {
                return new JsonModel([
                    'success' => 1,
                    'description' => nl2br($eventType->getDescription())
                ]);
            }
        }

        return new JsonModel([
            'success' => 0,
        ]);
    }

    public function settingsAction()
    {
        if (!$this->isGranted('mail.edit-settings')) {
            throw new UnauthorizedException();
        }

        $translator = $this->getServiceLocator()->get('translator');
        $title = $translator->translate('Mail settings', 'Mail');

        $rootViewModel = $this->getEvent()->getViewModel();
        $rootViewModel->setVariable('title', $title);

        $smtpSettingsForm = $this->model->getForm('SmtpSettings');
        $elements = array_keys($smtpSettingsForm->getElements());
        unset($elements[count($elements) - 1]);

        foreach ($elements as $name) {
            $data[$name] = $this->model->getConfigValue($name);
        }

        $smtpSettingsForm->setData($data);

        $defaultSettingsForm = $this->model->getForm('DefaultSettings');
        $elements = array_keys($defaultSettingsForm->getElements());
        unset($elements[count($elements) - 1]);

        foreach ($elements as $name) {
            $data[$name] = $this->model->getConfigValue($name);
        }

        $defaultSettingsForm->setData($data);

        $testMessageForm = $this->model->getForm('TestMessage');

        return new ViewModel([
            'smtpSettingsForm' => $smtpSettingsForm,
            'defaultSettingsForm' => $defaultSettingsForm,
            'testMessageForm' => $testMessageForm
        ]);
    }

    public function saveSmtpSettingsAction()
    {
        if (!$this->isGranted('mail.edit-settings')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost()) {
            $form = $this->model->getForm('SmtpSettings');
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);

                $this->model->saveConfig($data);

                $translator = $this->getServiceLocator()->get('translator');
                return new JsonModel([
                    'success' => 1,
                    'message' => $translator->translate('SMTP settings has been successfully updated', 'Mail')
                ]);
            } else {
                return new JsonModel([
                    'success' => 0,
                    'messages' => $form->getMessages(),
                ]);
            }
        }

        return new JsonModel();
    }

    public function saveDefaultSettingsAction()
    {
        if (!$this->isGranted('mail.edit-settings')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest()) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost()) {
            $form = $this->model->getForm('DefaultSettings');
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);

                $this->model->saveConfig($data);

                $translator = $this->getServiceLocator()->get('translator');
                return new JsonModel([
                    'success' => 1,
                    'message' => $translator->translate('Default settings has been successfully updated', 'Mail')
                ]);
            } else {
                return new JsonModel([
                    'success' => 0,
                    'messages' => $form->getMessages(),
                ]);
            }
        }

        return new JsonModel();
    }

    public function inboxAction()
    {
        $translator = $this->getServiceLocator()->get('translator');
        $title = $translator->translate('Mailbox', 'Mail');

        $rootViewModel = $this->getEvent()->getViewModel();
        $rootViewModel->setVariable('title', $title);

        $page = $this->params()->fromRoute('page', 1);
        $itemCountPerPage = $this->params()->fromQuery('onpage', 20);

        $messages = $this->model->getInboxMessages($page, $itemCountPerPage);

        return new ViewModel([
            'messages' => $messages,
            'unreadMessageCount' => $this->model->getUnreadMessageCount()
        ]);
    }

    public function trashAction()
    {
        $translator = $this->getServiceLocator()->get('translator');
        $title = $translator->translate('Mailbox', 'Mail');

        $rootViewModel = $this->getEvent()->getViewModel();
        $rootViewModel->setVariable('title', $title);

        $page = $this->params()->fromRoute('page', 1);

        $messages = $this->model->getTrashMessages($page);

        return new ViewModel([
            'messages' => $messages,
        ]);
    }

    public function messageAction()
    {
        $messageId = $this->params()->fromRoute('id');

        $message = $this->model->getMessageById($messageId);
        $message->setRead(true);
        $this->model->save($message);

        return new ViewModel([
            'message' => $message
        ]);
    }

    public function deleteMessageAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', null);

        if (!$id || !$message = $this->model->getMessageById($id)) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()) {

            if (!$this->isGranted('mail.delete-message', $message)) {
                throw new UnauthorizedException();
            }

            switch ($type = $message->getType()) {
                case 'inbox':
                        $message->setType('trash');
                        $this->model->save($message);
                    break;
                case 'trash':
                        $this->model->remove($message);
                    break;
            }

            return $this->redirect()->toRoute('admin/mail/' . $type);
        }

        $viewModel = new ViewModel();
        $viewModel->setTemplate('mail/admin/delete');

        $translator = $this->getServiceLocator()->get('translator');

        $message = $translator->translate('Are you sure that you want to delete the message?', 'Mail');

        $viewModel->setVariables([
            'url' => $request->getPost('url'), // get from ajax data
            'message' => $message,
        ]);

        return $viewModel;
    }

    public function deleteMessagesAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        $translator = $this->getServiceLocator()->get('translator');

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            $messages = $this->model->getMessagesByIds($ids);

            foreach ($messages as $message) {
                if (!$this->isGranted('mail.delete-message', $message)) {
                    throw new UnauthorizedException();
                }
            }

            if (count($ids) && $type = $request->getQuery('type')) {
                $this->model->removeMessages($ids, $type);

                return $this->redirect()->toRoute('admin/mail/inbox');
            } else {
                $message = $translator->translate('Nothing selected', 'Mail');

                $viewModel = new ViewModel(['message' => $message]);
                $viewModel->setCaptureTo('error');
                $viewModel->setTemplate('error/error');

                return $viewModel;
            }
        }

        $message = $translator->translate('Are you sure that you want to delete selected messages?', 'Mail');

        $viewModel = new ViewModel([
            'ids'     => $ids,
            'message' => $message,
            'url'     => $request->getPost('url') // get from ajax
        ]);

        $viewModel->setTemplate('mail/admin/delete-selected');

        return $viewModel;
    }

    public function changeMessagesStatusAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        $status = $this->params()->fromRoute('read', 0);

        $messages = $this->model->getMessagesByIds($ids);

        foreach ($messages as $message) {
            if ($this->identity()->getEmail() !== $message->getRecipient()) {
                throw new UnauthorizedException();
            }
        }

        $this->model->changeMessagesStatus($ids, $status);

        return new JsonModel([
            'success' => 1,
        ]);
    }

    public function testAction()
    {
        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $form = $this->model->getForm('TestMessage');

        $form->setData($request->getPost());

        if ($form->isValid()) {
            $translator = $this->getServiceLocator()->get('translator');
            $mail = $this->getServiceLocator()->get('Mail\Service\Mail');

            $message = $mail->getMessage();
            $transport = $mail->getTransport();

            $from = $this->model->getConfigValue('default_email');
            $name = $this->model->getConfigValue('default_name');

            $recipient = $form->getData()['email'];

            $text = 'This is a simple test message';

            $body = new MimeMessage();

            $text = new MimePart($text);
            $text->type = "text/plain";

            $body->addPart($text);

            $message->setFrom($from, $name)
                    ->setTo($recipient)
                    ->setSubject('Test message')
                    ->setBody($body);

            try {
                $transport->send($message);
            } catch (\RuntimeException $e) {
                return new JsonModel([
                    'success' => 0,
                    'message' => $e->getMessage()
                ]);
            }

            return new JsonModel([
                'success' => 1,
                'message' => $translator->translate('Messages has been sent successfully.', 'Mail'),
            ]);

        } else {
            return new JsonModel([
                'success' => 0,
                'messages' => $form->getMessages(),
            ]);
        }
    }
}

