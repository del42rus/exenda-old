CREATE TABLE IF NOT EXISTS `ex_mail_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(255) NOT NULL,
  `config_value` varchar(255),
  PRIMARY KEY (`id`)
)Engine=InnoDb DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_mail_event_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `description` text,
  KEY (`ident`),
  PRIMARY KEY (`id`)
) Engine=InnoDb CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_type_id` int(11),
  `type` varchar(4) DEFAULT 'text',
  `active` int(1) DEFAULT 0,
  `sender_email` varchar(255) NOT NULL,
  `sender_name` varchar(255) NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `carbon_copy` varchar(255),
  `blind_carbon_copy` varchar(255),
  `subject` varchar(255) NOT NULL,
  `text` text,
  `language` varchar(2) NOT NULL DEFAULT 'en',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `keep_on_server` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`event_type_id`) REFERENCES `ex_mail_event_types`(`id`) ON DELETE SET NULL ON UPDATE CASCADE
) Engine=InnoDb CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_mail_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(255) NOT NULL,
  `sender_name` varchar(255),
  `recipient` varchar(255) NOT NULL,
  `cc` varchar(255),
  `bcc` varchar(255),
  `subject` varchar(255) NOT NULL,
  `content` text,
  `type` varchar(255) NOT NULL, #inbox, sent, trash
  `is_read` int(1) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) Engine=InnoDb CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ex_mailouts` (
  `id` int(11) NOT NULL  AUTO_INCREMENT,
  `recipient` VARCHAR(255) NOT NULL,
  `event_type_ident` VARCHAR(255) NOT NULL,
  `data` TEXT,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `is_sent` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDb CHARSET=utf8;