��    P      �  k         �     �     �     �     �  :   �  7   (  8   `  3   �  1   �  1   �     1     B  
   F     Q     T     c     h     u     �     �  .   �     �     �     �     �     �  	   �  
   	  )   	     =	     I	  
   N	     Y	  
   _	     j	     z	     �	     �	     �	  $   �	     �	     �	     �	     �	     �	     �	     �	     
     
  +   "
     N
     Z
  	   _
     i
     n
     �
     �
  
   �
     �
     �
  '   �
  +   �
  	          #        ;     U     X     ^     c  5   v     �     �     �     �     �     �  (   �  	     �  !     �                -  k   E  ]   �  Z     `   j  J   �  D        [     w     �  
   �  +   �  
   �     �       5   %  *   [  L   �     �     �     �     �  '     !   =     _  0   t     �     �     �     �     �  0        6  
   N  !   Y     {  7   �     �     �     �      �           ,     9  !   L     n  9   �     �     �     �     �  6        G     U     s     �     �  ,   �  5   �  !     
   6  1   A  $   s     �     �  (   �  (   �  Q     (   X     �  '   �     �     �     �  �   �     �         N   (             )                 L   ;   
   /           5   #   *       4   &   A      ?           <   .   I      B                %       O          F       :       J       C       =         	   2                 K   3      0   D                  8       "   -   1   +       @   9      P       '   6   >   7              ,   M         G              H   E            $           !        Action Active Add template Add type Are you sure that you want to delete selected event types? Are you sure that you want to delete selected messages? Are you sure that you want to delete selected templates? Are you sure that you want to delete the event type Are you sure that you want to delete the message? Are you sure that you want to delete the template Available fields Bcc Blind copy Cc Confirm Delete Copy Created date Default E-mail Default name Default settings Default settings has been successfully updated Delete Description E-mail Edit Edit template Edit type Encryption Event type has been updated successfully. Event types From Identifier Inbox Is active? Keep on server? Language Login Mail templates Message type Messages has been sent successfully. Name No None Nothing found Nothing selected Password Read Remove selected SMTP settings SMTP settings has been successfully updated SMTP-server Save Saving... Send Send a test message Sender Sender name Sending... Server port Subject Template has been updated successfully. Template with id &laquo;%s&raquo; not found Templates Text The form was completed with errors. There are no new messages To Trash Type Type of mail event Type of mail event with id &laquo;%s&raquo; not found Types of mail events Unread Updated date Use SMTP Write to us Yes Your message has been sent successfully. Your name Project-Id-Version: Mail
POT-Creation-Date: 2015-10-05 12:11+0700
PO-Revision-Date: 2015-10-05 12:11+0700
Last-Translator: Ilya <coder4@web-axioma.ru>
Language-Team: ilya <coder4@web-axioma.ru>
Language: Russian
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop;translate
X-Poedit-Basepath: /var/www/skidka.me/www/module
X-Poedit-SearchPath-0: Mail
 Действие Активен Добавить шаблон Добавить тип Вы уверены, что хотите удалить выбранные почтовые события? Вы уверены что хотите удалить выбранные сообщения? Вы уверены, что хотите удалить выбранные шаблоны? Вы уверены, что хотите удалить тип почтового события Вы уверены что хотите удалить сообщение? Вы уверены что хотите удалить шаблон? Доступные поля Скрытая копия Скрытая копия Копия Подтверждение удаления Копия Дата создания E-mail отправителя Имя отправителя по умолчанию Настройки по умолчанию Настройки по умолчанию успешно обновлены Удалить Описание E-mail Редактировать Редактировать шаблон Редактировать тип Шифрование Событие успешно обновлено Тип событий От кого Идентификатор Входящие Активен? Хранить письма на сервере? Язык шаблона Логин Шаблоны сообщений Тип сообщения Сообщение успешно отправлено. Навание Нет Нет Ничего не найдено Ничего не выбрано Пароль Прочитано Удалить выбранные Настройки SMTP Настройки SMTP успешно обновлены Сервер SMTP Сохранить Сохранение... Отправить Отправить тестовое сообщение От кого Имя отправителя ИДет отправка... Порт сервера Тема Шаблон успешно обновлен Шаблон с ID &laquo;%s&raquo; не найден Шаблоны сообщений Текст Форма заполнена с ошибками Новых сообщений нет Кому Удаленные Тип почтового события Тип почтового события Тип почтового события с ID &laquo;%s&raquo; не найден Типы почтовых событий Не прочитано Последнее обновление Использовать SMTP Написать нам Да Ваше сообщение успешно отправлено. Спасибо за интерес, проявленный к нашему сервису. Имя 