<?php
namespace Pages\Form\Element;

use Zend\Form\Element\Select;

class ContentSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        
        $model = $this->getOption('model');
        $page = $this->getOption('page');

        $translator = $model->getServiceLocator()->get('translator');
        $this->setEmptyOption($translator->translate('New content', 'Pages'));

        $detached = $model->findDetachedContent();
        
        $contentList = [];
        
        foreach ($detached as $detachedContent) {
            $contentList[$detachedContent->getId()] = $detachedContent->getTitle() . "({$detachedContent->getId()})";
        }

        // Add page edited content to list
        if (null !== $page && null !== $page->getContent()) {
            $contentList[$page->getContent()->getId()] = $page->getContent()->getTitle() . "({$page->getContent()->getId()})";

            // set selected value
            $this->setValue($page->getContent()->getId());
        }

        if (is_array($contentList)) {
            $this->setValueOptions($contentList);
        }
    }
    
    public function getInputSpecification()
    {
        $spec = [
            'name' => $this->getName(),
            'required' => false,
            'validators' => [
                $this->getValidator()
            ]
        ];

        return $spec;
    }
}

