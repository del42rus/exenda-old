<?php
namespace Pages\Form\Element;

use Zend\Form\Element\Select;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Filter\FilterChain;

class ModuleSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        
        $this->setAttribute('id', 'module');
        
        $model = $this->getOption('model');
        $page = $this->getOption('page');
        
        $modules = $this->getModules($model->getServiceLocator());
        $this->setValue('content');

        if (null !== $page && !$page->hasLink()) {
            
            $module = strtolower($page->getModuleName());
            //set selected value
            $this->setValue($module);
        }
        
        $this->setValueOptions($modules);
    }

    private  function getModules(ServiceLocatorInterface $serviceLocator)
    {
        $moduleManager = $serviceLocator->get('ModuleManager');
        $modules = $moduleManager->getModules();

        $filterChain = new FilterChain();
        $filterChain->attachByName('wordcamelcasetodash')
                    ->attachByName('stringtolower');

        $options = [];

        foreach ($modules as $moduleName) {
            $module = $moduleManager->getModule($moduleName);

            if (method_exists($module, 'getModuleName')) {
                $moduleName = $module->getModuleName();
                $options[$filterChain->filter($moduleName)] = $moduleName;
            }
        }

        return $options;
    }
    
    public function getInputSpecification()
    {
        $spec = [
            'name'     => $this->getName(),
            'required' => false,
            'validators' => [
                [
                    'name' => 'Pages\Form\Validator\HasChildren',
                    'options' => [
                        'model' => $this->getOption('model')
                    ]
                ]
            ]];

        return $spec;
    }
}

