<?php
namespace Pages\Form\Element;

use Zend\Form\Element\Select;
use Zend\Stdlib\Glob;

class TemplateSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('id', 'template');
        
        $model = $this->getOption('model');
        $page = $this->getOption('page');

        $app = $model->getServiceLocator()->get('Application\Model\Application');
        
        $templates = $this->getTemplates();
        
        if (null !== $page) {
            $template = $page->getTemplate();
            
            //set selected value
            $this->setValue($template);
        }
        
        $this->setValueOptions($templates);
    }

    private function getTemplates()
    {
        $templateDir = MODULE_PATH . '/Application/view/layout/index/';
        $templates = Glob::glob($templateDir . '*.phtml', 0);

        $options = [];

        foreach ($templates as $templateName) {
            $templateName = basename($templateName);
            $options[$templateName] = $templateName;
        }

        return $options;
    }
    
    public function getInputSpecification()
    {
        $spec = [
            'name' => $this->getName(),
            'required' => false,
            'validators' => [
                $this->getValidator()
            ]
        ];

        return $spec;
    }
}

