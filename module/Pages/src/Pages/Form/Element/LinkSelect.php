<?php
namespace Pages\Form\Element;

use Zend\Form\Element\Select;

class LinkSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('id', 'link');
        
        $model = $this->getOption('model');
        $page = $this->getOption('page');
        
        $pages = $model->getPages();
        
        foreach ($pages as $_page) {
            $_pages[$_page->getId()] = $_page->getTitle() . "({$_page->getId()})";
        }

        if (null !== $page) {
            if ($page->hasLink()) {
                $this->setValue($page->getLink()->getId());
            }
            
            unset($_pages[$page->getId()]);
            
            if ($page->hasReferrer()) {
                $referrers = $page->getReferrerChain();
                
                if (count($referrers)) {
                    foreach ($referrers as $referrer) {
                        unset($_pages[$referrer->getId()]);
                    }
                }
            }
        }

        if (is_array($_pages)) {
            $this->setValueOptions($_pages);
        }
    }
    
    public function getInputSpecification()
    {
        $spec = [
            'name' => $this->getName(),
            'required' => false,
            'validators' => [
                $this->getValidator()
            ]
        ];

        return $spec;
    }
}

