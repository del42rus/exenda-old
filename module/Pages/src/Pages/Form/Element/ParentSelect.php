<?php
namespace Pages\Form\Element;

use Pages\Model\Entity\Page;
use Zend\Form\Element\Select;

class ParentSelect extends Select
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        
        $model = $this->getOption('model');
        $page = $this->getOption('page');
        $exclude = [];

        if ($page instanceof Page) {
            $exclude = $page->getChildrenIds();
            $exclude[] = $page->getId();

            $parent = $page->getParent();

            if ($parent) {
                $this->setValue($parent->getId());
            }
        }

        $pages = $model->getPages(null, $exclude);
        $values = [];

        foreach ($pages as $page) {
            if ($page->isModule()) {
                continue;
            }
            $values[$page->getId()] = $page->getTitle() . '(' . $page->getId() . ')';
        }

        if ($parent = $this->getOption('parent')) {
            $this->setValue($parent);
        }
        
        $this->setValueOptions($values);
    }
}

