<?php
/**
 * Exenda CMS
 * 
 * @category Zend
 * @package Pages_Form
 */
namespace Pages\Form\Validator;

use Zend\Validator\AbstractValidator;
use Pages\Model\Pages as Model;

/**
 * @category Zend
 * @package Pages_Form
 * @subpackage Validator
 */
class HasChildren extends AbstractValidator
{  
    const HAS_CHILDREN = 'hasChildren';
    
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = [
        self::HAS_CHILDREN => "Page '%alias%' has children. You can not set module '%value%' for this pages"
    ];
    
    /**
     * @var array
     */
    protected $messageVariables = [
        'alias' => 'alias'
    ];
    
    /**
     * @var string
     */
    protected $alias;
    
    /**
     * @var \Pages\Model\Pages
     */
    protected $model;
         
    /**
     * Constructor
     * 
     * @param array $options
     */
    public function __construct($options = []) {
        parent::__construct($options);
    }
    
    /**
     * Return true if text page has not children
     * 
     * @param string $value
     * @param array $context
     * @return boolean
     */
    public function isValid($value, $context = null) 
    {
        $this->setValue(ucfirst($value));
        
        if ($id = $context['id']) {
            $page = $this->model->getPageById($id);
        
            $this->setAlias($page->getAlias());
            
            if ($page->hasChildren() && $value != 'content') {
                $this->error(self::HAS_CHILDREN);
                return false;
            }
        }
        return true;
    }
    
    /**
     * Set alias variable for message template
     * 
     * @param string $alias
     * @return self
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }
    
    /**
     * Set model
     * 
     * @param \Pages\Model\Pages $model
     * @return self
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }
}
