<?php
/**
 * Exenda CMS
 * @category Zend
 * @package Pages_Form
 */
namespace Pages\Form\Validator;

use Zend\Validator\AbstractValidator;

/**
 * @category Zend
 * @package Pages_Form
 * @subpackage Validator
 */
class UniqueAlias extends AbstractValidator
{
    const ALIAS_EXISTS = 'aliasExists';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = [
        self::ALIAS_EXISTS => "Alias '%value%' already exists"
    ];

    /**
     *
     * @var \Pages\Model\Pages
     */
    protected $model;

    public function __construct($options = null) {
        parent::__construct($options);
    }

    /**
     * Return true if there isn't the same page alias on the same level
     * 
     * @param string $value
     * @param array $context
     * @return boolean
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        if ($context['parentId']) {
            $parentId = $context['parentId'];
        }

        $page = $this->model->getPage($value, $parentId);

        if (null === $page || $page->getId() == $context['id']) {
            return true;
        }       

        $this->error(self::ALIAS_EXISTS);
        return false;
    }

    /**
     * Set model
     * @param \Application\Mvc\Model\AbstractModel $model
     */
    public function setModel(\Application\Mvc\Model\AbstractModel $model)
    {
        $this->model = $model;
    }
}
