<?php
namespace Pages\Form;

use Application\Form\Form;

use Pages\Form\Element\LinkSelect;
use Pages\Form\Element\ModuleSelect;
use Pages\Form\Element\ParentSelect;
use Pages\Form\Element\TemplateSelect;
use Pages\Form\Element\ContentSelect;

class PageAdd extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');

        $model = $this->getModel();

        if (null !== $page = $this->getOption('page')) {
            $url = $model->getPageUrl($page);
        }

        $parentSelect = new ParentSelect('parentId', [
            'model' => $model,
            'page' => $this->getOption('page'),
            'parent' => $this->getOption('parentId')
        ]);

        $parentSelect->setLabel(gettext('Select a parent page'));
        $parentSelect->setEmptyOption('None');

        $this->add($parentSelect);

        $this->add([
            'name' => 'url',
            'attributes' => [
                'type' => 'text',
                'value' => $url,
                'readonly' => true
            ]
        ]);

        $this->add([
            'name' => 'seoTitle',
            'type' => 'text',
            'options' => [
                'label' => gettext('Title SEO')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'description',
            'type' => 'Zend\Form\Element\Textarea',
            'options' => [
                'label' => gettext('Description')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'keywords',
            'type' => 'Zend\Form\Element\Textarea',
            'options' => [
                'label' => gettext('Keywords')
            ],
            'attributes' => [
                'class' => 'form-control'
            ]
        ]);

        $this->add([
            'name' => 'title',
            'options' => [
                'label' => gettext('Title'),
            ],
            'attributes' => [
                'type' => 'text',
                'id' => 'title',
                'placeholder' => gettext('Page title'),
                'required' => 'required'
            ]
        ]);

        $this->add([
            'name' => 'alias',
            'options' => [
                'label' => gettext('Alias'),
            ],
            'attributes' => [
                'type' => 'text',
                'id' => 'alias',
                'placeholder' => gettext('Page alias'),
                'required' => 'required'
            ]
        ]);

        $link = new LinkSelect('linkId', [
            'model' => $model,
            'page' => $page,
            'label' => gettext('Link to page'),
        ]);

        $sl = $this->getModel()->getServiceLocator();
        $translator = $sl->get('translator');

        $link->setEmptyOption($translator->translate('None', 'Pages'));
        $this->add($link);

        $moduleSelect = new ModuleSelect('module', [
            'model' => $model,
            'page'  => $page,
            'label' => gettext('Module'),
        ]);

        $disabled = $page && $page->hasLink();
        $moduleSelect->setAttribute('disabled', $disabled);
        $this->add($moduleSelect);

        $contentSelect = new ContentSelect('content', [
            'model' => $model,
            'page'  => $page,
            'label' => gettext('Content'),
        ]);

        $disabled = $page && ($page->hasLink() || $page->getModuleName() !== 'content');
        $contentSelect->setAttribute('disabled', $disabled);
        $this->add($contentSelect);

        $templateSelect = new TemplateSelect('template', [
            'model' => $model,
            'page'  => $page,
            'label' => gettext('Choose template'),
        ]);

        $disabled = $page && $page->hasLink();
        $templateSelect->setAttribute('disabled', $disabled);
        $this->add($templateSelect);

        if (null !== $page) {
            $parent = (int) $page->getParentId();
        }

        $this->add([
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'published',
            'options' => [
                'label' => $translator->translate('Publish', 'Pages'),
                'use_hidden_element' => false,
            ],
            'attributes' => [
                'id' => 'published',
            ],
            'label_attributes' => [
                'for' => 'published'
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'hidden',
            'options' => [
                'label' => $translator->translate('Hide', 'Pages'),
                'use_hidden_element' => false,
            ],
            'attributes' => [
                'id' => 'hidden',
            ],
            'label_attributes' => [
                'for' => 'hidden'
            ]
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Hidden',
            'name' => 'id',
        ]);

        $this->add([
            'type' => 'Zend\Form\Element\Submit',
            'name' => 'submit',
            'attributes' => [
                'value' => gettext('Add page'),
                'class' => 'btn btn-primary',
                'data-loading-text' => $translator->translate('Saving...', 'Pages')
            ]
        ]);

        $inputFilter = $this->getInputFilter();

        $inputFilter->add([
            'name' => 'parentId',
            'required' => false,
        ]);

        $inputFilter->add([
            'name' => 'seoTitle',
            'required' => false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max'      => 255,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'title',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 255,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name'     => 'alias',
            'required' => true,
            'filters'  => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                [
                    'name'    => 'StringLength',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 255,
                    ],
                ],
                [
                    'name' => 'Pages\Form\Validator\UniqueAlias',
                    'options' => [
                        'model' => $model
                    ]
                ]
            ],
        ]);

        // Next three inputs can be removed or added to form dynamically,
        // so add them into InputFilter explicitly
        $inputFilter->add([
            'name' => 'module',
            'required' => false,
        ]);

        $inputFilter->add([
            'name' => 'content',
            'required' => false,
        ]);

        $inputFilter->add([
            'name' => 'template',
            'required' => false,
        ]);

        $inputFilter->add([
            'name' => 'published',
            'required' => false,
        ]);

        $inputFilter->add([
           'name' => 'published',
           'required' => false,
        ]);

        $inputFilter->add([
            'name' => 'hidden',
            'required' => false,
        ]);
    }
}

