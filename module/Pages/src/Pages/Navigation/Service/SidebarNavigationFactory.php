<?php
namespace Pages\Navigation\Service;

use Zend\Navigation\Navigation;
use Zend\ServiceManager\ServiceLocatorInterface;

class SidebarNavigationFactory extends DefaultNavigationFactory
{    
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Zend\Navigation\Navigation
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $pages = $serviceLocator->get('Pages\Model\Pages');
        
        $mvcEvent = $serviceLocator->get('Application')->getMvcEvent();
        $matches = $mvcEvent->getRouteMatch();
        
        // Current page id
        $parentId = $matches->getParam('pageId');
        
        $parent = $pages->getPageById($parentId);
        
        if (null !== $parent && null !== $parents = $parent->getParentPages()) {
            $parent = array_pop($parents);
            $parentId = $parent->getId();
        }
        
        $pages = $this->getPages($serviceLocator, $parentId, true);
        
        return new Navigation($pages);
    }

    /**
     * Get factory name
     * @return string
     */
    protected function getName()
    {
        return 'sidebar';
    }
}
