<?php
namespace Pages\Navigation\Service;

use Zend\Navigation\Navigation;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Default navigation factory.
 *
 * @category  Zend
 * @package   Zend_Navigation
 * @subpackage Service
 */
class BottomNavigationFactory extends DefaultNavigationFactory
{
    /**
     * @var array
     */
    protected $pages = [];
    
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Zend\Navigation\Navigation
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $pages = $this->getPages($serviceLocator, null);

        return new Navigation($pages);
    }

    /**
     * Get factory name
     * @return string
     */
    protected function getName()
    {
        return 'bottom';
    }
}
