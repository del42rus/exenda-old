<?php
namespace Pages\Navigation\Service;

use Zend\Navigation\Navigation;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DefaultNavigationFactory implements FactoryInterface
{
    /**
     * @var array
     */
    protected $pages = [];

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return \Zend\Navigation\Navigation
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $pages = $this->getPages($serviceLocator, null, true);

        return new Navigation($pages);
    }
    
    /**
     * Get all pages from the database
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     * @param int|null $parent
     * @param boolean $recursive
     * @return array
     */
    protected function getPages(ServiceLocatorInterface $serviceLocator, $parent = null, $recursive = false)
    {
        if (empty($this->pages)) {
            $pages = $serviceLocator->get('Pages\Model\Pages')->getPageChildren($parent, $recursive);

            $this->pages = $this->preparePages($serviceLocator, $pages);
        }
        return $this->pages;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @param $pages
     * @return array
     */
    protected function preparePages(ServiceLocatorInterface $serviceLocator, $pages)
    {
        $mvcEvent = $serviceLocator->get('Application')->getMvcEvent();
        $pageManager = $serviceLocator->get('Pages\Model\Pages');

        $activePageId = $mvcEvent->getRouteMatch()->getParam('pageId');

        foreach ($pages as $key => &$page) {
            if (!$page['published'] || $page['hidden']) {
                unset($pages[$key]);
                continue;
            }

            $pageObj = $pageManager->getPageById($page['id']);

            $ids = $pageManager->getPageChildrenIds($page['id'], true);

            $active = false;

            if ($activePageId == $page['id'] || in_array($activePageId, $ids)) {
                $active = true;
            }

            $uriPage = [
                'title' => $page['title'],
                'label'=> $page['title'],
                'uri' => $pageObj->getPath(),
                'order' => $page['position'],
                'active' => $active
            ];

            if (count($page['__children'])) {
                $uriPage['pages'] = $this->preparePages($serviceLocator, $page['__children']);
            }

            $page = $uriPage;
        }
        
        return $pages;
    }
    
    /**
     * Get factory name
     * @return string
     */
    protected function getName()
    {
        return 'default';
    }
}
