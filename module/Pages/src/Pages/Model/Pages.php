<?php
namespace Pages\Model;

use Application\Mvc\Model\AbstractModel;
use Pages\Model\Entity\Page;
use Zend\Paginator\Paginator;

use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Doctrine\ORM\Query;

class Pages extends AbstractModel
{
    public function getPageById($id)
    {
        $page = $this->getEntity('Page')->findOneById($id);
        return $page;
    }

    public function getPages($module = null, $exclude = [])
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $qb->select('p')
            ->from('Pages\Model\Entity\Page', 'p');

        if ($module) {
            $qb->where($qb->expr()->eq('p.module', $module));
        }

        if (!empty($exclude)) {
            $qb->andWhere($qb->expr()->notIn('p.id', $exclude));
        }

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function getPage($alias, $parent = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $stmt = $qb->select('p')
                   ->from('Pages\Model\Entity\Page', 'p');

        if (!$parent) {
            $stmt = $stmt->where($qb->expr()->andX(
                        $qb->expr()->eq('p.alias', '?1'),
                        $qb->expr()->isNull('p.parent')
                    ));

            $query = $stmt->getQuery();
            $query->setParameter(1, $alias);

        } else {
            $stmt= $stmt->where($qb->expr()->andX(
                        $qb->expr()->eq('p.alias', '?1'),
                        $qb->expr()->eq('p.parent', '?2')
                    ));

            $query = $stmt->getQuery();
            $query->setParameters([1 => $alias, 2 => $parent]);
        }

        return $query->getOneOrNullResult();
    }

    public function getPagesByIds($ids)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
                    ->from('Pages\Model\Entity\Page', 'p')
                    ->where($qb->expr()->in('p.id', $ids))
                    ->getQuery();

        return $query->getResult();
    }

    public function getSiblingsPages(Page $page, $type = null)
    {
        $em = $this->getEntityManager();
        
        $qb = $em->createQueryBuilder();
        
        $parent = $page->getParent();

        $stmt = $qb->select('p')
                    ->from('Pages\Model\Entity\Page', 'p');
        
        $stmt->where($qb->expr()->neq('p.id', '?2'));
        
        if (null === $parent) {
            $stmt->andWhere($qb->expr()->isNull('p.parent'));
        } else {
            $stmt->andWhere($qb->expr()->eq('p.parent', '?1'));
        }
        
        if ($type == 'next') {
            $stmt->andWhere($qb->expr()->gt('p.position', '?3'));
        } elseif ($type == 'prev') {
            $stmt->andWhere($qb->expr()->lt('p.position', '?3'));
        }
        
        $query = $stmt->getQuery();
        
        if (null !== $parent) {
            $query->setParameter(1, $parent->getId());
        }
        
        $query->setParameter(2, $page->getId());
        
        if ($type == 'next' || $type == 'prev') {
            $query->setParameter(3, $page->getPosition());
        }
        
        return $query->getResult();
    }

    public function findDetachedContent()
    {
        $contentManager = $this->getServiceLocator()->get('ContentManager\Model\ContentManager');
        return $contentManager->findDetachedContent();
    }

    public function savePage(Page $page)
    {
        $id = (int) $page->getId();
        $em = $this->getEntityManager();

        if ($id == 0) {
            $em->persist($page);
        }

        $em->flush();
    }

    public function removePage(Page $page)
    {
        $em = $this->getEntityManager();

        $deletedCount = $this->getPageChildrenCount($page->getId()) + 1;

        $referrers = $page->getReferrerChain();
        
        if (count($referrers)) {
            foreach ($referrers as $referrer) {
                $em->remove($referrer);
            }
            $deletedCount += count($referrers);
        }
        
        $em->remove($page);
        $em->flush();

        return $deletedCount;
    }

    public function removePages($ids)
    {
        $pages = $this->getPagesByIds($ids);

        foreach ($pages as $page) {
            $this->removePage($page);
        }
    }

    public function copyPage($page, $parent = null, $recursive = true, $solveAliasConflict = true)
    {
        $em = $this->getEntityManager();

        $newPage = clone $page;
        $content = $page->getContent();

        if ($content) {
            $newContent = clone $content;
            $newPage->setContent($newContent);
        }

        $newPage->setParent($parent);

        $em->persist($newPage);
        $em->flush();

        if ($recursive) {
            foreach ($page->getChildren() as $child) {
                $this->copyPage($child, $newPage, true, false);
            }
        }

        if ($solveAliasConflict) {
            $children = $this->getPagesByParentId($parent, Query::HYDRATE_OBJECT);
            $children = new \ArrayIterator($children);

            $regexp = '/[a-zA-Z0-9_-]+-copy(\d+)/';
            $alias = $newPage->getAlias();
            $number = 1;

            while ($children->valid()) {
                $child = $children->current();

                if ($alias == $child->getAlias()) {
                    if (preg_match($regexp, $child->getAlias(), $matches)) {
                        $number = ++$matches[1];
                    }

                    $alias = $newPage->getAlias() . '-copy' . $number;
                    $children->rewind();
                } else {
                    $children->next();
                }
            }

            if ($newPage->getAlias() != $alias) {
                $newPage->setAlias($alias);
            }

            $em->flush();
        }
    }

    public function movePage($page, $newParent, $position)
    {
        $em = $this->getEntityManager();
        $page->setParent($newParent);

        $children = $this->getPagesByParentId($newParent, Query::HYDRATE_OBJECT);
        $children = new \ArrayIterator($children);

        $regexp = '/[a-zA-Z0-9_-]+-copy(\d+)/';
        $alias = $page->getAlias();
        $number = 1;

        while ($children->valid()) {
            $child = $children->current();

            if ($alias == $child->getAlias()) {
                if (preg_match($regexp, $child->getAlias(), $matches)) {
                    $number = ++$matches[1];
                }

                $alias = $page->getAlias() . '-copy' . $number;
                $children->rewind();
            } else {
                $children->next();
            }
        }

        if ($page->getAlias() != $alias) {
            $page->setAlias($alias);
        }

        $em->flush();

        $page->setPosition($position);
        $em->flush();
    }

    public function getPageReferrerIds($ids)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p.id')
                    ->from('Pages\Model\Entity\Page', 'p')
                    ->where($qb->expr()->in('p.link', '?1'))
                    ->setParameter(1, $ids)
                    ->getQuery();

        return $query->getResult('FetchColumnHydrator');
    }

    /**
     * Get first level children for parent page
     * 
     * @param int|null $parent
     * @return array
     */
    public function getPagesByParentId($parent = null, $paged = null, $itemCountPerPage = 50, $hydrateMode = Query::HYDRATE_ARRAY)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $stmt = $qb->select('p')
                    ->from('Pages\Model\Entity\Page', 'p');

        if (null === $parent) {
            $stmt = $stmt->where($qb->expr()->isNull('p.parent'))
                         ->orderBy('p.position', 'ASC');
            $query = $stmt->getQuery();
        } else {
            $stmt= $stmt->where($qb->expr()->eq('p.parent', '?1'))
                        ->orderBy('p.position', 'ASC');

            $query = $stmt->getQuery();
            $query->setParameter(1, $parent);
        }


        if (null !== $paged) {
            $adapter = new DoctrineAdapter(new ORMPaginator($query));
            $paginator = new Paginator($adapter);

            $paginator->setCurrentPageNumber((int) $paged)
                ->setItemCountPerPage($itemCountPerPage)
                ->setPageRange(5);

            return $paginator;
        }

        return $query->getResult($hydrateMode);
    }
    
    /**
     * Get entire page hierarchy
     * 
     * @return array
     */
    public function getChildrenHierarchy()
    {
        return $this->getPageChildren(null, true);
    }

    /**
     * Get all children for some page if $recursive flag is true
     * else return first level children
     * 
     * @param int|null $pageId
     * @param boolean $recursive
     * @return array
     */
    public function getPageChildren($pageId = null, $recursive = false)
    {
        $pages = $this->getPagesByParentId($pageId);

        foreach($pages as &$page) {
            if ($recursive) {
                $page['__children'] = $this->getPageChildren($page['id'], true);
            }
        }
        return $pages;
    }

    /**
     * Get all children pages ids if $recursive flag is true
     * else return only first level children pages ids 
     * 
     * @param int|null $pageId
     * @param boolean $recursive
     * @return array
     */
    public function getPageChildrenIds($pageId, $recursive = false)
    {
        $pages = $this->getPagesByParentId($pageId);

        $ids = [];

        foreach($pages as $page) {
            $ids[] = $page['id'];
            if ($recursive) {
                $ids = array_merge($ids, $this->getPageChildrenIds($page['id'], true));
            }
        }

        return $ids;
    }

    /**
     * Get number of all children pages (not only first level)
     * 
     * @param int|null $pageId
     * @return int
     */
    public function getPageChildrenCount($pageId)
    {
        $ids = $this->getPageChildrenIds($pageId, true);
        return count($ids);
    }

    /**
     * Get real page url 
     * 
     * @param \Pages\Model\Entity\Page $page
     * @return string
     */
    public function getPageUrl($page)
    {
        $request = $this->getServiceLocator()->get('Request');
        $host = $request->getUri()->getHost();

        $url = $page instanceof Page ? $host . rtrim($page->getPath(), '/') : $host;

        return $url;
    }
    
    /**
     * Get ids of pages link to $page
     * 
     * @param \Pages\Model\Entity\Page $page
     * @return array
     */
    public function getReferrerIds($page)
    {
        $ids = [];
        $referrers = $page->getReferrerChain();
        
        foreach ($referrers as $referrer) {
            $ids[] = $referrer->getId();
        }
        
        return $ids;
    }

    public function getRecentlyUpdatedPages()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p')
                    ->from('Pages\Model\Entity\Page', 'p')
                    ->andWhere($qb->expr()->eq("DATE_DIFF(CURRENT_DATE(), DATE(p.updated))", 1))
                    ->getQuery();

        return $query->getResult();
    }

    public function getPageIds()
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();

        $query = $qb->select('p.id')
                    ->from('Pages\Model\Entity\Page', 'p')
                    ->getQuery();

        return $query->getResult('FetchColumnHydrator');
    }

    public function generateSitemap($subdomain, $filename, $compress = false)
    {
        $config = $this->getServiceLocator()->get('Config');
        $domain = $config['domain'];

        $filename = $compress ? $filename . '.gz' : $filename;
        $content = '';

        if (!file_exists($filename)) {
            $pagesModel = $this->getServiceLocator()->get('Pages\Model\Pages');
            $pages = $pagesModel->getPages();

            if (count($pages)) {
                // Create a sitemap based on pages
                $xml = new \XMLWriter();

                $xml->openMemory();
                $xml->startDocument('1.0', 'UTF-8');
                $xml->startElement('urlset');
                $xml->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

                foreach ($pages as $page) {
                    $xml->startElement('url');
                    $xml->writeAttribute('id', $page->getId());

                    $location = 'http://' . $subdomain . '.' . $domain . $page->getPath();

                    $xml->writeElement('loc', $location);
                    $xml->writeElement('lastmod', $page->getUpdated()->format('Y-m-d'));
                    $xml->writeElement('changefreq', 'monthly');
                    $xml->writeElement('priority', 1);
                    $xml->endElement(); // end a url element
                }

                $xml->endElement(); // end a urlset element
                $xml->endDocument();

                $content = $xml->outputMemory();
            }
        } else {
            if ($compress) {
                $filter = new \Zend\Filter\Decompress('Gz');
                $content = $filter->filter($filename);
            } else {
                $content = file_get_contents($filename);
            }

            $dom = new \DOMDocument();
            $dom->loadXML($content);
            $xpath = new \DOMXPath($dom);
            $xpath->registerNamespace('x', 'http://www.sitemaps.org/schemas/sitemap/0.9');

            // Remove non-existing companies from sitemap
            $items = $xpath->query("//x:urlset/x:url/@id");

            if ($items->length) {
                $ids = [];

                foreach ($items as $item) {
                    $ids[] = $item->value;
                }

                $pageIds = $this->getPageIds();
                $ids = array_diff($ids, $pageIds);

                foreach ($ids as $id) {
                    $element = $xpath->query("//x:urlset/x:url[@id='{$id}']")->item(0);
                    $dom->documentElement->removeChild($element);
                }
            }

            // Add or update
            $pages = $this->getRecentlyUpdatedPages();

            if (count($pages)) {
                foreach ($pages as $page) {
                    $elements = $xpath->query("//x:urlset/x:url[@id='{$page->getId()}']");

                    if ($elements->length) {
                        $element = $elements->item(0);

                        if (!$page->isPublished()) {
                            $dom->documentElement->removeChild($element);
                        } else {
                            $element->lastChild->nodeValue = $page->getUpdated()->format('Y-m-d');
                        }
                    } else {
                        if (!$page->isPublished()) {
                            continue;
                        }

                        $urlNode = $dom->createElement('url');
                        $urlNode->setAttribute('id', $page->getId());

                        $location = 'http://' . $subdomain . '.' . $domain . $page->getPath();

                        $locNode = $dom->createElement('loc', $location);
                        $lastmodNode = $dom->createElement('lastmod', $page->getUpdated()->format('Y-m-d'));
                        $changefreqNode = $dom->createElement('changefreq', 'monthly');
                        $priorityNode = $dom->createElement('priority', 0.8);

                        $urlNode->appendChild($locNode);
                        $urlNode->appendChild($lastmodNode);
                        $urlNode->appendChild($changefreqNode);
                        $urlNode->appendChild($priorityNode);

                        $dom->documentElement->appendChild($urlNode);
                    }
                }
            }

            $content = $dom->saveXML();
        }

        if ($content) {
            if ($compress) {
                $filter = new \Zend\Filter\Compress([
                    'adapter' => 'Gz',
                    'options' => [
                        'archive' => $filename
                    ],
                ]);

                $filter->filter($content);
            } else {
                file_put_contents($filename, $content);
            }

            return $filename;
        }

        return false;
    }
}
