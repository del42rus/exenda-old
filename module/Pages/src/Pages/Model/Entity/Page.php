<?php
namespace Pages\Model\Entity;

use ContentManager\Model\Entity\Content;
use Gedmo\Mapping\Annotation as Gedmo;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="pages")
 * @ORM\Entity(repositoryClass="Gedmo\Sortable\Entity\Repository\SortableRepository")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="module", type="string")
     */
    private $moduleName;

    /**
     * @ORM\Column(type="string")
     */
    private $alias;

    /**
     * @ORM\Column(type="string")
     */
    private $template;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="seo_title", type="string")
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $keywords;

    /**
     * @ORM\OneToOne(targetEntity="Page")
     * @ORM\JoinColumn(name="link", referencedColumnName="id")
     */
    private $link;

    /**
     * @ORM\OneToOne(targetEntity="Page", mappedBy="link")
     */
    private $referrer;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent", cascade={"persist", "remove"})
     */
    private $children;

    /**
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="Page", cascade={"persist"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $published;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $hidden;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updated;

    /**
     * @ORM\OneToOne (targetEntity="ContentManager\Model\Entity\Content", mappedBy="page", cascade={"persist"})
     */
    private $content;

    public function __construct()
    {
        $this->moduleName = 'content';
        $this->children = new ArrayCollection();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function getModuleName()
    {
        return $this->moduleName;
    }

    public function setModuleName($name)
    {
        $this->moduleName = $name;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($parent = null)
    {
        $this->parent = $parent;
        if ($parent instanceof Page) {
            $parent->addChildren($this);
        }
        return $this;
    }

    public function addChildren(Page $children = null)
    {
        return $this->children->add($children);
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getChildrenIds($recursive = true)
    {
        $ids = [];

        foreach ($this->children as $children) {
            $ids[] = $children->getId();

            if (true === $recursive) {
                $ids = array_merge($ids, $children->getChildrenIds(true));
            }
        }

        return $ids;
    }

    public function getParentId()
    {
        if (null !== $this->parent) {
            return $this->parent->getId();
        } else {
            return null;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setPosition($position)
    {
        $this->position = (int)$position; //int prevent SortableListener bug
    }

    public function getPosition()
    {
        return (int)$this->position;
    }

    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }

    public function getPublished()
    {
        return $this->published;
    }

    public function isPublished()
    {
        return (bool)$this->published;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    public function getHidden()
    {
        return $this->hidden;
    }

    public function isHidden()
    {
        return (bool)$this->hidden;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;

        if ($content instanceof Content) {
            $content->setPage($this);
        }

        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink(Page $link)
    {
        $this->link = $link;
        $link->setReferrer($this);
        return $this;
    }

    public function removeLink()
    {
        $this->link = null;
    }

    public function setReferrer($referrer)
    {
        $this->referrer = $referrer;
        return $this;
    }

    public function getReferrer()
    {
        return $this->referrer;
    }

    public function getLinkChain()
    {
        $link = $this->getLink();
        $chain = new ArrayCollection();

        while ($link) {
            $chain->add($link);

            if (!$link->hasLink()) {
                break;
            }
            $link = $link->getLink();
        }

        return $chain;
    }

    public function getReferrerChain()
    {
        $referrer = $this->getReferrer();

        $chain = new ArrayCollection();

        while ($referrer) {
            $chain->add($referrer);

            if (!$referrer->hasReferrer()) {
                break;
            }

            $referrer = $referrer->getReferrer();
        }

        return $chain;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function populate($data = [])
    {
        $this->id = $data['id'];
        $this->alias = $data['alias'];
        $this->title = $data['title'];
        $this->template = $data['template'];
        $this->published = (int)$data['published'];
        $this->hidden = (int)$data['hidden'];
        $this->moduleName = $data['module'] ?: $this->moduleName;

        $this->seoTitle = $data['seoTitle'];
        $this->description = $data['description'];
        $this->keywords = $data['keywords'];
    }

    /**
     * Magic getter to expose protected properties.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->$property;
    }

    /**
     * Magic setter to save protected properties.
     *
     * @param string $property
     * @param mixed $value
     */
    public function __set($property, $value)
    {
        $this->$property = $value;
    }

    public function getParentPages()
    {
        $pages = [$this];

        if (null === $this->getParentId()) {
            return $pages;
        } else {
            $parent = $this->getParent();
            $pages = array_merge($pages, $parent->getParentPages());
        }

        return $pages;
    }

    public function getPath()
    {
        $pages = $this->getParentPages();

        if (count($pages)) {
            foreach (array_reverse($pages) as $page) {
                $parts[] = $page->getAlias();
            }
            $path = join('/', $parts);
        }

        if ($path == 'index') {
            return '/';
        }

        return '/' . $path;
    }

    public function hasChildren()
    {
        return count($this->getChildren());
    }

    public function hasLink()
    {
        return (bool)$this->link;
    }

    public function isLink()
    {
        return (bool)$this->link;
    }
    
    public function hasReferrer()
    {
        return (bool) $this->referrer;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setSeoTitle($title)
    {
        $this->seoTitle = $title;
        return $this;
    }

    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    public function isModule()
    {
        $page = $this;

        if ($this->isLink()) {
            $page = $this->getLinkChain()->last();
        }

        return $page->getModuleName() != 'content';
    }
}
