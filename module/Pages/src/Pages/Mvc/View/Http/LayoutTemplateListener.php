<?php
namespace Pages\Mvc\View\Http;

use Zend\EventManager\EventManagerInterface as Events;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router;

class LayoutTemplateListener implements ListenerAggregateInterface
{
    /**
     * Listeners we've registered
     *
     * @var array
     */
    protected $listeners = [];

    /**
     * Attach listeners
     *
     * @param  Events $events
     * @return void
     */
    public function attach(Events $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setLayout']);
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setLayout']);
    }

    /**
     * Detach listeners
     *
     * @param  Events $events
     * @return void
     */
    public function detach(Events $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    public function setLayout(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        
        if (!$matches instanceof Router\RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        if (strpos($matches->getMatchedRouteName(), 'admin') !== false) {
            return;
        }

        $app = $e->getApplication();
        $serviceManager = $app->getServiceManager();

        $viewModel = $e->getViewModel();

        $pageId = $matches->getParam('pageId');
        $pages = $serviceManager->get('Pages\Model\Pages');

        $page = $pages->getPageById($pageId);
        $template = $page ? $page->getTemplate() : '404';

        $template = 'layout/index/' .  $template;

        // Set the layout template
        $viewModel->setTemplate($template);
    }
}
