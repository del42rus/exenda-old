<?php
namespace Pages\Mvc;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router;

use Zend\Filter\FilterChain;

class RouteListener implements
    ListenerAggregateInterface
{
    /**
     * @var \Zend\Stdlib\CallbackHandler[]
     */
    protected $listeners = array();

    /**
     *
     * @param \Zend\EventManager\EventManagerInterface $events
     * @param type $priority
     */
    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onRoute'), $priority);
    }

    /**
     * Detach all our listeners from the event manager
     *
     * @param  EventManagerInterface $events
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

    /**
     * Listen to the "route" event and attempt to route the request
     * Parse uri path and determine which module is requested
     * 
     * If no matches are returned, triggers "dispatch.error" in order to
     * create a 404 response.
     * 
     * @param  \Zend\Mvc\MvcEvent $e
     * @return RouteMatch
     */
    public function onRoute(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();

        if (!$routeMatch instanceof Router\RouteMatch) {
            // Can't do anything without a route match
            return;
        }

        if (strpos($routeMatch->getMatchedRouteName(), 'default') !== 0) {
            //Specially do nothing if route is not default
            return;
        }

        $target  = $e->getTarget();
        $request = $e->getRequest();
        $router  = $e->getRouter();

        $sm = $target->getServiceManager();

        $uri = $request->getUri();
        $path = rtrim($uri->getPath(), '/') ?: 'index';

        $parts = explode('/', trim($path, '/'));

        $pages = $sm->get('Pages\Model\Pages');
        
        $parent = null;

        // Find requested page by alias
        foreach ($parts as $alias) {
            if (null === $page = $pages->getPage($alias, $parent)) {
                //if page not found then break
                break;
            }

            // If page found is referring to real page or link
            // which referring to this page then get this real page
            if (null !== $page && $page->hasLink() && count($parts) == 1) {
                $page = $page->getLinkChain()->last();
            }

            // If page is modular page (e.g. article's module is attached to this page)
            // then there is no sense to parse uri further, because modular page can't
            // has children pages
            if (!$page->hasLink() && $page->getModuleName() != 'content') {
                $parts[0] = $page->getModuleName();
                break;
            }

            array_shift($parts);
            $parent = $page->getId();
        }

        if (null === $page) {
            $moduleName = $routeMatch->getParam('module');
            $moduleName = str_replace(' ', '', (ucwords(str_replace('-', ' ', $moduleName))));

            $moduleManager = $sm->get('ModuleManager');
            $module = $moduleManager->getModule($moduleName);

            if (null === $module) {
                return $this->dispatchError($target::ERROR_CONTROLLER_NOT_FOUND, $e, $target);
            }

            if ($request->isXmlHttpRequest() || !method_exists($module, 'getConfig')) {
                return;
            }

            $moduleConfig = $module->getConfig();

            if (array_key_exists('direct_access', $moduleConfig)) {
                if ($moduleConfig['direct_access'] === true) {
                    return;
                }

                $actionName = $routeMatch->getParam('action');;

                if (is_array($moduleConfig['direct_access']) &&
                    !empty($moduleConfig['direct_access']) &&
                    in_array($actionName, $moduleConfig['direct_access'])
                ) {
                    return;
                } else {
                    return $this->dispatchError($target::ERROR_CONTROLLER_NOT_FOUND, $e, $target);
                }
            } else {
                return $this->dispatchError($target::ERROR_CONTROLLER_NOT_FOUND, $e, $target);
            }
        }

        if (!$page->isPublished()) {
            return $this->dispatchError($target::ERROR_CONTROLLER_NOT_FOUND, $e, $target);
        }

        $moduleName = $page->getModuleName();

        // Assemble new uri path
        if ($moduleName == 'content') {
            $path = '/content';
        } else {
            $path = '/' . join('/', $parts);
        }

        $uri->setPath($path);
        $request->setUri($uri);

        // Create new route match
        $routeMatch = $router->match($request);

        $filterChain = new FilterChain();
        $filterChain->attachByName('worddashtocamelcase');

        $routeMatch->setParam('module', $filterChain->filter($moduleName));
        $routeMatch->setParam('pageId', $page->getId());

        if (!$routeMatch instanceof Router\RouteMatch) {
            return $this->dispatchError($target::ERROR_ROUTER_NO_MATCH, $e, $target);
        }

        $e->setRouteMatch($routeMatch);

        return $routeMatch;
    }
    
    /**
     * Triggers "dispatch.error" in order to create a 404 response.
     * 
     * @param string $type
     * @param \Zend\Mvc\MvcEvent $event
     * @param \Zend\Mvc\Application $application
     * @return mixed
     */
    public function dispatchError(
        $type,
        MvcEvent $event,
        \Zend\Mvc\Application $application
    ) {        
        $event->setError($type);

        $results = $application->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $event);
        if (count($results)) {
            $return  = $results->last();
        } else {
            $return = $event->getParams();
        }
        return $return;
    }

}

