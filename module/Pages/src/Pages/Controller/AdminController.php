<?php
namespace Pages\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Pages\Model\Entity\Page;
use ContentManager\Model\Entity\Content;
use Pages\Model\Pages;

use ZfcRbac\Exception\UnauthorizedException;

class AdminController extends AbstractActionController
{
    public function __construct(Pages $pages)
    {
        $this->pages = $pages;
    }

    public function indexAction()
    {
        $rootViewModel = $this->getEvent()->getViewModel();
        $translator = $this->getServiceLocator()->get('translator');

        $parentId = $this->params()->fromQuery('parent', null);

        if ($parentId) {
            $parent = $this->pages->getPageById($parentId);

            if ($parent && $parent->isModule()) {
                $text = $translator->translate('You can create a child page for module page.', 'Pages');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);

                return false;
            }
        }

        $page = $this->params()->fromRoute('page', 1);

        $pages = $this->pages->getPagesByParentId($parentId, $page);

        return new ViewModel([
            'pages' => $pages,
            'parent' => $parentId
        ]);
    }

    public function addAction()
    {
        if (!$this->isGranted('pages.create')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();
        $rootViewModel = $this->getEvent()->getViewModel();
        $translator = $this->getServiceLocator()->get('translator');
        $parentId = $request->getQuery('parent');

        if ($parentId) {
            $parent = $this->pages->getPageById($parentId);

            if ($parent && $parent->isModule()) {
                $text = $translator->translate('You can create a child page for module page.', 'Pages');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);

                return false;
            }
        }

        $form = $this->pages->getForm('PageAdd', [
            'parentId' => $parentId
        ]);

        if (null !== $parent) {
            $url = $this->pages->getPageUrl($parent);
        } else {
            $url = $request->getUri()->getHost();
        }

        $data['url'] = $url;

        $form->setData($data);

        if ($request->isPost()) { // form submit
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $page = new Page();
                $page->populate($form->getData());
                $parentId = (int) $request->getPost('parentId');

                $parent = $this->pages->getPageById($parentId);

                if (null !== $parent) {
                    $page->setParent($parent);
                }

                if ($request->getPost('linkId')) {
                    $link = $this->pages->getPageById($request->getPost('linkId'));
                    $page->setLink($link);
                    $page->setModuleName(null);
                } else {
                    $page->setModuleName($request->getPost('module'));
                }

                if ($request->getPost('module') == 'content') {
                    // Attach already exists content to page and save page
                    if ($request->getPost('content')) {
                        $contentManager = $this->getServiceLocator()->get('ContentManager\Model\ContentManager');
                        $content = $contentManager->findContentById($request->getPost('content'));

                        $page->setContent($content);
                    } else {
                        $content = new Content();
                        $content->setTitle($page->getTitle());

                        $page->setContent($content);
                    }
                }

                $this->pages->savePage($page);

                return $this->redirect()->toRoute('admin_default/wildcard', ['module' => 'pages', 'action' => 'edit', 'id' => $page->getId()]);
            } else {
                $text = $translator->translate('The form was completed with errors.', 'Pages');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $form,
        ]);

        return $viewModel;
    }

    public function editAction()
    {
        if (!$this->isGranted('pages.edit')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();
        
        if (!$id = $this->params()->fromRoute('id')) {
            $id = $request->getPost('id');
        } 

        $page = $this->pages->getPageById($id);

        if (null == $page) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $form = $this->pages->getForm('PageAdd', ['page' => $page]);
        $form->bind($page);

        // We will populate Page object manually
        $form->setBindOnValidate(\Zend\Form\FormInterface::BIND_MANUAL);

        if ($request->isPost()) {
            $form->setData($request->getPost());

            $rootViewModel = $this->getEvent()->getViewModel();
            $translator = $this->getServiceLocator()->get('translator');

            if ($form->isValid()) {
                $page->populate($form->getData(\Zend\Form\FormInterface::VALUES_AS_ARRAY));
                $parentId = (int) $request->getPost('parentId');

                if ($parentId) {
                    $parent = $this->pages->getPageById($parentId);

                    if (null !== $parent) {
                        $page->setParent($parent);
                    }
                } else {
                    $page->setParent(null);
                }

                $contentManager = $this->getServiceLocator()->get('ContentManager\Model\ContentManager');

                if ($request->getPost('linkId')) {
                    $link = $this->pages->getPageById($request->getPost('linkId'));
                    $page->setLink($link);

                    $page->setModuleName(null);
                    $page->setTemplate(null);

                    $content = $page->getContent();

                    if ($content instanceof Content) {
                        $content->detach();
                        $contentManager->save($content);
                    }
                } else {
                    $page->setModuleName($request->getPost('module'));

                    if ($page->hasLink()) {
                        $page->removeLink();
                    }
                }

                if ($request->getPost('module') == 'content') {
                    $oldContent = $page->getContent();

                    if ($contentId = $request->getPost('content')) {
                        if ($oldContent instanceof Content &&
                            ($oldContent->getId() != $contentId))
                        {
                            $oldContent->detach();
                            $contentManager->save($oldContent);
                        }

                        $newContent = $contentManager->findContentById($contentId);
                        $page->setContent($newContent);
                    } else {
                        if ($oldContent instanceof Content) {
                            // Detach an old content
                            $oldContent->detach();
                            $contentManager->save($oldContent);
                        }

                        $content = new Content();
                        $content->setTitle($page->getTitle());

                        $page->setContent($content);
                    }

                } else {
                    $content = $page->getContent();

                    if ($content instanceof Content) {
                        $content->detach();
                        $contentManager->save($content);
                    }
                }

                $this->pages->savePage($page);

                $text = $translator->translate('Page has been updated successfully.', 'Pages');
                $rootViewModel->setVariable('alert', ['type' => 'success', 'text' => $text]);
            } else {
                $text = $translator->translate('The form was completed with errors.', 'Pages');
                $rootViewModel->setVariable('alert', ['type' => 'danger', 'text' => $text]);
            }
        }

        $viewModel = new ViewModel([
            'form' => $form,
            'page' => $page,
        ]);

        return $viewModel;
    }

    public function deleteAction()
    {
        if (!$this->isGranted('pages.delete')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()
        ) {
            $this->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        $id = (int) $this->params()->fromRoute('id', null);

        if (!$id || !$page = $this->pages->getPageById($id)) {
            $this->getEvent()->getResponse()->setStatusCode(404);
            return new ViewModel();
        }

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()) {

            if ($request->getPost('delete_content')) {
                $contentManager = $this->getServiceLocator()->get('ContentManager\Model\ContentManager');
                $contentManager->removePageContent([$id], true);
            }

            $this->pages->removePage($page);

            return $this->redirect()->toUrl($request->getPost('backUrl'));
        }

        $translator = $this->getServiceLocator()->get('translator');
        $message = sprintf($translator->translate('Are you sure that you want to delete a page &laquo;%s&raquo;? All links and sub pages will be removed!', 'Pages'), $page->getTitle());

        $viewModel = new ViewModel();

        $viewModel->setVariables([
            'url' => $request->getPost('url'), // get from ajax data
            'message' => $message,
            'backUrl' => $request->getPost('backUrl')
        ]);

        return $viewModel;
    }

    public function deleteSelectedAction()
    {
        if (!$this->isGranted('pages.delete')) {
            throw new UnauthorizedException();
        }

        $request = $this->getRequest();

        if (!$request->isXmlHttpRequest() &&
            !$request->isPost()) {
            $this->getResponse()->setStatusCode(404);

            return new ViewModel();
        }

        $ids = $request->getPost('ids'); // get from ajax

        $translator = $this->getServiceLocator()->get('translator');

        if ($request->isPost() &&
            !$request->isXmlHttpRequest()
        ) {
            if (count($ids)) {

                if ($request->getPost('delete_content')) {
                    $contentManager = $this->getServiceLocator()->get('ContentManager\Model\ContentManager');
                    $contentManager->removePageContent($ids, true);
                }

                $this->pages->removePages($ids);

                return $this->redirect()->toUrl($request->getPost('backUrl'));
            } else {
                $message = $translator->translate('Nothing selected', 'Pages');

                $viewModel = new ViewModel(['message' => $message]);
                $viewModel->setTemplate('error/error');

                return $viewModel;
            }
        }

        $message = $translator->translate('Are you sure that you want delete selected pages?', 'Pages');

        $viewModel = new ViewModel([
            'ids'     => $ids,
            'message' => $message,
            'url'     => $request->getPost('url'), // get from ajax,
            'backUrl' => $request->getPost('backUrl')
        ]);

        return $viewModel;
    }

    public function getPageUrlAction()
    {
        $request = $this->getRequest();
        $pageId = $request->getPost('pageId');

        $page = $this->pages->getPageById($pageId);

        return new JsonModel([
            'url' => $this->pages->getPageUrl($page),
        ]);
    }
}
