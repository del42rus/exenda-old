<?php
namespace Pages\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Pages\Model\Pages;

class IndexController extends AbstractActionController
{
    public function __construct(Pages $pages)
    {
        $this->pages = $pages;
    }

    public function getJsonAction()
    {
        $pages = $this->pages->getPageChildren(0, true);

        return new JsonModel($pages);
    }

    public function indexAction()
    {
        $this->getResponse()->setStatusCode(404);
        return new ViewModel();
    }
}
