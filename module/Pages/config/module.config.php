<?php
namespace Pages;

use Pages\Controller\AdminController;
use Pages\Controller\IndexController;

return [
    'controllers' => [
        'factories' => [
            'Pages\Controller\Index' => function($cpm) {
                return new IndexController($cpm->getServiceLocator()->get('Pages\Model\Pages'));
            },
            'Pages\Controller\Admin' => function($cpm) {
                return new AdminController($cpm->getServiceLocator()->get('Pages\Model\Pages'));
            }
        ]
    ],

    'service_manager' => [
        'factories' => [
            'navigation' => 'Pages\Navigation\Service\DefaultNavigationFactory',
            'sidebar' => 'Pages\Navigation\Service\SidebarNavigationFactory',
            'bottom' => 'Pages\Navigation\Service\BottomNavigationFactory',
        ],
    ],

    'router' => [
        'routes' => [
            'admin_page' => [
                'type'    => 'segment',
                'options' => [
                    'route'    => '/admin/page/id/:id',
                    'constraints' => [
                        'lang' => '[a-zA-Z]{2}',
                        'id' => '\d+'
                    ],
                    'defaults' => [
                        'module' => 'Pages',
                        'controller' => 'Admin',
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

    'translator' => [
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../lang',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ],
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
           'pages' =>  __DIR__ . '/../view',
        ],
    ],

    // Doctrine config
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/' . __NAMESPACE__ . '/Model/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],

    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                __NAMESPACE__ => __DIR__ . '/..',
            ],
        ],
        'caching' => [
            'default' => [
                'cache'     => 'Filesystem',  // Apc, FilePath, FileSystem etc.
                'options' => [
                    'dir' => __DIR__ . '/../../../data/cache/assets', // path/to/cache
                ],
            ],
        ],
    ],

    'navigation' => [
        'admin' => [
            [
                'label' => 'Pages',
                'route' => 'admin_default',
                'class' => 'fa fa-files-o',
                'permission' => 'pages.view',
                'order' => 2,
                'params' => [
                    'module' => 'pages',
                ],
                'pages' => [
                    [
                        'route' => 'admin_default',
                        'visible' => false,
                        'params' => [
                            'module' => 'pages',
                            'action' => 'add',
                        ]
                    ],
                    [
                        'route' => 'admin_default/wildcard',
                        'visible' => false,
                        'params' => [
                            'module' => 'pages',
                            'action' => 'edit',
                        ]
                    ],
                ]
            ],
        ],
    ],
];
