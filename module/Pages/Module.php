<?php
namespace Pages;

use Pages\Model\Pages;

use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Mvc\MvcEvent;

use Zend\Console\Request as ConsoleRequest;
use Zend\Mvc\Router\RouteMatch;

use Pages\Mvc\RouteListener;
use Pages\Mvc\View\Http\LayoutTemplateListener;

class Module implements ViewHelperProviderInterface, ServiceProviderInterface
{
    public function onBootstrap($e)
    {
        $app = $e->getApplication();
        $eventManager  = $app->getEventManager();

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 100);
        $eventManager->attach(MvcEvent::EVENT_RENDER, [$this, 'injectTitle']);

        $routeListener = new RouteListener();
        $routeListener->attach($eventManager, -100);

        $layoutTemplateListener = new LayoutTemplateListener();
        $layoutTemplateListener->attach($eventManager);
    }

    public function injectTitle(MvcEvent $e)
    {
        $request = $e->getRequest();
        $matches = $e->getRouteMatch();
        $viewModel = $e->getViewModel();

        if (!$request->isXmlHttpRequest() &&
            !$viewModel->getVariable('title') &&
            str_replace(['-', '_'], '', strtolower($matches->getParam('module'))) == strtolower(__NAMESPACE__)) {
            $serviceLocator = $e->getApplication()->getServiceManager();
            $translator = $serviceLocator->get('translator');

            $title = $translator->translate('Pages', 'Pages');
            $viewModel->setVariable('title', $title);
        }
    }

    public function onDispatch(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();
        $request = $e->getRequest();

        if ($request instanceof ConsoleRequest) {
            return;
        }

        if ($matches->getMatchedRouteName() !== 'default') {
            return;
        }

        $sm = $e->getApplication()->getServiceManager();
        $viewHelperManager = $sm->get('viewHelperManager');

        $pageId = $matches->getParam('pageId');

        if (!$pageId) {
            return;
        }

        $pageModel = $sm->get('Pages\Model\Pages');
        $page = $pageModel->getPageById($pageId);

        $title =  $page->getSeoTitle();
        $description = $page->getDescription();
        $keywords = $page->getKeywords();

        if ($title) {
            $viewHelperManager->get('headTitle')->append($title);
        }

        $viewHelperManager->get('headMeta')
            ->appendName('description', $description)
            ->appendName('keywords', $keywords);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                'Pages\Model\Pages' =>  function($sm) {
                    return new Pages();
                },
            ],
            'aliases' => [
                'PageManager' => 'Pages\Model\Pages',
            ]
        ];
    }
    
    public function getViewHelperConfig() 
    {
        return [
            'factories' => [
                'linkTo' => function ($sm) {
                    $helper = new \Application\View\Helper\LinkTo();
                    $serviceLocator = $sm->getServiceLocator();
                    
                    $helper->setServiceLocator($serviceLocator); 
                    
                    $helper->setRouter($serviceLocator->get('Router'));
                    $match = $serviceLocator->get('application')
                                            ->getMvcEvent()
                                            ->getRouteMatch();

                    if ($match instanceof RouteMatch) {
                        $helper->setRouteMatch($match);
                    }

                    return $helper;
                },
            ]
        ];
    }

}
