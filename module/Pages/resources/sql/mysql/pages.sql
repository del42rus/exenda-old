CREATE TABLE IF NOT EXISTS `ex_pages` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `parent_id` int(11) DEFAULT NULL,
    `alias` varchar(255) NOT NULL,
    `module` varchar(255) DEFAULT 'content',
    `template` varchar(255) DEFAULT NULL,
    `title` varchar(255) NOT NULL,
    `seo_title` varchar(255),
    `description` text,
    `keywords` text,
    `link` int(11) DEFAULT NULL,
    `position` int(11) NOT NULL,
    `published` int(1) NOT NULL DEFAULT 1,
    `hidden` int(1) NOT NULL DEFAULT 0,
    `created_at` datetime NOT NULL,
    `updated_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idx_alias` (`alias`),
    FOREIGN KEY (`link`) REFERENCES `ex_pages` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (`parent_id`) REFERENCES `ex_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=utf8;

CREATE TABLE `ex_content` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `text` text,
    `page_id` int(11) DEFAULT NULL,
    `created_at` datetime NOT NULL,
    `updated_at` datetime NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idx_title`(`title`),
    FOREIGN KEY (`page_id`) REFERENCES `ex_pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

