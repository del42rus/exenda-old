var Pages = (function() {
    var $btnDelete = $('[data-action="delete"]');
    var $btnCloseModal = $('[data-dismiss="modal"]');

    var confirmDelete = function(data) {
        $.fancybox({
            padding: 0,
            autoScale : false,
            transitionIn : 'none',
            transitionOut : 'none',
            type : 'ajax',
            modal : true,
            ajax: {
                type: 'post',
                dataType: 'html',
                data: data
            },
            href : data.url
        });
    };

    var closeModal = function() {
        $.fancybox.close(true);
    };

    var bindUIActions = function() {
        $btnDelete.on('click', function(e) {
            e.preventDefault();
            var url = $(this).attr('data-url');

            var data = {
                url : url,
                backUrl: window.location.pathname + window.location.search
            };

            confirmDelete(data);
        });

        $('body').on({
            click: function(e) {
                e.preventDefault();
                closeModal();
            }
        }, $btnCloseModal.selector);
    };

    var init = function() {
        bindUIActions()
    };

    return {
        init: init
    }
})();

$(function() {
   Pages.init();
});