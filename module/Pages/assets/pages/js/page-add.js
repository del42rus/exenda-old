var Form = (function() {
    var $formAdd = $('form[name="PageAdd"]');
    var $formEdit = $('form[name="PageEdit"]');

    var $parentSelect = $('select[name="parentId"]');
    var $linkSelect = $('select[name="linkId"]');
    var $contentSelect = $('select[name="content"]');
    var $moduleSelect = $('select[name="module"]');
    var $templateSelect = $('select[name="template"]');
    var $aliasInput = $('input[name="alias"]');
    var $urlInput = $('input[name="url"]');

    var baseUrl = $urlInput.val();

    var parentChange = function() {
        return $.ajax({
            url: '/admin/pages/get-page-url',
            type: 'post',
            data: { pageId: $parentSelect.val() },
            dataType: 'json',
            success: function(res) {
                baseUrl = res.url;
                var alias = $aliasInput.val();
                var url = alias ?  baseUrl + '/' + alias : baseUrl;

                $urlInput.val(url);
            }
        })
    };

    var bindUIActions = function() {
        $linkSelect.on('change', function() {
            var disabled = $(this).val() !== '';

            $moduleSelect.attr('disabled', disabled);
            $templateSelect.attr('disabled', disabled);

            disabled = $(this).val() !== '' || $moduleSelect.val() !== 'content';

            $contentSelect.attr('disabled', disabled);
        });

        $moduleSelect.on('change', function() {
           $contentSelect.attr('disabled', $(this).val() !== 'content');
        });

        $formEdit.on('keyup', $aliasInput, function() {
            var alias = $aliasInput.val();

            var length = baseUrl.lastIndexOf('/');
            var path = baseUrl.substr(0, length);

            var url = alias ? path + '/' + alias : path;
            $urlInput.val(url);
        });

        $formAdd.on('keyup', $aliasInput, function() {
            var alias = $aliasInput.val();
            var url = alias ? baseUrl + '/' + alias : baseUrl;

            $urlInput.val(url);
        });

        $parentSelect.on('change', function() {
            parentChange();
        });
    };

    var init = function() {
        bindUIActions();
    };

    return {
        init: init
    }
})();

$(function(){
    Form.init();
});