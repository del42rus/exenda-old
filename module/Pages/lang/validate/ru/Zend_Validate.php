<?php
return [
    "Alias '%value%' already exists" => "Алиас '%value%' уже существует",
    "Page '%alias%' has children. You can not set module '%value%' for this pages" => "Страница '%alias%' содержит дочерние страницы. Вы не можете установить модуль '%value%' для данной страницы"
];