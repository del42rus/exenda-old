<?php
// Composer autoloading
if (file_exists('vendor/autoload.php')) {
    $loader = include 'vendor/autoload.php';

    $vendorDir = __DIR__ .  '/vendor';

    $classMap = [
        'WideImage' => $vendorDir . '/WideImage/WideImage.php',
        'CKEditor'  => $vendorDir . '/CKEditor/ckeditor.php',
        'CKFinder'  => $vendorDir . '/CKFinder/ckfinder.php',
    ];

    $composerClassMap = require  $vendorDir. '/composer/autoload_classmap.php';

    $classMap = array_diff($classMap, $composerClassMap);

    $composerFiles = require  $vendorDir. '/composer/autoload_files.php';

    $files = [];

    $files = array_diff($files, $composerFiles);
    $loader->addClassMap($classMap);

    foreach ($files as $file) {
        require $file;
    }

}

$zf2Path = false;

if (is_dir('vendor/ZF2/library')) {
    $zf2Path = 'vendor/ZF2/library';
} elseif (getenv('ZF2_PATH')) {      // Support for ZF2_PATH environment variable or git submodule
    $zf2Path = getenv('ZF2_PATH');
} elseif (get_cfg_var('zf2_path')) { // Support for zf2_path directive value
    $zf2Path = get_cfg_var('zf2_path');
}

if ($zf2Path) {
    if (isset($loader)) {
        $loader->add('Zend', $zf2Path);
    } else {
        include $zf2Path . '/Zend/Loader/AutoloaderFactory.php';
        Zend\Loader\AutoloaderFactory::factory([
            'Zend\Loader\StandardAutoloader' => [
                'autoregister_zf' => true
            ]
        ]);
    }
}

if (!class_exists('Zend\Loader\AutoloaderFactory')) {
    throw new RuntimeException('Unable to load ZF2. Run `php composer.phar install` or define a ZF2_PATH environment variable.');
}
