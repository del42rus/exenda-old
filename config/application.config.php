<?php
return array (
    'modules' =>
        array (
            'Zf2Whoops',
            'ZfcRbac',
            'AssetManager',
            'DoctrineModule',
            'DoctrineORMModule',
            'Application',
            'AccessManager',
            'Users',
            'Installation',
        ),
    'module_listener_options' =>
        array (
            'module_paths' =>
                array (
                    0 => './module',
                    1 => './vendor',
                ),
            'config_glob_paths' =>
                array (
                    0 => 'config/autoload/{,*.}{global,local}.php',
                ),
        ),
);